package com.syltech.nigma.nationalhealth;


import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
//import android.widget.ImageButton;
//import android.widget.Toast;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;


import java.util.ArrayList;



public class Medicines extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    private BottomNavigationView navigation;
    //Typeface tfcFaq;

    TextView cod,nam,uni,price,leve;
    Typeface tfcCod,tfcNam,tfcUni,tfcPrice,tfcLeve;

    private SearchView search;
    private MyTabListAdapter listAdapter;
    private ArrayList<Pills> pillsList=new ArrayList<Pills>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicines);


        //btnBack=(ImageButton) findViewById(R.id.imageButtonGetBack);

        navigation=(BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_covered);

        navigation.setOnNavigationItemSelectedListener(this);

        setDefaultFragment();

    }

    private void setDefaultFragment() {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout,MedFrag.newInstance());
        transaction.commit();

    }





    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment selectedFragment=null;
        switch (item.getItemId()) {
            case R.id.navigation_covered:
                selectedFragment=MedFrag.newInstance();
                break;
            case R.id.navigation_ex:
                selectedFragment=ExcludedFrag.newInstance();
                break;

        }

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout,selectedFragment);
        transaction.commit();
        return true;

    }


    @Override
    public void onBackPressed() {
        this.finish();
    }
}
