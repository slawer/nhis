package com.syltech.nigma.nationalhealth;

import java.util.ArrayList;

/**
 * Created by nigma on 28/06/2017.
 */

public class Pills {

    private int number=0;

    private String name;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


    private ArrayList<Tablets> tabs=new ArrayList<Tablets>();

    public Pills(int number,String name, ArrayList<Tablets> tabs) {
        this.number = number;
        this.name = name;
        this.tabs = tabs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Tablets> getTabs() {
        return tabs;
    }

    public void setTabs(ArrayList<Tablets> tabs) {
        this.tabs = tabs;
    }
}
