package com.syltech.nigma.nationalhealth;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;


public class MedFrag extends Fragment implements SearchView.OnQueryTextListener,SearchView.OnCloseListener, ExpandableListView.OnChildClickListener, View.OnClickListener{

    View myFragment;

    private ExpandableListView list;
    private ImageButton btnBack;
    private SearchManager searchManager;
    private BottomNavigationView navigation;
    //Typeface tfcFaq;

    TextView cod,nam,uni,price,leve;
    Typeface tfcCod,tfcNam,tfcUni,tfcPrice,tfcLeve;

    private SearchView search;
    private MyTabListAdapter listAdapter;
    private ArrayList<Pills> pillsList=new ArrayList<Pills>();



    public static MedFrag newInstance() {
        MedFrag fragment = new MedFrag();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myFragment=inflater.inflate(R.layout.fragment_med, container, false);


        btnBack=(ImageButton) myFragment.findViewById(R.id.imageButtonGetBack);

        btnBack.setOnClickListener(this);

        searchManager=(SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        search=(SearchView) myFragment.findViewById(R.id.search);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setOnCloseListener(this);

        initData();

        list=(ExpandableListView) myFragment.findViewById(R.id.liste);
        listAdapter=new MyTabListAdapter(getActivity(),pillsList);
        list.setAdapter(listAdapter);
        list.setOnChildClickListener(this);
        // Inflate the layout for this fragment
        return myFragment;
    }

    private void initData() {
        ArrayList<Tablets> childList= new ArrayList<Tablets>();
        Tablets tablets=new Tablets("ACETAZIN1");
        childList.add(tablets);
        tablets=new Tablets("ACETAZTA1");
        childList.add(tablets);
        tablets=new Tablets("ACETYLIN1");
        childList.add(tablets);
        tablets=new Tablets("ACETYLTA1");
        childList.add(tablets);
        tablets=new Tablets("ACETYLDT1");
        childList.add(tablets);
        tablets=new Tablets("ACTCHAPO1");
        childList.add(tablets);
        tablets=new Tablets("ACICLOCR1");
        childList.add(tablets);
        tablets=new Tablets("ACICLOEO1");
        childList.add(tablets);
        tablets=new Tablets("ACICLOIN1");
        childList.add(tablets);
        tablets=new Tablets("ACICLOSU2");
        childList.add(tablets);
        tablets=new Tablets("ACICLOTA1");
        childList.add(tablets);
        tablets=new Tablets("ADRENAIN1");
        childList.add(tablets);
        tablets=new Tablets("ADRENAIN2");
        childList.add(tablets);
        tablets=new Tablets("ADRIAMIN1");
        childList.add(tablets);
        tablets=new Tablets("ALBENDSY1");
        childList.add(tablets);
        tablets=new Tablets("ALBENDTA1");
        childList.add(tablets);
        tablets=new Tablets("ALBENDTA2");
        childList.add(tablets);
        tablets=new Tablets("ALLOPUTA1");
        childList.add(tablets);
        tablets=new Tablets("ALLOPUTA2");
        childList.add(tablets);
        tablets=new Tablets("AMIACIIN1");
        childList.add(tablets);
        tablets=new Tablets("AMIACIIN2");
        childList.add(tablets);
        tablets=new Tablets("AMINOPIN1");
        childList.add(tablets);
        tablets=new Tablets("AMIODATA1");
        childList.add(tablets);
        tablets=new Tablets("AMITRITA1");
        childList.add(tablets);
        tablets=new Tablets("AMITRITA2");
        childList.add(tablets);
        tablets=new Tablets("AMITRITA3");
        childList.add(tablets);
        tablets=new Tablets("AMLODITA1");
        childList.add(tablets);
        tablets=new Tablets("AMLODITA2");
        childList.add(tablets);
        tablets=new Tablets("AMOARTPO1");
        childList.add(tablets);
        tablets=new Tablets("AMOARTPO2");
        childList.add(tablets);
        tablets=new Tablets("AMOARTTA1");
        childList.add(tablets);
        tablets=new Tablets("AMOARTTA2");
        childList.add(tablets);
        tablets=new Tablets("AMOXICCA1");
        childList.add(tablets);
        tablets=new Tablets("AMOXICCA2");
        childList.add(tablets);
        tablets=new Tablets("AMOXICSU1");
        childList.add(tablets);
        tablets=new Tablets("AMPICIIN1");
        childList.add(tablets);
        tablets=new Tablets("ANASTRTA1");
        childList.add(tablets);
        tablets=new Tablets("ANIMGLIN1");
        childList.add(tablets);
        tablets=new Tablets("AQUEOUCR1");
        childList.add(tablets);
        tablets=new Tablets("ARTLUMDT1");
        childList.add(tablets);
        tablets=new Tablets("ARTLUMSU1");
        childList.add(tablets);
        tablets=new Tablets("ARTLUMTA1");
        childList.add(tablets);
        tablets=new Tablets("ARTESUIN1");
        childList.add(tablets);
        tablets=new Tablets("ARTESUIN2");
        childList.add(tablets);
        tablets=new Tablets("ARTESURE1");
        childList.add(tablets);
        tablets=new Tablets("ARTESURE2");
        childList.add(tablets);
        tablets=new Tablets("ATEHYDTA1");
        childList.add(tablets);
        tablets=new Tablets("ATEHYDTA2");
        childList.add(tablets);
        tablets=new Tablets("ATENOLIN1");
        childList.add(tablets);
        tablets=new Tablets("ATENOLTA1");
        childList.add(tablets);
        tablets=new Tablets("ATENOLTA2");
        childList.add(tablets);
        tablets=new Tablets("ATENOLTA3");
        childList.add(tablets);
        tablets=new Tablets("ATORVATA1");
        childList.add(tablets);
        tablets=new Tablets("ATORVATA2");
        childList.add(tablets);
        tablets=new Tablets("ATROPIID1");
        childList.add(tablets);
        tablets=new Tablets("ATROPIIN1");
        childList.add(tablets);
        tablets=new Tablets("AZITHRCA1");
        childList.add(tablets);
        tablets=new Tablets("AZITHRSU1");
        childList.add(tablets);
        tablets=new Tablets("AZITHRSU2");
        childList.add(tablets);

        Pills groupList=new Pills(0,"a",childList);
        pillsList.add(groupList);

        //Group B
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("BADOESIN1");
        childList.add(tablets);
        tablets=new Tablets("BECDIPGA1");
        childList.add(tablets);
        tablets=new Tablets("BECDIPGA2");
        childList.add(tablets);
        tablets=new Tablets("BECDIPGA3");
        childList.add(tablets);
        tablets=new Tablets("BENDROTA1");
        childList.add(tablets);
        tablets=new Tablets("BENDROTA2");
        childList.add(tablets);
        tablets=new Tablets("BENZATIN1");
        childList.add(tablets);
        tablets=new Tablets("BENZATTA1");
        childList.add(tablets);
        tablets=new Tablets("BEACSAOI1");
        childList.add(tablets);
        tablets=new Tablets("BENPERCR1");
        childList.add(tablets);
        tablets=new Tablets("BENPERCR2");
        childList.add(tablets);
        tablets=new Tablets("BENBENLO1");
        childList.add(tablets);
        tablets=new Tablets("BENBENLO2");
        childList.add(tablets);
        tablets=new Tablets("BENZYLIN1");
        childList.add(tablets);
        tablets=new Tablets("BENZYLIN2");
        childList.add(tablets);
        tablets=new Tablets("BETVALCR2");
        childList.add(tablets);
        tablets=new Tablets("BETAXOID1");
        childList.add(tablets);
        tablets=new Tablets("BISACOTA1");
        childList.add(tablets);
        tablets=new Tablets("BROMOCTA1");
        childList.add(tablets);
        tablets=new Tablets("BUDFORGA2");
        childList.add(tablets);
        tablets=new Tablets("BUDFORGA1");
        childList.add(tablets);
        tablets=new Tablets("BUDESOGA1");
        childList.add(tablets);
        tablets=new Tablets("BUDESOGA2");
        childList.add(tablets);

        groupList=new Pills(1,"b",childList);
        pillsList.add(groupList);


        //Group C
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("CALAMICR1");
        childList.add(tablets);
        tablets=new Tablets("CALAMILO1");
        childList.add(tablets);
        tablets=new Tablets("CALCIFTA1");
        childList.add(tablets);
        tablets=new Tablets("CALCARTA1");
        childList.add(tablets);
        tablets=new Tablets("CALGLUIN1");
        childList.add(tablets);
        tablets=new Tablets("CALVITTA1");
        childList.add(tablets);
        tablets=new Tablets("CAPECITA1");
        childList.add(tablets);
        tablets=new Tablets("CARBAMTA1");
        childList.add(tablets);
        tablets=new Tablets("CARBAMTA2");
        childList.add(tablets);
        tablets=new Tablets("CARBAMTA3");
        childList.add(tablets);
        tablets=new Tablets("CARBAMTA4");
        childList.add(tablets);
        tablets=new Tablets("CARBIMTA1");
        childList.add(tablets);
        tablets=new Tablets("CARBIMTA2");
        childList.add(tablets);
        tablets=new Tablets("CARBOCSY2");
        childList.add(tablets);
        tablets=new Tablets("CEFACLCA1");
        childList.add(tablets);
        tablets=new Tablets("CEFACLCA2");
        childList.add(tablets);
        tablets=new Tablets("CEFACLSU1");
        childList.add(tablets);
        tablets=new Tablets("CEFACLSU2");
        childList.add(tablets);
        tablets=new Tablets("CEFOTAIN1");
        childList.add(tablets);
        tablets=new Tablets("CEFOTAIN2");
        childList.add(tablets);
        tablets=new Tablets("CEFTRIIN2");
        childList.add(tablets);
        tablets=new Tablets("CEFTRIIN3");
        childList.add(tablets);
        tablets=new Tablets("CEFUROIN1");
        childList.add(tablets);
        tablets=new Tablets("CEFUROSU1");
        childList.add(tablets);
        tablets=new Tablets("CEFUROTA1");
        childList.add(tablets);
        tablets=new Tablets("CEFUROTA2");
        childList.add(tablets);
        tablets=new Tablets("CETIRICA1");
        childList.add(tablets);
        tablets=new Tablets("CETIRISY1");
        childList.add(tablets);
        tablets=new Tablets("CETIRITA1");
        childList.add(tablets);
        tablets=new Tablets("CETRIMSO1");
        childList.add(tablets);
        tablets=new Tablets("CHLORAED1");
        childList.add(tablets);
        tablets=new Tablets("CHLORAID1");
        childList.add(tablets);
        tablets=new Tablets("CHLORAEO1");
        childList.add(tablets);
        tablets=new Tablets("CHLORAIN1");
        childList.add(tablets);
        tablets=new Tablets("CHLORASU1");
        childList.add(tablets);
        tablets=new Tablets("CHLORHCR1");
        childList.add(tablets);
        tablets=new Tablets("CHLORHMW1");
        childList.add(tablets);
        tablets=new Tablets("CHLORHSO1");
        childList.add(tablets);
        tablets=new Tablets("CHLPHESY1");
        childList.add(tablets);
        tablets=new Tablets("CHLPHETA1");
        childList.add(tablets);
        tablets=new Tablets("CHLPROIN1");
        childList.add(tablets);
        tablets=new Tablets("CHLPROTA1");
        childList.add(tablets);
        tablets=new Tablets("CHLPROTA2");
        childList.add(tablets);
        tablets=new Tablets("CHLPROTA3");
        childList.add(tablets);
        tablets=new Tablets("CHREFLIN1");
        childList.add(tablets);
        tablets=new Tablets("CHREFLIN2");
        childList.add(tablets);
        tablets=new Tablets("CIPROFID1");
        childList.add(tablets);
        tablets=new Tablets("CIPROFIN1");
        childList.add(tablets);
        tablets=new Tablets("CIPROFTA1");
        childList.add(tablets);
        tablets=new Tablets("CIPROFTA2");
        childList.add(tablets);
        tablets=new Tablets("CIPTINTA1");
        childList.add(tablets);
        tablets=new Tablets("CLARITCA1");
        childList.add(tablets);
        tablets=new Tablets("CLARITCA2");
        childList.add(tablets);
        tablets=new Tablets("CLARITSU1");
        childList.add(tablets);
        tablets=new Tablets("CLINDACA1");
        childList.add(tablets);
        tablets=new Tablets("CLINDAIN1");
        childList.add(tablets);
        tablets=new Tablets("CLINDASU1");
        childList.add(tablets);
        tablets=new Tablets("CLINDASO1");
        childList.add(tablets);
        tablets=new Tablets("CLOPROCR1");
        childList.add(tablets);
        tablets=new Tablets("CLOHYDCR1");
        childList.add(tablets);
        tablets=new Tablets("CLOTRICR1");
        childList.add(tablets);
        tablets=new Tablets("CLOTRICR2");
        childList.add(tablets);
        tablets=new Tablets("CLOTRIVP1");
        childList.add(tablets);
        tablets=new Tablets("CLOTRIVP2");
        childList.add(tablets);
        tablets=new Tablets("CLOTRIVP3");
        childList.add(tablets);
        tablets=new Tablets("CLOXACIN1");
        childList.add(tablets);
        tablets=new Tablets("CLOXACIN2");
        childList.add(tablets);
        tablets=new Tablets("COAMOXIN1");
        childList.add(tablets);
        tablets=new Tablets("COAMOXIN2");
        childList.add(tablets);
        tablets=new Tablets("COAMOXSU1");
        childList.add(tablets);
        tablets=new Tablets("COAMOXSU2");
        childList.add(tablets);
        tablets=new Tablets("COAMOXTA1");
        childList.add(tablets);
        tablets=new Tablets("COAMOXTA2");
        childList.add(tablets);
        tablets=new Tablets("CODEINTA1");
        childList.add(tablets);
        tablets=new Tablets("COOENOTA1");
        childList.add(tablets);
        tablets=new Tablets("CONOESTA1");
        childList.add(tablets);
        tablets=new Tablets("CONOESVC1");
        childList.add(tablets);
        tablets=new Tablets("CORANTID1");
        childList.add(tablets);
        tablets=new Tablets("CORANTEO1");
        childList.add(tablets);
        tablets=new Tablets("COTRIMSU1");
        childList.add(tablets);
        tablets=new Tablets("COTRIMTA1");
        childList.add(tablets);
        tablets=new Tablets("CYCLOPID1");
        childList.add(tablets);
        tablets=new Tablets("CYCLOPIN1");
        childList.add(tablets);
        groupList=new Pills(2,"c",childList);
        pillsList.add(groupList);


        //Group D
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("DALSODIN1");
        childList.add(tablets);
        tablets=new Tablets("DARROWIN1");
        childList.add(tablets);
        tablets=new Tablets("DEXAMEID1");
        childList.add(tablets);
        tablets=new Tablets("DEXAMEEO1");
        childList.add(tablets);
        tablets=new Tablets("DEXAMEIN1");
        childList.add(tablets);
        tablets=new Tablets("DEXAMEIN2");
        childList.add(tablets);
        tablets=new Tablets("DEXAMETA1");
        childList.add(tablets);
        tablets=new Tablets("DESOCHIN1");
        childList.add(tablets);
        tablets=new Tablets("DESOCHIN2");
        childList.add(tablets);
        tablets=new Tablets("DEXTROIN1");
        childList.add(tablets);
        tablets=new Tablets("DEXTROIN2");
        childList.add(tablets);
        tablets=new Tablets("DEXTROIN3");
        childList.add(tablets);
        tablets=new Tablets("DEXTROIN4");
        childList.add(tablets);
        tablets=new Tablets("DEXTROIN6");
        childList.add(tablets);
        tablets=new Tablets("DIAZEPIN1");
        childList.add(tablets);
        tablets=new Tablets("DIAZEPRS1");
        childList.add(tablets);
        tablets=new Tablets("DIAZEPTA1");
        childList.add(tablets);
        tablets=new Tablets("DIAZEPTA2");
        childList.add(tablets);
        tablets=new Tablets("DICLOFCA1");
        childList.add(tablets);
        tablets=new Tablets("DICLOFGE1");
        childList.add(tablets);
        tablets=new Tablets("DICLOFIN1");
        childList.add(tablets);
        tablets=new Tablets("DICLOFRE1");
        childList.add(tablets);
        tablets=new Tablets("DICLOFRE2");
        childList.add(tablets);
        tablets=new Tablets("DICLOFTA1");
        childList.add(tablets);
        tablets=new Tablets("DICLOFTA2");
        childList.add(tablets);
        tablets=new Tablets("DIESTITA1");
        childList.add(tablets);
        tablets=new Tablets("DIESTITA2");
        childList.add(tablets);
        tablets=new Tablets("DIGOXIEL1");
        childList.add(tablets);
        tablets=new Tablets("DIGOXITA1");
        childList.add(tablets);
        tablets=new Tablets("DIGOXITA2");
        childList.add(tablets);
        tablets=new Tablets("DIGOXITA3");
        childList.add(tablets);
        tablets=new Tablets("DIHPIPPO1");
        childList.add(tablets);
        tablets=new Tablets("DIHYDRTA1");
        childList.add(tablets);
        tablets=new Tablets("DISOPYCA1");
        childList.add(tablets);
        tablets=new Tablets("DISPHOIN1");
        childList.add(tablets);
        tablets=new Tablets("DOCETAIN1");
        childList.add(tablets);
        tablets=new Tablets("DOMPERTA1");
        childList.add(tablets);
        tablets=new Tablets("DOPAMIIN1");
        childList.add(tablets);
        tablets=new Tablets("DOXAPRIN1");
        childList.add(tablets);
        tablets=new Tablets("DOXYCYCA1");
        childList.add(tablets);

        groupList=new Pills(3,"d",childList);
        pillsList.add(groupList);


        //Group E
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("ENOSODIN2");
        childList.add(tablets);
        tablets=new Tablets("EPHEDRIN1");
        childList.add(tablets);
        tablets=new Tablets("EPHEDRND1");
        childList.add(tablets);
        tablets=new Tablets("EPHEDRND2");
        childList.add(tablets);
        tablets=new Tablets("ERGOMEIN1");
        childList.add(tablets);
        tablets=new Tablets("ERGOMEIN2");
        childList.add(tablets);
        tablets=new Tablets("ERGOMETA1");
        childList.add(tablets);
        tablets=new Tablets("ERGOTATA1");
        childList.add(tablets);
        tablets=new Tablets("ERYTHRSY1");
        childList.add(tablets);
        tablets=new Tablets("ERYTHRTA1");
        childList.add(tablets);
        tablets=new Tablets("ESOMEPCA1");
        childList.add(tablets);
        tablets=new Tablets("ESOMEPCA2");
        childList.add(tablets);
        tablets=new Tablets("ETHOSUSY1");
        childList.add(tablets);
        tablets=new Tablets("ETHOSUTA1");
        childList.add(tablets);

        groupList=new Pills(4,"e",childList);
        pillsList.add(groupList);


        //Group F
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("FEAMCISU1");
        childList.add(tablets);
        tablets=new Tablets("FERFUMTA1");
        childList.add(tablets);
        tablets=new Tablets("FERSULSY1");
        childList.add(tablets);
        tablets=new Tablets("FESUFOTA1");
        childList.add(tablets);
        tablets=new Tablets("FERSULTA1");
        childList.add(tablets);
        tablets=new Tablets("FINASTTA1");
        childList.add(tablets);
        tablets=new Tablets("FLUCLOCA1");
        childList.add(tablets);
        tablets=new Tablets("FLUCLOIN1");
        childList.add(tablets);
        tablets=new Tablets("FLUCLOIN2");
        childList.add(tablets);
        tablets=new Tablets("FLUCLOSU1");
        childList.add(tablets);
        tablets=new Tablets("FLUCONCA1");
        childList.add(tablets);
        tablets=new Tablets("FLUCONCA2");
        childList.add(tablets);
        tablets=new Tablets("FLUCONSU1");
        childList.add(tablets);
        tablets=new Tablets("FLUCONSU2");
        childList.add(tablets);
        tablets=new Tablets("FLUCONTA1");
        childList.add(tablets);
        tablets=new Tablets("FLUDROTA1");
        childList.add(tablets);
        tablets=new Tablets("FLUOXECA1");
        childList.add(tablets);
        tablets=new Tablets("FLUPENTA1");
        childList.add(tablets);
        tablets=new Tablets("FLUPENTA2");
        childList.add(tablets);
        tablets=new Tablets("FLUDECIN1");
        childList.add(tablets);
        tablets=new Tablets("FLUSALGA1");
        childList.add(tablets);
        tablets=new Tablets("FLUTICGA1");
        childList.add(tablets);
        tablets=new Tablets("FLUTICGA2");
        childList.add(tablets);
        tablets=new Tablets("FLUTICGA3");
        childList.add(tablets);
        tablets=new Tablets("FLUVASCA1");
        childList.add(tablets);
        tablets=new Tablets("FOLACITA1");
        childList.add(tablets);
        tablets=new Tablets("FUROSEIN1");
        childList.add(tablets);
        tablets=new Tablets("FUROSETA1");
        childList.add(tablets);

        groupList=new Pills(5,"f",childList);
        pillsList.add(groupList);


        //Group G
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("GELATIIN1");
        childList.add(tablets);
        tablets=new Tablets("GENTAMED1");
        childList.add(tablets);
        tablets=new Tablets("GENTAMID1");
        childList.add(tablets);
        tablets=new Tablets("GENTAMIN1");
        childList.add(tablets);
        tablets=new Tablets("GLIBENTA1");
        childList.add(tablets);
        tablets=new Tablets("GLICLATA1");
        childList.add(tablets);
        tablets=new Tablets("GLIMEPTA1");
        childList.add(tablets);
        tablets=new Tablets("GLIMEPTA2");
        childList.add(tablets);
        tablets=new Tablets("GLIMEPTA3");
        childList.add(tablets);
        tablets=new Tablets("GLIMEPTA4");
        childList.add(tablets);
        tablets=new Tablets("GLUCAGIN1");
        childList.add(tablets);
        tablets=new Tablets("GLTRSUTA1");
        childList.add(tablets);
        tablets=new Tablets("GRANISIN1");
        childList.add(tablets);
        tablets=new Tablets("GRANISTA1");
        childList.add(tablets);
        tablets=new Tablets("GRISEOSU1");
        childList.add(tablets);
        tablets=new Tablets("GRISEOTA1");
        childList.add(tablets);
        tablets=new Tablets("GRISEOTA2");
        childList.add(tablets);

        groupList=new Pills(6,"g",childList);
        pillsList.add(groupList);


        //Group H
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("HALOPEIN1");
        childList.add(tablets);
        tablets=new Tablets("HALOPETA1");
        childList.add(tablets);
        tablets=new Tablets("HALOPETA2");
        childList.add(tablets);
        tablets=new Tablets("HALOPETA3");
        childList.add(tablets);
        tablets=new Tablets("HEPARIIN1");
        childList.add(tablets);
        tablets=new Tablets("HEPARIIN2");
        childList.add(tablets);
        tablets=new Tablets("HEPARIIN3");
        childList.add(tablets);
        tablets=new Tablets("HUIMTEIN1");
        childList.add(tablets);
        tablets=new Tablets("HUIMTEIN2");
        childList.add(tablets);
        tablets=new Tablets("HYDRALIN1");
        childList.add(tablets);
        tablets=new Tablets("HYDRALTA1");
        childList.add(tablets);
        tablets=new Tablets("HYDROCCR1");
        childList.add(tablets);
        tablets=new Tablets("HYDROCID1");
        childList.add(tablets);
        tablets=new Tablets("HYDROCEO1");
        childList.add(tablets);
        tablets=new Tablets("HYSOSUIN1");
        childList.add(tablets);
        tablets=new Tablets("HYDROXIN1");
        childList.add(tablets);
        tablets=new Tablets("HYOBUTIN1");
        childList.add(tablets);
        tablets=new Tablets("HYOBUTTA1");
        childList.add(tablets);

        groupList=new Pills(7,"h",childList);
        pillsList.add(groupList);


        //Group I
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("IBUPROSU1");
        childList.add(tablets);
        tablets=new Tablets("IBUPROTA1");
        childList.add(tablets);
        tablets=new Tablets("IBUPROTA2");
        childList.add(tablets);
        tablets=new Tablets("IMIPRATA1");
        childList.add(tablets);
        tablets=new Tablets("INPRMIIN1");
        childList.add(tablets);
        tablets=new Tablets("INSSOLIN1");
        childList.add(tablets);
        tablets=new Tablets("INTRALSO1");
        childList.add(tablets);
        tablets=new Tablets("IROPOLCA1");
        childList.add(tablets);
        tablets=new Tablets("IROPOLSU1");
        childList.add(tablets);
        tablets=new Tablets("IRODEXIN1");
        childList.add(tablets);
        tablets=new Tablets("IROSUCIN1");
        childList.add(tablets);
        tablets=new Tablets("ISOINSIN1");
        childList.add(tablets);
        tablets=new Tablets("ISODINTA1");
        childList.add(tablets);
        tablets=new Tablets("ITRACOCA1");
        childList.add(tablets);
        tablets=new Tablets("ITRACOSU1");
        childList.add(tablets);

        groupList=new Pills(8,"i",childList);
        pillsList.add(groupList);


        //Group K
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("KETOCOCR1");
        childList.add(tablets);
        tablets=new Tablets("KETOCOTA1");
        childList.add(tablets);

        groupList=new Pills(9,"k",childList);
        pillsList.add(groupList);


        //Group L
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("LABETAIN1");
        childList.add(tablets);
        tablets=new Tablets("LABETATA1");
        childList.add(tablets);
        tablets=new Tablets("LABETATA2");
        childList.add(tablets);
        tablets=new Tablets("LACTULLI1");
        childList.add(tablets);
        tablets=new Tablets("LEVSODTA1");
        childList.add(tablets);
        tablets=new Tablets("LEVSODTA2");
        childList.add(tablets);
        tablets=new Tablets("LEVSODTA3");
        childList.add(tablets);
        tablets=new Tablets("LIDOCACR1");
        childList.add(tablets);
        tablets=new Tablets("LIDOCAGE1");
        childList.add(tablets);
        tablets=new Tablets("LISHYDTA1");
        childList.add(tablets);
        tablets=new Tablets("LISHYDTA2");
        childList.add(tablets);
        tablets=new Tablets("LISINOTA1");
        childList.add(tablets);
        tablets=new Tablets("LISINOTA2");
        childList.add(tablets);
        tablets=new Tablets("LISINOTA3");
        childList.add(tablets);
        tablets=new Tablets("LISINOTA4");
        childList.add(tablets);
        tablets=new Tablets("LODOXAID1");
        childList.add(tablets);
        tablets=new Tablets("LORAZEIN1");
        childList.add(tablets);
        tablets=new Tablets("LORAZETA1");
        childList.add(tablets);
        tablets=new Tablets("LORAZETA2");
        childList.add(tablets);
        tablets=new Tablets("LORAZETA3");
        childList.add(tablets);
        tablets=new Tablets("LOSARTTA1");
        childList.add(tablets);
        tablets=new Tablets("LOSARTTA2");
        childList.add(tablets);
        tablets=new Tablets("LOSARTTA3");
        childList.add(tablets);

        groupList=new Pills(10,"l",childList);
        pillsList.add(groupList);


        //Group M
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("MAGSULIN1");
        childList.add(tablets);
        tablets=new Tablets("MAGSULIN3");
        childList.add(tablets);
        tablets=new Tablets("MAGSULPO1");
        childList.add(tablets);
        tablets=new Tablets("MATRALMI1");
        childList.add(tablets);
        tablets=new Tablets("MATRALTA1");
        childList.add(tablets);
        tablets=new Tablets("MAGTRIMI1");
        childList.add(tablets);
        tablets=new Tablets("MAGTRITA1");
        childList.add(tablets);
        tablets=new Tablets("MANNITIN1");
        childList.add(tablets);
        tablets=new Tablets("MANNITIN2");
        childList.add(tablets);
        tablets=new Tablets("MEBENDSU1");
        childList.add(tablets);
        tablets=new Tablets("MEBENDTA1");
        childList.add(tablets);
        tablets=new Tablets("MEBENDTA2");
        childList.add(tablets);
        tablets=new Tablets("MEBEVETA1");
        childList.add(tablets);
        tablets=new Tablets("MEDACETA1");
        childList.add(tablets);
        tablets=new Tablets("MEFACICA1");
        childList.add(tablets);
        tablets=new Tablets("MEFACITA1");
        childList.add(tablets);
        tablets=new Tablets("METFORTA1");
        childList.add(tablets);
        tablets=new Tablets("METHOTIN1");
        childList.add(tablets);
        tablets=new Tablets("METHOTIN2");
        childList.add(tablets);
        tablets=new Tablets("METHOTTA1");
        childList.add(tablets);
        tablets=new Tablets("METHOTTA2");
        childList.add(tablets);
        tablets=new Tablets("METCELID1");
        childList.add(tablets);
        tablets=new Tablets("METHYLTA1");
        childList.add(tablets);
        tablets=new Tablets("METOCLIN1");
        childList.add(tablets);
        tablets=new Tablets("METOCLSY1");
        childList.add(tablets);
        tablets=new Tablets("METOCLTA1");
        childList.add(tablets);
        tablets=new Tablets("METOLATA1");
        childList.add(tablets);
        tablets=new Tablets("METRONIN1");
        childList.add(tablets);
        tablets=new Tablets("METRONRE1");
        childList.add(tablets);
        tablets=new Tablets("METRONSU1");
        childList.add(tablets);
        tablets=new Tablets("METRONSU2");
        childList.add(tablets);
        tablets=new Tablets("METRONTA1");
        childList.add(tablets);
        tablets=new Tablets("METRONTA2");
        childList.add(tablets);
        tablets=new Tablets("MICHYDCR1");
        childList.add(tablets);
        tablets=new Tablets("MICONACR1");
        childList.add(tablets);
        tablets=new Tablets("MICONAOG1");
        childList.add(tablets);
        tablets=new Tablets("MICONAVP1");
        childList.add(tablets);
        tablets=new Tablets("MIDAZOIN1");
        childList.add(tablets);
        tablets=new Tablets("MIDAZOTA1");
        childList.add(tablets);
        tablets=new Tablets("MORPHIIN1");
        childList.add(tablets);
        tablets=new Tablets("MORPHIIN2");
        childList.add(tablets);
        tablets=new Tablets("MORSULTA1");
        childList.add(tablets);
        tablets=new Tablets("MORSULTA2");
        childList.add(tablets);
        tablets=new Tablets("MULTIVDR1");
        childList.add(tablets);
        tablets=new Tablets("MULTIVSY1");
        childList.add(tablets);
        tablets=new Tablets("MULTIVTA1");
        childList.add(tablets);

        groupList=new Pills(11,"m",childList);
        pillsList.add(groupList);


        //Group N
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("NALOXOIN1");
        childList.add(tablets);
        tablets=new Tablets("NEOMYCTA1");
        childList.add(tablets);
        tablets=new Tablets("NEOBROTA1");
        childList.add(tablets);
        tablets=new Tablets("NEOSTIIN1");
        childList.add(tablets);
        tablets=new Tablets("NIFEDICA1");
        childList.add(tablets);
        tablets=new Tablets("NIFEDITA1");
        childList.add(tablets);
        tablets=new Tablets("NIFEDITA2");
        childList.add(tablets);
        tablets=new Tablets("NIFEDITA3");
        childList.add(tablets);
        tablets=new Tablets("NITROFTA1");
        childList.add(tablets);
        tablets=new Tablets("NORETHTA1");
        childList.add(tablets);
        tablets=new Tablets("NYSTATOI1");
        childList.add(tablets);
        tablets=new Tablets("NYSTATTA1");
        childList.add(tablets);
        tablets=new Tablets("NYSTATSU1");
        childList.add(tablets);
        tablets=new Tablets("NYSTATTA2");
        childList.add(tablets);

        groupList=new Pills(12,"n",childList);
        pillsList.add(groupList);


        //Group O
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("OMEPRAIN2");
        childList.add(tablets);
        tablets=new Tablets("OMEPRATA1");
        childList.add(tablets);
        tablets=new Tablets("ORRESAPO1");
        childList.add(tablets);
        tablets=new Tablets("OXYTOCIN1");
        childList.add(tablets);
        tablets=new Tablets("OXYTOCIN2");
        childList.add(tablets);

        groupList=new Pills(13,"o",childList);
        pillsList.add(groupList);


        //Group P
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("PACLITIN1");
        childList.add(tablets);
        tablets=new Tablets("PARACERE1");
        childList.add(tablets);
        tablets=new Tablets("PARACERE2");
        childList.add(tablets);
        tablets=new Tablets("PARACERE3");
        childList.add(tablets);
        tablets=new Tablets("PARACESY1");
        childList.add(tablets);
        tablets=new Tablets("PARACETA1");
        childList.add(tablets);
        tablets=new Tablets("PARAFFLI1");
        childList.add(tablets);
        tablets=new Tablets("PETHIDIN1");
        childList.add(tablets);
        tablets=new Tablets("PHENOBEL1");
        childList.add(tablets);
        tablets=new Tablets("PHENOBIN1");
        childList.add(tablets);
        tablets=new Tablets("PHENOBTA1");
        childList.add(tablets);
        tablets=new Tablets("PHENOBTA2");
        childList.add(tablets);
        tablets=new Tablets("PHENOLIN1");
        childList.add(tablets);
        tablets=new Tablets("PHEPENTA1");
        childList.add(tablets);
        tablets=new Tablets("PHENYTIN1");
        childList.add(tablets);
        tablets=new Tablets("PHENYTCA1");
        childList.add(tablets);
        tablets=new Tablets("PHENYTCA2");
        childList.add(tablets);
        tablets=new Tablets("PHENYTTA1");
        childList.add(tablets);
        tablets=new Tablets("PHYTOMIN1");
        childList.add(tablets);
        tablets=new Tablets("PHYTOMIN2");
        childList.add(tablets);
        tablets=new Tablets("PILOCAID1");
        childList.add(tablets);
        tablets=new Tablets("PILOCAID2");
        childList.add(tablets);
        tablets=new Tablets("PIOGLITA1");
        childList.add(tablets);
        tablets=new Tablets("PIOGLITA2");
        childList.add(tablets);
        tablets=new Tablets("PIRACETA1");
        childList.add(tablets);
        tablets=new Tablets("POTCHLIN1");
        childList.add(tablets);
        tablets=new Tablets("POTCHLTA1");
        childList.add(tablets);
        tablets=new Tablets("POTCITMI1");
        childList.add(tablets);
        tablets=new Tablets("POVIDOSO1");
        childList.add(tablets);
        tablets=new Tablets("POVIDOOI1");
        childList.add(tablets);
        tablets=new Tablets("PRAZIQTA1");
        childList.add(tablets);
        tablets=new Tablets("PRAZOSTA1");
        childList.add(tablets);
        tablets=new Tablets("PREDNIID1");
        childList.add(tablets);
        tablets=new Tablets("PREDNIID2");
        childList.add(tablets);
        tablets=new Tablets("PREDNITA1");
        childList.add(tablets);
        tablets=new Tablets("PRIMIDTA1");
        childList.add(tablets);
        tablets=new Tablets("PROBENIN1");
        childList.add(tablets);
        tablets=new Tablets("PROHYDEL1");
        childList.add(tablets);
        tablets=new Tablets("PROHYDIN1");
        childList.add(tablets);
        tablets=new Tablets("PROMETTA1");
        childList.add(tablets);
        tablets=new Tablets("PROTHETA1");
        childList.add(tablets);
        tablets=new Tablets("PROPRAIN1");
        childList.add(tablets);
        tablets=new Tablets("PROPRATA1");
        childList.add(tablets);
        tablets=new Tablets("PROPRATA2");
        childList.add(tablets);
        tablets=new Tablets("PROPRATA3");
        childList.add(tablets);
        tablets=new Tablets("PROPYLTA1");
        childList.add(tablets);
        tablets=new Tablets("PROSULIN1");
        childList.add(tablets);

        groupList=new Pills(14,"p",childList);
        pillsList.add(groupList);


        //Group Q
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("QUINININ1");
        childList.add(tablets);
        tablets=new Tablets("QUININSY1");
        childList.add(tablets);
        tablets=new Tablets("QUININTA1");
        childList.add(tablets);

        groupList=new Pills(15,"q",childList);
        pillsList.add(groupList);


        //Group R
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("RAMIPRTA1");
        childList.add(tablets);
        tablets=new Tablets("RAMIPRTA2");
        childList.add(tablets);
        tablets=new Tablets("RANITITA1");
        childList.add(tablets);
        tablets=new Tablets("RETSOFCA2");
        childList.add(tablets);
        tablets=new Tablets("RINLACSO1");
        childList.add(tablets);
        tablets=new Tablets("RISPERLI1");
        childList.add(tablets);
        tablets=new Tablets("RISPERTA1");
        childList.add(tablets);
        tablets=new Tablets("RISPERTA2");
        childList.add(tablets);
        tablets=new Tablets("RISPERTA3");
        childList.add(tablets);

        groupList=new Pills(16,"r",childList);
        pillsList.add(groupList);


        //Group S
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("SALBUTGA1");
        childList.add(tablets);
        tablets=new Tablets("SALBUTGA2");
        childList.add(tablets);
        tablets=new Tablets("SALBUTGA3");
        childList.add(tablets);
        tablets=new Tablets("SALSULIN1");
        childList.add(tablets);
        tablets=new Tablets("SALBUTSY1");
        childList.add(tablets);
        tablets=new Tablets("SALBUTTA1");
        childList.add(tablets);
        tablets=new Tablets("SALBUTTA2");
        childList.add(tablets);
        tablets=new Tablets("SALACIOI1");
        childList.add(tablets);
        tablets=new Tablets("SECNIDTA1");
        childList.add(tablets);
        tablets=new Tablets("SELSULSH1");
        childList.add(tablets);
        tablets=new Tablets("SERTRATA1");
        childList.add(tablets);
        tablets=new Tablets("SERTRATA2");
        childList.add(tablets);
        tablets=new Tablets("SILSULCR1");
        childList.add(tablets);
        tablets=new Tablets("SIMLINSY1");
        childList.add(tablets);
        tablets=new Tablets("SIMLINSY2");
        childList.add(tablets);
        tablets=new Tablets("SIMVASTA1");
        childList.add(tablets);
        tablets=new Tablets("SIMVASTA2");
        childList.add(tablets);
        tablets=new Tablets("SIMVASTA3");
        childList.add(tablets);
        tablets=new Tablets("SIMVASTA4");
        childList.add(tablets);
        tablets=new Tablets("SODBICIN1");
        childList.add(tablets);
        tablets=new Tablets("SODCHLIN1");
        childList.add(tablets);
        tablets=new Tablets("SODCHLIN3");
        childList.add(tablets);
        tablets=new Tablets("SODCHLND1");
        childList.add(tablets);
        tablets=new Tablets("SODVALCA1");
        childList.add(tablets);
        tablets=new Tablets("SODVALCA2");
        childList.add(tablets);
        tablets=new Tablets("SODVALSY1");
        childList.add(tablets);
        tablets=new Tablets("SODVALTA1");
        childList.add(tablets);
        tablets=new Tablets("SOANSTOI1");
        childList.add(tablets);
        tablets=new Tablets("SOANSTRE1");
        childList.add(tablets);
        tablets=new Tablets("SOOANAOI1");
        childList.add(tablets);
        tablets=new Tablets("SOOANARE1");
        childList.add(tablets);
        tablets=new Tablets("SPIRONTA1");
        childList.add(tablets);
        tablets=new Tablets("SPIRONTA2");
        childList.add(tablets);
        tablets=new Tablets("STREPTIN1");
        childList.add(tablets);
        tablets=new Tablets("STREPTIN2");
        childList.add(tablets);
        tablets=new Tablets("STREPTIN3");
        childList.add(tablets);
        tablets=new Tablets("SULFASTA1");
        childList.add(tablets);

        groupList=new Pills(17,"s",childList);
        pillsList.add(groupList);


        //Group T
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("TAMOXITA1");
        childList.add(tablets);
        tablets=new Tablets("TAMOXITA2");
        childList.add(tablets);
        tablets=new Tablets("TAMSULCA1");
        childList.add(tablets);
        tablets=new Tablets("TERAZOTA1");
        childList.add(tablets);
        tablets=new Tablets("TERAZOTA2");
        childList.add(tablets);
        tablets=new Tablets("TERBINTA1");
        childList.add(tablets);
        tablets=new Tablets("TETRACCA1");
        childList.add(tablets);
        tablets=new Tablets("TETRACEO1");
        childList.add(tablets);
        tablets=new Tablets("TETRACEO2");
        childList.add(tablets);
        tablets=new Tablets("THEOPHTA1");
        childList.add(tablets);
        tablets=new Tablets("THIAMIIN1");
        childList.add(tablets);
        tablets=new Tablets("THIAMITA1");
        childList.add(tablets);
        tablets=new Tablets("THIAMITA2");
        childList.add(tablets);
        tablets=new Tablets("TIABENTA1");
        childList.add(tablets);
        tablets=new Tablets("TIMMALID1");
        childList.add(tablets);
        tablets=new Tablets("TINIDACA1");
        childList.add(tablets);
        tablets=new Tablets("TIROFIIN2");
        childList.add(tablets);
        tablets=new Tablets("TOLBUTTA1");
        childList.add(tablets);
        tablets=new Tablets("TRAACICA1");
        childList.add(tablets);
        tablets=new Tablets("TRAACIIN1");
        childList.add(tablets);
        tablets=new Tablets("TRAACITA1");
        childList.add(tablets);
        tablets=new Tablets("TRIHEXTA1");
        childList.add(tablets);
        tablets=new Tablets("TRIHEXTA2");
        childList.add(tablets);

        groupList=new Pills(18,"t",childList);
        pillsList.add(groupList);




        //Group V
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("VERAPATA1");
        childList.add(tablets);
        tablets=new Tablets("VERAPATA2");
        childList.add(tablets);

        groupList=new Pills(19,"v",childList);
        pillsList.add(groupList);


        //Group W
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("WARFARTA1");
        childList.add(tablets);
        tablets=new Tablets("WARFARTA2");
        childList.add(tablets);
        tablets=new Tablets("WARFARTA3");
        childList.add(tablets);
        tablets=new Tablets("WATFORIN1");
        childList.add(tablets);

        groupList=new Pills(20,"w",childList);
        pillsList.add(groupList);



        //Group Z
        childList=new ArrayList<Tablets>();
        tablets=new Tablets("ZINCOOTA1");
        childList.add(tablets);
        tablets=new Tablets("ZINCOOTA2");
        childList.add(tablets);

        groupList=new Pills(21,"z",childList);
        pillsList.add(groupList);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                getActivity().finish();
                break;
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        AlertDialog.Builder mBuilder=new AlertDialog.Builder(getActivity());
        View mView=getLayoutInflater(null).inflate(R.layout.pop,null);
        mBuilder.setView(mView);
        mBuilder.setCancelable(true);
        mBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog12=mBuilder.create();


        cod=(TextView) mView.findViewById(R.id.txtCode1);
        nam=(TextView) mView.findViewById(R.id.txtName1);
        uni=(TextView) mView.findViewById(R.id.txtUnit1);
        price=(TextView) mView.findViewById(R.id.txtPricing1);
        leve=(TextView) mView.findViewById(R.id.txtLevel1);

        String selected = listAdapter.getChild(groupPosition,childPosition);

        switch (selected) {
            case "ACETAZIN1":

                cod.setText("ACETAZIN1");
                nam.setText("Acetazolamide Injection, 500 mg");
                uni.setText("Ampoule");
                price.setText("GHs 41.25");
                leve.setText("C");

                mBuilder.setView(mView);
                AlertDialog dialog=mBuilder.create();
                dialog.show();
                break;

            case "ACETAZTA1":
                cod.setText("ACETAZTA1");
                nam.setText("Acetazolamide Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.25");
                leve.setText("C");

                mBuilder.setView(mView);
                AlertDialog dialog1=mBuilder.create();
                dialog1.show();
                break;

            case "ACETYLIN1":
                cod.setText("ACETAZTA1");
                nam.setText("Acetylcysteine Injection, 200 mg/mL");
                uni.setText("1 mL");
                price.setText("GHs 78.00");
                leve.setText("C");

                mBuilder.setView(mView);
                AlertDialog dialog2=mBuilder.create();
                dialog2.show();
                break;

            case "ACETYLTA1":
                cod.setText("ACETAZTA1");
                nam.setText("Acetylsalicylic Acid Tablet, 300 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("B1");

                mBuilder.setView(mView);
                AlertDialog dialog3=mBuilder.create();
                dialog3.show();
                break;


            case "ACETYLDT1":
                cod.setText("ACETYLDT1");
                nam.setText("Acetylsalicylic Acid Tablet, 75 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("C");

                mBuilder.setView(mView);
                AlertDialog dialog4=mBuilder.create();
                dialog4.show();
                break;

            case "ACTCHAPO1":
                cod.setText("ACTCHAPO1");
                nam.setText("Activated Charcoal Powder, 50 g");
                uni.setText("50G");
                price.setText("GHs 10.00");
                leve.setText("A");

                mBuilder.setView(mView);
                AlertDialog dialog5=mBuilder.create();
                dialog5.show();
                break;

            case "ACICLOCR1":
                cod.setText("ACICLOCR1");
                nam.setText("Acyclovir Cream, 5%");
                uni.setText("5 G");
                price.setText("GHs 9.50");
                leve.setText("C");

                mBuilder.setView(mView);
                AlertDialog dialog6=mBuilder.create();
                dialog6.show();
                break;

            case "ACICLOEO1":
                cod.setText(getString(R.string.a7));
                nam.setText(getString(R.string.a7i));
                uni.setText(getString(R.string.a7ii));
                price.setText(getString(R.string.a7iii));
                leve.setText(getString(R.string.a7iv));

                mBuilder.setView(mView);
                AlertDialog dialog7=mBuilder.create();
                dialog7.show();
                break;

            case "ACICLOIN1":
                cod.setText(getString(R.string.a8));
                nam.setText(getString(R.string.a8i));
                uni.setText(getString(R.string.a8ii));
                price.setText(getString(R.string.a8iii));
                leve.setText(getString(R.string.a8iv));

                mBuilder.setView(mView);
                AlertDialog dialog8=mBuilder.create();
                dialog8.show();
                break;

                    /*Intent intent8 = new Intent(Medicines.this, MedExp.class);
                    intent8.putExtra("code", getString(R.string.a8));
                    intent8.putExtra("name", getString(R.string.a8i));
                    intent8.putExtra("unit",getString(R.string.a8ii));
                    intent8.putExtra("pricing", getString(R.string.a8iii));
                    intent8.putExtra("level", getString(R.string.a8iv));
                    startActivity(intent8);
                    break;*/

            case "ACICLOSU2":
                cod.setText(getString(R.string.a9));
                nam.setText(getString(R.string.a9i));
                uni.setText(getString(R.string.a9ii));
                price.setText(getString(R.string.a9iii));
                leve.setText(getString(R.string.a9iv));

                mBuilder.setView(mView);
                AlertDialog dialog9=mBuilder.create();
                dialog9.show();
                break;


/*                Intent intent9 = new Intent(Medicines.this, MedExp.class);
                    intent9.putExtra("code", getString(R.string.a9));
                    intent9.putExtra("name", getString(R.string.a9i));
                    intent9.putExtra("unit",getString(R.string.a9ii));
                    intent9.putExtra("pricing", getString(R.string.a9iii));
                    intent9.putExtra("level", getString(R.string.a9iv));
                    startActivity(intent9);
                    break;*/

            case "ACICLOTA1":
                cod.setText(getString(R.string.a10));
                nam.setText(getString(R.string.a10i));
                uni.setText(getString(R.string.a10ii));
                price.setText(getString(R.string.a10iii));
                leve.setText(getString(R.string.a10iv));

                mBuilder.setView(mView);
                AlertDialog dialog10=mBuilder.create();
                dialog10.show();
                break;

                    /*Intent intent10 = new Intent(Medicines.this, MedExp.class);
                    intent10.putExtra("code", getString(R.string.a10));
                    intent10.putExtra("name", getString(R.string.a10i));
                    intent10.putExtra("unit",getString(R.string.a10ii));
                    intent10.putExtra("pricing", getString(R.string.a10iii));
                    intent10.putExtra("level", getString(R.string.a10iv));
                    startActivity(intent10);
                    break;*/

            case "ADRENAIN1":
                cod.setText(getString(R.string.a11));
                nam.setText(getString(R.string.a11i));
                uni.setText(getString(R.string.a11ii));
                price.setText(getString(R.string.a11iii));
                leve.setText(getString(R.string.a11iv));

                mBuilder.setView(mView);
                AlertDialog dialog11=mBuilder.create();
                dialog11.show();
                break;

                    /*Intent intent11 = new Intent(Medicines.this, MedExp.class);
                    intent11.putExtra("code", getString(R.string.a11));
                    intent11.putExtra("name", getString(R.string.a11i));
                    intent11.putExtra("unit",getString(R.string.a11ii));
                    intent11.putExtra("pricing", getString(R.string.a11iii));
                    intent11.putExtra("level", getString(R.string.a11iv));
                    startActivity(intent11);
                    break;*/

            case "ADRENAIN2":
                cod.setText(getString(R.string.a12));
                nam.setText(getString(R.string.a12i));
                uni.setText(getString(R.string.a12ii));
                price.setText(getString(R.string.a12iii));
                leve.setText(getString(R.string.a12iv));

                //mBuilder.setView(mView);
                //AlertDialog dialog12=mBuilder.create();
                dialog12.show();
                break;

                    /*Intent intent12 = new Intent(Medicines.this, MedExp.class);
                    intent12.putExtra("code", getString(R.string.a12));
                    intent12.putExtra("name", getString(R.string.a12i));
                    intent12.putExtra("unit",getString(R.string.a12ii));
                    intent12.putExtra("pricing", getString(R.string.a12iii));
                    intent12.putExtra("level", getString(R.string.a12iv));
                    startActivity(intent12);
                    break;*/

            case "ADRIAMIN1":
                cod.setText(getString(R.string.a13));
                nam.setText(getString(R.string.a13i));
                uni.setText(getString(R.string.a13ii));
                price.setText(getString(R.string.a13iii));
                leve.setText(getString(R.string.a13iv));

                mBuilder.setView(mView);
                AlertDialog dialog13=mBuilder.create();
                dialog13.show();
                break;

                    /*Intent intent13 = new Intent(Medicines.this, MedExp.class);
                    intent13.putExtra("code", getString(R.string.a13));
                    intent13.putExtra("name", getString(R.string.a13i));
                    intent13.putExtra("unit",getString(R.string.a13ii));
                    intent13.putExtra("pricing", getString(R.string.a13iii));
                    intent13.putExtra("level", getString(R.string.a13iv));
                    startActivity(intent13);
                    break;
*/
            case "ALBENDSY1":
                cod.setText(getString(R.string.a14));
                nam.setText(getString(R.string.a14i));
                uni.setText(getString(R.string.a14ii));
                price.setText(getString(R.string.a14iii));
                leve.setText(getString(R.string.a14iv));

                mBuilder.setView(mView);
                AlertDialog dialog14=mBuilder.create();
                dialog14.show();
                break;


               /* Intent intent14 = new Intent(Medicines.this, MedExp.class);
                    intent14.putExtra("code", getString(R.string.a14));
                    intent14.putExtra("name", getString(R.string.a14i));
                    intent14.putExtra("unit",getString(R.string.a14ii));
                    intent14.putExtra("pricing", getString(R.string.a14iii));
                    intent14.putExtra("level", getString(R.string.a14iv));
                    startActivity(intent14);
                    break;*/

            case "ALBENDTA1":

                cod.setText(getString(R.string.a15));
                nam.setText(getString(R.string.a15i));
                uni.setText(getString(R.string.a15ii));
                price.setText(getString(R.string.a15iii));
                leve.setText(getString(R.string.a15iv));

                mBuilder.setView(mView);
                AlertDialog dialog15=mBuilder.create();
                dialog15.show();
                break;


                /*Intent intent15 = new Intent(Medicines.this, MedExp.class);
                    intent15.putExtra("code", getString(R.string.a15));
                    intent15.putExtra("name", getString(R.string.a15i));
                    intent15.putExtra("unit",getString(R.string.a15ii));
                    intent15.putExtra("pricing", getString(R.string.a15iii));
                    intent15.putExtra("level", getString(R.string.a15iv));
                    startActivity(intent15);
                    break;*/

            case "ALBENDTA2":
                cod.setText(getString(R.string.a16));
                nam.setText(getString(R.string.a16i));
                uni.setText(getString(R.string.a16ii));
                price.setText(getString(R.string.a16iii));
                leve.setText(getString(R.string.a16iv));

                mBuilder.setView(mView);
                AlertDialog dialog16=mBuilder.create();
                dialog16.show();
                break;


                /*Intent intent16 = new Intent(Medicines.this, MedExp.class);
                    intent16.putExtra("code", getString(R.string.a16));
                    intent16.putExtra("name", getString(R.string.a16i));
                    intent16.putExtra("unit",getString(R.string.a16ii));
                    intent16.putExtra("pricing", getString(R.string.a16iii));
                    intent16.putExtra("level", getString(R.string.a16iv));
                    startActivity(intent16);
                    break;*/

            case "ALLOPUTA1":
                cod.setText(getString(R.string.a17));
                nam.setText(getString(R.string.a17i));
                uni.setText(getString(R.string.a17ii));
                price.setText(getString(R.string.a17iii));
                leve.setText(getString(R.string.a17iv));

                mBuilder.setView(mView);
                AlertDialog dialog17=mBuilder.create();
                dialog17.show();
                break;


                /*Intent intent17 = new Intent(Medicines.this, MedExp.class);
                    intent17.putExtra("code", getString(R.string.a17));
                    intent17.putExtra("name", getString(R.string.a17i));
                    intent17.putExtra("unit",getString(R.string.a17ii));
                    intent17.putExtra("pricing", getString(R.string.a17iii));
                    intent17.putExtra("level", getString(R.string.a17iv));
                    startActivity(intent17);
                    break;*/

            case "ALLOPUTA2":
                cod.setText(getString(R.string.a18));
                nam.setText(getString(R.string.a18i));
                uni.setText(getString(R.string.a18ii));
                price.setText(getString(R.string.a18iii));
                leve.setText(getString(R.string.a18iv));

                mBuilder.setView(mView);
                AlertDialog dialog18=mBuilder.create();
                dialog18.show();
                break;


                /*Intent intent18 = new Intent(Medicines.this, MedExp.class);
                    intent18.putExtra("code", getString(R.string.a18));
                    intent18.putExtra("name", getString(R.string.a18i));
                    intent18.putExtra("unit",getString(R.string.a18ii));
                    intent18.putExtra("pricing", getString(R.string.a18iii));
                    intent18.putExtra("level", getString(R.string.a18iv));
                    startActivity(intent18);
                    break;*/

            case "AMIACIIN1":
                cod.setText(getString(R.string.a19));
                nam.setText(getString(R.string.a19i));
                uni.setText(getString(R.string.a19ii));
                price.setText(getString(R.string.a19iii));
                leve.setText(getString(R.string.a19iv));

                mBuilder.setView(mView);
                AlertDialog dialog19=mBuilder.create();
                dialog19.show();
                break;


                /*Intent intent19 = new Intent(Medicines.this, MedExp.class);
                    intent19.putExtra("code", getString(R.string.a19));
                    intent19.putExtra("name", getString(R.string.a19i));
                    intent19.putExtra("unit",getString(R.string.a19ii));
                    intent19.putExtra("pricing", getString(R.string.a19iii));
                    intent19.putExtra("level", getString(R.string.a19iv));
                    startActivity(intent19);
                    break;*/

            case "AMIACIIN2":
                cod.setText(getString(R.string.a20));
                nam.setText(getString(R.string.a20i));
                uni.setText(getString(R.string.a20ii));
                price.setText(getString(R.string.a20iii));
                leve.setText(getString(R.string.a20iv));

                mBuilder.setView(mView);
                AlertDialog dialog20=mBuilder.create();
                dialog20.show();
                break;


               /* Intent intent20 = new Intent(Medicines.this, MedExp.class);
                    intent20.putExtra("code", getString(R.string.a20));
                    intent20.putExtra("name", getString(R.string.a20i));
                    intent20.putExtra("unit",getString(R.string.a20ii));
                    intent20.putExtra("pricing", getString(R.string.a20iii));
                    intent20.putExtra("level", getString(R.string.a20iv));
                    startActivity(intent20);
                    break;
*/
            case "AMINOPIN1":
                cod.setText(getString(R.string.a21));
                nam.setText(getString(R.string.a21i));
                uni.setText(getString(R.string.a21ii));
                price.setText(getString(R.string.a21iii));
                leve.setText(getString(R.string.a21iv));

                mBuilder.setView(mView);
                AlertDialog dialog21=mBuilder.create();
                dialog21.show();
                break;


/*                Intent intent21 = new Intent(Medicines.this, MedExp.class);
                    intent21.putExtra("code", getString(R.string.a21));
                    intent21.putExtra("name", getString(R.string.a21i));
                    intent21.putExtra("unit",getString(R.string.a21ii));
                    intent21.putExtra("pricing", getString(R.string.a21iii));
                    intent21.putExtra("level", getString(R.string.a21iv));
                    startActivity(intent21);
                    break;
*/
            case "AMIODATA1":
                cod.setText(getString(R.string.a22));
                nam.setText(getString(R.string.a22i));
                uni.setText(getString(R.string.a22ii));
                price.setText(getString(R.string.a22iii));
                leve.setText(getString(R.string.a22iv));

                mBuilder.setView(mView);
                AlertDialog dialog22=mBuilder.create();
                dialog22.show();
                break;


                /*Intent intent22 = new Intent(Medicines.this, MedExp.class);
                    intent22.putExtra("code", getString(R.string.a22));
                    intent22.putExtra("name", getString(R.string.a22i));
                    intent22.putExtra("unit",getString(R.string.a22ii));
                    intent22.putExtra("pricing", getString(R.string.a22iii));
                    intent22.putExtra("level", getString(R.string.a22iv));
                    startActivity(intent22);
                    break;
*/
            case "AMITRITA1":
                cod.setText(getString(R.string.a23));
                nam.setText(getString(R.string.a23i));
                uni.setText(getString(R.string.a23iii));
                price.setText(getString(R.string.a23ii));
                leve.setText(getString(R.string.a23iv));

                mBuilder.setView(mView);
                AlertDialog dialog23=mBuilder.create();
                dialog23.show();
                break;


               /* Intent intent23 = new Intent(Medicines.this, MedExp.class);
                    intent23.putExtra("code", getString(R.string.a23));
                    intent23.putExtra("name", getString(R.string.a23i));
                    intent23.putExtra("unit",getString(R.string.a23iii));
                    intent23.putExtra("pricing", getString(R.string.a23ii));
                    intent23.putExtra("level", getString(R.string.a23iv));
                    startActivity(intent23);
                    break;
*/
            case "AMITRITA2":
                cod.setText(getString(R.string.a24));
                nam.setText(getString(R.string.a24i));
                uni.setText(getString(R.string.a24ii));
                price.setText(getString(R.string.a24iii));
                leve.setText(getString(R.string.a24iv));

                mBuilder.setView(mView);
                AlertDialog dialog24=mBuilder.create();
                dialog24.show();
                break;


                /*Intent intent24 = new Intent(Medicines.this, MedExp.class);
                    intent24.putExtra("code", getString(R.string.a24));
                    intent24.putExtra("name", getString(R.string.a24i));
                    intent24.putExtra("unit",getString(R.string.a24ii));
                    intent24.putExtra("pricing", getString(R.string.a24iii));
                    intent24.putExtra("level", getString(R.string.a24iv));
                    startActivity(intent24);
                    break;*/

            case "AMITRITA3":
                cod.setText(getString(R.string.a25));
                nam.setText(getString(R.string.a25i));
                uni.setText(getString(R.string.a25ii));
                price.setText(getString(R.string.a25iii));
                leve.setText(getString(R.string.a25iv));

                mBuilder.setView(mView);
                AlertDialog dialog25=mBuilder.create();
                dialog25.show();
                break;


               /* Intent intent25 = new Intent(Medicines.this, MedExp.class);
                    intent25.putExtra("code", getString(R.string.a25));
                    intent25.putExtra("name", getString(R.string.a25i));
                    intent25.putExtra("unit",getString(R.string.a25ii));
                    intent25.putExtra("pricing", getString(R.string.a25iii));
                    intent25.putExtra("level", getString(R.string.a25iv));
                    startActivity(intent25);
                    break;*/

            case "AMLODITA1":
                cod.setText(getString(R.string.a26));
                nam.setText(getString(R.string.a26i));
                uni.setText(getString(R.string.a26ii));
                price.setText(getString(R.string.a26iii));
                leve.setText(getString(R.string.a26iv));

                mBuilder.setView(mView);
                AlertDialog dialog26=mBuilder.create();
                dialog26.show();
                break;


                /*Intent intent26 = new Intent(Medicines.this, MedExp.class);
                    intent26.putExtra("code", getString(R.string.a26));
                    intent26.putExtra("name", getString(R.string.a26i));
                    intent26.putExtra("unit",getString(R.string.a26ii));
                    intent26.putExtra("pricing", getString(R.string.a26iii));
                    intent26.putExtra("level", getString(R.string.a26iv));
                    startActivity(intent26);
                    break;*/

            case "AMLODITA2":
                cod.setText(getString(R.string.a27));
                nam.setText(getString(R.string.a27i));
                uni.setText(getString(R.string.a27ii));
                price.setText(getString(R.string.a27iii));
                leve.setText(getString(R.string.a27iv));

                mBuilder.setView(mView);
                AlertDialog dialog27=mBuilder.create();
                dialog27.show();
                break;


                /*Intent intent27 = new Intent(Medicines.this, MedExp.class);
                    intent27.putExtra("code", getString(R.string.a27));
                    intent27.putExtra("name", getString(R.string.a27i));
                    intent27.putExtra("unit",getString(R.string.a27ii));
                    intent27.putExtra("pricing", getString(R.string.a27iii));
                    intent27.putExtra("level", getString(R.string.a27iv));
                    startActivity(intent27);
                    break;*/

            case "AMOARTPO1":
                cod.setText(getString(R.string.a28));
                nam.setText(getString(R.string.a28i));
                uni.setText(getString(R.string.a28ii));
                price.setText(getString(R.string.a28iii));
                leve.setText(getString(R.string.a28iv));

                mBuilder.setView(mView);
                AlertDialog dialog28=mBuilder.create();
                dialog28.show();
                break;


                /*Intent intent28 = new Intent(Medicines.this, MedExp.class);
                    intent28.putExtra("code", getString(R.string.a28));
                    intent28.putExtra("name", getString(R.string.a28i));
                    intent28.putExtra("unit",getString(R.string.a28ii));
                    intent28.putExtra("pricing", getString(R.string.a28iii));
                    intent28.putExtra("level", getString(R.string.a28iv));
                    startActivity(intent28);*/

            case "AMOARTPO2":
                cod.setText(getString(R.string.a29));
                nam.setText(getString(R.string.a29i));
                uni.setText(getString(R.string.a29ii));
                price.setText(getString(R.string.a29iii));
                leve.setText(getString(R.string.a29iv));

                mBuilder.setView(mView);
                AlertDialog dialog29=mBuilder.create();
                dialog29.show();
                break;


                /*Intent intent29 = new Intent(Medicines.this, MedExp.class);
                    intent29.putExtra("code", getString(R.string.a29));
                    intent29.putExtra("name", getString(R.string.a29i));
                    intent29.putExtra("unit",getString(R.string.a29ii));
                    intent29.putExtra("pricing", getString(R.string.a29iii));
                    intent29.putExtra("level", getString(R.string.a29iv));
                    startActivity(intent29);
                    break;
                */

            case "AMOARTTA1":
                cod.setText(getString(R.string.a30));
                nam.setText(getString(R.string.a30i));
                uni.setText(getString(R.string.a30ii));
                price.setText(getString(R.string.a30iii));
                leve.setText(getString(R.string.a30iv));

                mBuilder.setView(mView);
                AlertDialog dialog30=mBuilder.create();
                dialog30.show();
                break;


                /*Intent intent30 = new Intent(Medicines.this, MedExp.class);
                    intent30.putExtra("code", getString(R.string.a30));
                    intent30.putExtra("name", getString(R.string.a30i));
                    intent30.putExtra("unit",getString(R.string.a30ii));
                    intent30.putExtra("pricing", getString(R.string.a30iii));
                    intent30.putExtra("level", getString(R.string.a30iv));
                    startActivity(intent30);
                    break;*/

            case "AMOARTTA2":
                cod.setText(getString(R.string.a31));
                nam.setText(getString(R.string.a31i));
                uni.setText(getString(R.string.a31ii));
                price.setText(getString(R.string.a31iii));
                leve.setText(getString(R.string.a31iv));

                mBuilder.setView(mView);
                AlertDialog dialog31=mBuilder.create();
                dialog31.show();
                break;


                /*Intent intent31 = new Intent(Medicines.this, MedExp.class);
                    intent31.putExtra("code", getString(R.string.a31));
                    intent31.putExtra("name", getString(R.string.a31i));
                    intent31.putExtra("unit",getString(R.string.a31ii));
                    intent31.putExtra("pricing", getString(R.string.a31iii));
                    intent31.putExtra("level", getString(R.string.a31iv));
                    startActivity(intent31);
                    break;*/

            case "AMOXICCA1":
                cod.setText(getString(R.string.a32));
                nam.setText(getString(R.string.a32i));
                uni.setText(getString(R.string.a32ii));
                price.setText(getString(R.string.a32iii));
                leve.setText(getString(R.string.a32iv));

                mBuilder.setView(mView);
                AlertDialog dialog32=mBuilder.create();
                dialog32.show();
                break;

                /*Intent intent32 = new Intent(Medicines.this, MedExp.class);
                    intent32.putExtra("code", getString(R.string.a32));
                    intent32.putExtra("name", getString(R.string.a32i));
                    intent32.putExtra("unit",getString(R.string.a32ii));
                    intent32.putExtra("pricing", getString(R.string.a32iii));
                    intent32.putExtra("level", getString(R.string.a32iv));
                    startActivity(intent32);
                    break;*/

            case "AMOXICCA2":
                cod.setText(getString(R.string.a33));
                nam.setText(getString(R.string.a33i));
                uni.setText(getString(R.string.a33ii));
                price.setText(getString(R.string.a33iii));
                leve.setText(getString(R.string.a33iv));

                mBuilder.setView(mView);
                AlertDialog dialog33=mBuilder.create();
                dialog33.show();
                break;


                /*Intent intent33 = new Intent(Medicines.this, MedExp.class);
                    intent33.putExtra("code", getString(R.string.a33));
                    intent33.putExtra("name", getString(R.string.a33i));
                    intent33.putExtra("unit",getString(R.string.a33ii));
                    intent33.putExtra("pricing", getString(R.string.a33iii));
                    intent33.putExtra("level", getString(R.string.a33iv));
                    startActivity(intent33);
                    break;*/

            case "AMOXICSU1":
                cod.setText(getString(R.string.a34));
                nam.setText(getString(R.string.a34i));
                uni.setText(getString(R.string.a34ii));
                price.setText(getString(R.string.a34iii));
                leve.setText(getString(R.string.a34iv));

                mBuilder.setView(mView);
                AlertDialog dialog34=mBuilder.create();
                dialog34.show();
                break;


                /*Intent intent34 = new Intent(Medicines.this, MedExp.class);
                    intent34.putExtra("code", getString(R.string.a34));
                    intent34.putExtra("name", getString(R.string.a34i));
                    intent34.putExtra("unit",getString(R.string.a34ii));
                    intent34.putExtra("pricing", getString(R.string.a34iii));
                    intent34.putExtra("level", getString(R.string.a34iv));
                    startActivity(intent34);
                    break;*/

            case "AMPICIIN1":
                cod.setText(getString(R.string.a35));
                nam.setText(getString(R.string.a35i));
                uni.setText(getString(R.string.a35ii));
                price.setText(getString(R.string.a35iii));
                leve.setText(getString(R.string.a35iv));

                mBuilder.setView(mView);
                AlertDialog dialog35=mBuilder.create();
                dialog35.show();
                break;


                /*Intent intent35 = new Intent(Medicines.this, MedExp.class);
                    intent35.putExtra("code", getString(R.string.a35));
                    intent35.putExtra("name", getString(R.string.a35i));
                    intent35.putExtra("unit",getString(R.string.a35ii));
                    intent35.putExtra("pricing", getString(R.string.a35iii));
                    intent35.putExtra("level", getString(R.string.a35iv));
                    startActivity(intent35);
                    break;*/

            case "ANASTRTA1":
                cod.setText(getString(R.string.a36));
                nam.setText(getString(R.string.a36i));
                uni.setText(getString(R.string.a36ii));
                price.setText(getString(R.string.a36iii));
                leve.setText(getString(R.string.a36iv));

                mBuilder.setView(mView);
                AlertDialog dialog36=mBuilder.create();
                dialog36.show();
                break;


                /*Intent intent36 = new Intent(Medicines.this, MedExp.class);
                    intent36.putExtra("code", getString(R.string.a36));
                    intent36.putExtra("name", getString(R.string.a36i));
                    intent36.putExtra("unit",getString(R.string.a36ii));
                    intent36.putExtra("pricing", getString(R.string.a36iii));
                    intent36.putExtra("level", getString(R.string.a36iv));
                    startActivity(intent36);
                    break;*/

            case "ANIMGLIN1":
                cod.setText(getString(R.string.a37));
                nam.setText(getString(R.string.a37i));
                uni.setText(getString(R.string.a37ii));
                price.setText(getString(R.string.a37iii));
                leve.setText(getString(R.string.a37iv));

                mBuilder.setView(mView);
                AlertDialog dialog37=mBuilder.create();
                dialog37.show();
                break;


                /*Intent intent37 = new Intent(Medicines.this, MedExp.class);
                    intent37.putExtra("code", getString(R.string.a37));
                    intent37.putExtra("name", getString(R.string.a37i));
                    intent37.putExtra("unit",getString(R.string.a37ii));
                    intent37.putExtra("pricing", getString(R.string.a37iii));
                    intent37.putExtra("level", getString(R.string.a37iv));
                    startActivity(intent37);
                    break;*/

            case "AQUEOUCR1":
                cod.setText(getString(R.string.a38));
                nam.setText(getString(R.string.a38i));
                uni.setText(getString(R.string.a38ii));
                price.setText(getString(R.string.a38iii));
                leve.setText(getString(R.string.a38iv));

                mBuilder.setView(mView);
                AlertDialog dialog38=mBuilder.create();
                dialog38.show();
                break;


                /*Intent intent38 = new Intent(Medicines.this, MedExp.class);
                    intent38.putExtra("code", getString(R.string.a38));
                    intent38.putExtra("name", getString(R.string.a38i));
                    intent38.putExtra("unit",getString(R.string.a38ii));
                    intent38.putExtra("pricing", getString(R.string.a38iii));
                    intent38.putExtra("level", getString(R.string.a38iv));
                    startActivity(intent38);
                    break;*/

            case "ARTLUMDT1":
                cod.setText(getString(R.string.a39));
                nam.setText(getString(R.string.a39i));
                uni.setText(getString(R.string.a39ii));
                price.setText(getString(R.string.a39iii));
                leve.setText(getString(R.string.a39iv));

                mBuilder.setView(mView);
                AlertDialog dialog39=mBuilder.create();
                dialog39.show();
                break;


                /*Intent intent39 = new Intent(Medicines.this, MedExp.class);
                    intent39.putExtra("code", getString(R.string.a39));
                    intent39.putExtra("name", getString(R.string.a39i));
                    intent39.putExtra("unit",getString(R.string.a39ii));
                    intent39.putExtra("pricing", getString(R.string.a39iii));
                    intent39.putExtra("level", getString(R.string.a39iv));
                    startActivity(intent39);
                    break;*/

            case "ARTLUMSU1":
                cod.setText(getString(R.string.a40));
                nam.setText(getString(R.string.a40i));
                uni.setText(getString(R.string.a40ii));
                price.setText(getString(R.string.a40iii));
                leve.setText(getString(R.string.a40iv));

                mBuilder.setView(mView);
                AlertDialog dialog40=mBuilder.create();
                dialog40.show();
                break;


                /*Intent intent40 = new Intent(Medicines.this, MedExp.class);
                    intent40.putExtra("code", getString(R.string.a40));
                    intent40.putExtra("name", getString(R.string.a40i));
                    intent40.putExtra("unit",getString(R.string.a40ii));
                    intent40.putExtra("pricing", getString(R.string.a40iii));
                    intent40.putExtra("level", getString(R.string.a40iv));
                    startActivity(intent40);
                    break;*/

            case "ARTLUMTA1":
                cod.setText(getString(R.string.a41));
                nam.setText(getString(R.string.a41i));
                uni.setText(getString(R.string.a41ii));
                price.setText(getString(R.string.a41iii));
                leve.setText(getString(R.string.a41iv));

                mBuilder.setView(mView);
                AlertDialog dialog41=mBuilder.create();
                dialog41.show();
                break;


                /*Intent intent41 = new Intent(Medicines.this, MedExp.class);
                    intent41.putExtra("code", getString(R.string.a41));
                    intent41.putExtra("name", getString(R.string.a41i));
                    intent41.putExtra("unit",getString(R.string.a41ii));
                    intent41.putExtra("pricing", getString(R.string.a41iii));
                    intent41.putExtra("level", getString(R.string.a41iv));
                    startActivity(intent41);
                    break;*/

            case "ARTESUIN1":
                cod.setText(getString(R.string.a42));
                nam.setText(getString(R.string.a42i));
                uni.setText(getString(R.string.a42ii));
                price.setText(getString(R.string.a42iii));
                leve.setText(getString(R.string.a42iv));

                mBuilder.setView(mView);
                AlertDialog dialog42=mBuilder.create();
                dialog42.show();
                break;


                /*Intent intent42 = new Intent(Medicines.this, MedExp.class);
                    intent42.putExtra("code", getString(R.string.a42));
                    intent42.putExtra("name", getString(R.string.a42i));
                    intent42.putExtra("unit",getString(R.string.a42ii));
                    intent42.putExtra("pricing", getString(R.string.a42iii));
                    intent42.putExtra("level", getString(R.string.a42iv));
                    startActivity(intent42);
                    break;*/

            case "ARTESUIN2":
                cod.setText(getString(R.string.a43));
                nam.setText(getString(R.string.a43i));
                uni.setText(getString(R.string.a43ii));
                price.setText(getString(R.string.a43iii));
                leve.setText(getString(R.string.a43iv));

                mBuilder.setView(mView);
                AlertDialog dialog43=mBuilder.create();
                dialog43.show();
                break;


                /*Intent intent43 = new Intent(Medicines.this, MedExp.class);
                    intent43.putExtra("code", getString(R.string.a43));
                    intent43.putExtra("name", getString(R.string.a43i));
                    intent43.putExtra("unit",getString(R.string.a43ii));
                    intent43.putExtra("pricing", getString(R.string.a43iii));
                    intent43.putExtra("level", getString(R.string.a43iv));
                    startActivity(intent43);
                    break;*/

            case "ARTESURE1":
                cod.setText(getString(R.string.a44));
                nam.setText(getString(R.string.a44i));
                uni.setText(getString(R.string.a44ii));
                price.setText(getString(R.string.a44iii));
                leve.setText(getString(R.string.a44iv));

                mBuilder.setView(mView);
                AlertDialog dialog44=mBuilder.create();
                dialog44.show();
                break;


                /*Intent intent44 = new Intent(Medicines.this, MedExp.class);
                    intent44.putExtra("code", getString(R.string.a44));
                    intent44.putExtra("name", getString(R.string.a44i));
                    intent44.putExtra("unit",getString(R.string.a44ii));
                    intent44.putExtra("pricing", getString(R.string.a44iii));
                    intent44.putExtra("level", getString(R.string.a44iv));
                    startActivity(intent44);
                    break;*/

            case "ARTESURE2":
                cod.setText(getString(R.string.a45));
                nam.setText(getString(R.string.a45i));
                uni.setText(getString(R.string.a45ii));
                price.setText(getString(R.string.a45iii));
                leve.setText(getString(R.string.a45iv));

                mBuilder.setView(mView);
                AlertDialog dialog45=mBuilder.create();
                dialog45.show();
                break;


                /*Intent intent45 = new Intent(Medicines.this, MedExp.class);
                    intent45.putExtra("code", getString(R.string.a45));
                    intent45.putExtra("name", getString(R.string.a45i));
                    intent45.putExtra("unit",getString(R.string.a45ii));
                    intent45.putExtra("pricing", getString(R.string.a45iii));
                    intent45.putExtra("level", getString(R.string.a45iv));
                    startActivity(intent45);
                    break;*/

            case "ATEHYDTA1":
                cod.setText(getString(R.string.a46));
                nam.setText(getString(R.string.a46i));
                uni.setText(getString(R.string.a46ii));
                price.setText(getString(R.string.a46iii));
                leve.setText(getString(R.string.a46iv));

                mBuilder.setView(mView);
                AlertDialog dialog46=mBuilder.create();
                dialog46.show();
                break;


                /*Intent intent46 = new Intent(Medicines.this, MedExp.class);
                    intent46.putExtra("code", getString(R.string.a46));
                    intent46.putExtra("name", getString(R.string.a46i));
                    intent46.putExtra("unit",getString(R.string.a46ii));
                    intent46.putExtra("pricing", getString(R.string.a46iii));
                    intent46.putExtra("level", getString(R.string.a46iv));
                    startActivity(intent46);
                    break;*/

            case "ATEHYDTA2":
                cod.setText(getString(R.string.a47));
                nam.setText(getString(R.string.a47i));
                uni.setText(getString(R.string.a47ii));
                price.setText(getString(R.string.a47iii));
                leve.setText(getString(R.string.a47iv));

                mBuilder.setView(mView);
                AlertDialog dialog47=mBuilder.create();
                dialog47.show();
                break;


                /*Intent intent47 = new Intent(Medicines.this, MedExp.class);
                    intent47.putExtra("code", getString(R.string.a47));
                    intent47.putExtra("name", getString(R.string.a47i));
                    intent47.putExtra("unit",getString(R.string.a47ii));
                    intent47.putExtra("pricing", getString(R.string.a47iii));
                    intent47.putExtra("level", getString(R.string.a47iv));
                    startActivity(intent47);
                    break;*/

            case "ATENOLIN1":
                cod.setText(getString(R.string.a48));
                nam.setText(getString(R.string.a48i));
                uni.setText(getString(R.string.a48ii));
                price.setText(getString(R.string.a48iii));
                leve.setText(getString(R.string.a48iv));

                mBuilder.setView(mView);
                AlertDialog dialog48=mBuilder.create();
                dialog48.show();
                break;


                /*Intent intent48 = new Intent(Medicines.this, MedExp.class);
                    intent48.putExtra("code", getString(R.string.a48));
                    intent48.putExtra("name", getString(R.string.a48i));
                    intent48.putExtra("unit",getString(R.string.a48ii));
                    intent48.putExtra("pricing", getString(R.string.a48iii));
                    intent48.putExtra("level", getString(R.string.a48iv));
                    startActivity(intent48);
                    break;*/

            case "ATENOLTA1":
                cod.setText(getString(R.string.a49));
                nam.setText(getString(R.string.a49i));
                uni.setText(getString(R.string.a49ii));
                price.setText(getString(R.string.a49iii));
                leve.setText(getString(R.string.a49iv));

                mBuilder.setView(mView);
                AlertDialog dialog49=mBuilder.create();
                dialog49.show();
                break;


                /*Intent intent49 = new Intent(Medicines.this, MedExp.class);
                    intent49.putExtra("code", getString(R.string.a49));
                    intent49.putExtra("name", getString(R.string.a49i));
                    intent49.putExtra("unit",getString(R.string.a49ii));
                    intent49.putExtra("pricing", getString(R.string.a49iii));
                    intent49.putExtra("level", getString(R.string.a49iv));
                    startActivity(intent49);
                    break;*/

            case "ATENOLTA2":
                cod.setText(getString(R.string.a50));
                nam.setText(getString(R.string.a50i));
                uni.setText(getString(R.string.a50ii));
                price.setText(getString(R.string.a50iii));
                leve.setText(getString(R.string.a50iv));

                mBuilder.setView(mView);
                AlertDialog dialog50=mBuilder.create();
                dialog50.show();
                break;


                /*Intent intent50 = new Intent(Medicines.this, MedExp.class);
                    intent50.putExtra("code", getString(R.string.a50));
                    intent50.putExtra("name", getString(R.string.a50i));
                    intent50.putExtra("unit",getString(R.string.a50ii));
                    intent50.putExtra("pricing", getString(R.string.a50iii));
                    intent50.putExtra("level", getString(R.string.a50iv));
                    startActivity(intent50);
                    break;*/

            case "ATENOLTA3":
                cod.setText(getString(R.string.a51));
                nam.setText(getString(R.string.a51i));
                uni.setText(getString(R.string.a51ii));
                price.setText(getString(R.string.a51iii));
                leve.setText(getString(R.string.a51iv));

                mBuilder.setView(mView);
                AlertDialog dialog51=mBuilder.create();
                dialog51.show();
                break;


                /*Intent intent51 = new Intent(Medicines.this, MedExp.class);
                    intent51.putExtra("code", getString(R.string.a51));
                    intent51.putExtra("name", getString(R.string.a51i));
                    intent51.putExtra("unit",getString(R.string.a51ii));
                    intent51.putExtra("pricing", getString(R.string.a51iii));
                    intent51.putExtra("level", getString(R.string.a51iv));
                    startActivity(intent51);
                    break;*/

            case "ATORVATA1":
                cod.setText(getString(R.string.a52));
                nam.setText(getString(R.string.a52i));
                uni.setText(getString(R.string.a52ii));
                price.setText(getString(R.string.a52iii));
                leve.setText(getString(R.string.a52iv));

                mBuilder.setView(mView);
                AlertDialog dialog52=mBuilder.create();
                dialog52.show();
                break;


                /*Intent intent52 = new Intent(Medicines.this, MedExp.class);
                    intent52.putExtra("code", getString(R.string.a52));
                    intent52.putExtra("name", getString(R.string.a52i));
                    intent52.putExtra("unit",getString(R.string.a52ii));
                    intent52.putExtra("pricing", getString(R.string.a52iii));
                    intent52.putExtra("level", getString(R.string.a52iv));
                    startActivity(intent52);
                    break;*/

            case "ATORVATA2":
                cod.setText(getString(R.string.a53));
                nam.setText(getString(R.string.a53i));
                uni.setText(getString(R.string.a53ii));
                price.setText(getString(R.string.a53iii));
                leve.setText(getString(R.string.a32iv));

                mBuilder.setView(mView);
                AlertDialog dialog53=mBuilder.create();
                dialog53.show();
                break;


                /*Intent intent53 = new Intent(Medicines.this, MedExp.class);
                    intent53.putExtra("code", getString(R.string.a53));
                    intent53.putExtra("name", getString(R.string.a53i));
                    intent53.putExtra("unit",getString(R.string.a53ii));
                    intent53.putExtra("pricing", getString(R.string.a53iii));
                    intent53.putExtra("level", getString(R.string.a53iv));
                    startActivity(intent53);
                    break;*/

            case "ATROPIID1":
                cod.setText(getString(R.string.a54));
                nam.setText(getString(R.string.a54i));
                uni.setText(getString(R.string.a54ii));
                price.setText(getString(R.string.a54iii));
                leve.setText(getString(R.string.a54iv));

                mBuilder.setView(mView);
                AlertDialog dialog54=mBuilder.create();
                dialog54.show();
                break;


                /*Intent intent54 = new Intent(Medicines.this, MedExp.class);
                    intent54.putExtra("code", getString(R.string.a54));
                    intent54.putExtra("name", getString(R.string.a54i));
                    intent54.putExtra("unit",getString(R.string.a54ii));
                    intent54.putExtra("pricing", getString(R.string.a54iii));
                    intent54.putExtra("level", getString(R.string.a54iv));
                    startActivity(intent54);
                    break;*/

            case "ATROPIIN1":
                cod.setText(getString(R.string.a55));
                nam.setText(getString(R.string.a55i));
                uni.setText(getString(R.string.a55ii));
                price.setText(getString(R.string.a55iii));
                leve.setText(getString(R.string.a55iv));

                mBuilder.setView(mView);
                AlertDialog dialog55=mBuilder.create();
                dialog55.show();
                break;


                /*Intent intent55 = new Intent(Medicines.this, MedExp.class);
                    intent55.putExtra("code", getString(R.string.a55));
                    intent55.putExtra("name", getString(R.string.a55i));
                    intent55.putExtra("unit",getString(R.string.a55ii));
                    intent55.putExtra("pricing", getString(R.string.a55iii));
                    intent55.putExtra("level", getString(R.string.a55iv));
                    startActivity(intent55);
                    break;*/

            case "AZITHRCA1":
                cod.setText(getString(R.string.a56));
                nam.setText(getString(R.string.a56i));
                uni.setText(getString(R.string.a56ii));
                price.setText(getString(R.string.a56iii));
                leve.setText(getString(R.string.a56iv));

                mBuilder.setView(mView);
                AlertDialog dialog56=mBuilder.create();
                dialog56.show();
                break;


                /*Intent intent56 = new Intent(Medicines.this, MedExp.class);
                    intent56.putExtra("code", getString(R.string.a56));
                    intent56.putExtra("name", getString(R.string.a56i));
                    intent56.putExtra("unit",getString(R.string.a56ii));
                    intent56.putExtra("pricing", getString(R.string.a56iii));
                    intent56.putExtra("level", getString(R.string.a56iv));
                    startActivity(intent56);
                    break;*/

            case "AZITHRSU1":
                cod.setText(getString(R.string.a57));
                nam.setText(getString(R.string.a57i));
                uni.setText(getString(R.string.a57ii));
                price.setText(getString(R.string.a57iii));
                leve.setText(getString(R.string.a57iv));

                mBuilder.setView(mView);
                AlertDialog dialog57=mBuilder.create();
                dialog57.show();
                break;


                /*Intent intent57 = new Intent(Medicines.this, MedExp.class);
                    intent57.putExtra("code", getString(R.string.a57));
                    intent57.putExtra("name", getString(R.string.a57i));
                    intent57.putExtra("unit",getString(R.string.a57ii));
                    intent57.putExtra("pricing", getString(R.string.a57iii));
                    intent57.putExtra("level", getString(R.string.a57iv));
                    startActivity(intent57);
                    break;*/

            case "AZITHRSU2":
                cod.setText(getString(R.string.a58));
                nam.setText(getString(R.string.a58i));
                uni.setText(getString(R.string.a58ii));
                price.setText(getString(R.string.a58iii));
                leve.setText(getString(R.string.a58iv));

                mBuilder.setView(mView);
                AlertDialog dialog58=mBuilder.create();
                dialog58.show();
                break;


                /*Intent intent58 = new Intent(Medicines.this, MedExp.class);
                    intent58.putExtra("code", getString(R.string.a58));
                    intent58.putExtra("name", getString(R.string.a58i));
                    intent58.putExtra("unit",getString(R.string.a58ii));
                    intent58.putExtra("pricing", getString(R.string.a58iii));
                    intent58.putExtra("level", getString(R.string.a58iv));
                    startActivity(intent58);
                    break;*/
        }

        //}
        //else if(groupPosition==1){
        switch (selected){
            case "BADOESIN1":
                cod.setText(getString(R.string.b));
                nam.setText(getString(R.string.bi));
                uni.setText(getString(R.string.bii));
                price.setText(getString(R.string.biii));
                leve.setText(getString(R.string.biv));

                mBuilder.setView(mView);
                AlertDialog dialog=mBuilder.create();
                dialog.show();
                break;


                /*Intent intent = new Intent(Medicines.this, MedExp.class);
                    intent.putExtra("code", getString(R.string.b));
                    intent.putExtra("name", getString(R.string.bi));
                    intent.putExtra("unit",getString(R.string.bii));
                    intent.putExtra("pricing", getString(R.string.biii));
                    intent.putExtra("level", getString(R.string.biv));
                    startActivity(intent);
                    break;*/

            case "BECDIPGA1":
                cod.setText(getString(R.string.b1));
                nam.setText(getString(R.string.b1i));
                uni.setText(getString(R.string.b1ii));
                price.setText(getString(R.string.b1iii));
                leve.setText(getString(R.string.b1iv));

                mBuilder.setView(mView);
                AlertDialog dialog1=mBuilder.create();
                dialog1.show();
                break;


                /*Intent intent1 = new Intent(Medicines.this, MedExp.class);
                    intent1.putExtra("code", getString(R.string.b1));
                    intent1.putExtra("name", getString(R.string.b1i));
                    intent1.putExtra("unit",getString(R.string.b1ii));
                    intent1.putExtra("pricing", getString(R.string.b1iii));
                    intent1.putExtra("level", getString(R.string.b1iv));
                    startActivity(intent1);
                    break;*/

            case "BECDIPGA2":
                cod.setText(getString(R.string.b2));
                nam.setText(getString(R.string.b2i));
                uni.setText(getString(R.string.b2ii));
                price.setText(getString(R.string.b2iii));
                leve.setText(getString(R.string.b2iv));

                mBuilder.setView(mView);
                AlertDialog dialog2=mBuilder.create();
                dialog2.show();
                break;


            case "BECDIPGA3":
                cod.setText(getString(R.string.b3));
                nam.setText(getString(R.string.b3i));
                uni.setText(getString(R.string.b3ii));
                price.setText(getString(R.string.b3iii));
                leve.setText(getString(R.string.b3iv));

                mBuilder.setView(mView);
                AlertDialog dialog3=mBuilder.create();
                dialog3.show();
                break;

            case "BENDROTA1":
                cod.setText(getString(R.string.b4));
                nam.setText(getString(R.string.b4i));
                uni.setText(getString(R.string.b4ii));
                price.setText(getString(R.string.b4iii));
                leve.setText(getString(R.string.b4iv));

                mBuilder.setView(mView);
                AlertDialog dialog4=mBuilder.create();
                dialog4.show();
                break;



            case "BENDROTA2":
                cod.setText(getString(R.string.b5));
                nam.setText(getString(R.string.b5i));
                uni.setText(getString(R.string.b5ii));
                price.setText(getString(R.string.b5iii));
                leve.setText(getString(R.string.b5iv));

                mBuilder.setView(mView);
                AlertDialog dialog5=mBuilder.create();
                dialog5.show();
                break;

            case "BENZATIN1":
                cod.setText(getString(R.string.b6));
                nam.setText(getString(R.string.b6i));
                uni.setText(getString(R.string.b6ii));
                price.setText(getString(R.string.b6iii));
                leve.setText(getString(R.string.b6iv));

                mBuilder.setView(mView);
                AlertDialog dialog6=mBuilder.create();
                dialog6.show();
                break;


            case "BENZATTA1":
                cod.setText(getString(R.string.b7));
                nam.setText(getString(R.string.b7i));
                uni.setText(getString(R.string.b7ii));
                price.setText(getString(R.string.b7iii));
                leve.setText(getString(R.string.b7iv));

                mBuilder.setView(mView);
                AlertDialog dialog7=mBuilder.create();
                dialog7.show();
                break;

            case "BEACSAOI1":
                cod.setText(getString(R.string.b8));
                nam.setText(getString(R.string.b8i));
                uni.setText(getString(R.string.b8ii));
                price.setText(getString(R.string.b8iii));
                leve.setText(getString(R.string.b8iv));

                mBuilder.setView(mView);
                AlertDialog dialog8=mBuilder.create();
                dialog8.show();
                break;


            case "BENPERCR1":
                cod.setText(getString(R.string.b9));
                nam.setText(getString(R.string.b9i));
                uni.setText(getString(R.string.b9ii));
                price.setText(getString(R.string.b9iii));
                leve.setText(getString(R.string.b9iv));

                mBuilder.setView(mView);
                AlertDialog dialog9=mBuilder.create();
                dialog9.show();
                break;


            case "BENPERCR2":
                cod.setText(getString(R.string.b10));
                nam.setText(getString(R.string.b10i));
                uni.setText(getString(R.string.b10ii));
                price.setText(getString(R.string.b10iii));
                leve.setText(getString(R.string.b10iv));

                mBuilder.setView(mView);
                AlertDialog dialog10=mBuilder.create();
                dialog10.show();
                break;


            case "BENBENLO1":
                cod.setText(getString(R.string.b11));
                nam.setText(getString(R.string.b11i));
                uni.setText(getString(R.string.b11ii));
                price.setText(getString(R.string.b11iii));
                leve.setText(getString(R.string.b11iv));

                mBuilder.setView(mView);
                AlertDialog dialog11=mBuilder.create();
                dialog11.show();
                break;



            case "BENBENLO2":
                cod.setText(getString(R.string.b12));
                nam.setText(getString(R.string.b12i));
                uni.setText(getString(R.string.b12ii));
                price.setText(getString(R.string.b12iii));
                leve.setText(getString(R.string.b12iv));

                mBuilder.setView(mView);
                //AlertDialog dialog12=mBuilder.create();
                dialog12.show();
                break;


                /*Intent intent12 = new Intent(Medicines.this, MedExp.class);
                    intent12.putExtra("code", getString(R.string.b12));
                    intent12.putExtra("name", getString(R.string.b12i));
                    intent12.putExtra("unit",getString(R.string.b12ii));
                    intent12.putExtra("pricing", getString(R.string.b12iii));
                    intent12.putExtra("level", getString(R.string.b12iv));
                    startActivity(intent12);
                    break;*/

            case "BENZYLIN1":
                cod.setText(getString(R.string.b13));
                nam.setText(getString(R.string.b13i));
                uni.setText(getString(R.string.b13ii));
                price.setText(getString(R.string.b13iii));
                leve.setText(getString(R.string.b13iv));

                mBuilder.setView(mView);
                AlertDialog dialog13=mBuilder.create();
                dialog13.show();
                break;


            case "BENZYLIN2":
                cod.setText(getString(R.string.b14));
                nam.setText(getString(R.string.b14i));
                uni.setText(getString(R.string.b14ii));
                price.setText(getString(R.string.b14iii));
                leve.setText(getString(R.string.b14iv));

                mBuilder.setView(mView);
                AlertDialog dialog14=mBuilder.create();
                dialog14.show();
                break;

            case "BETVALCR2":
                cod.setText(getString(R.string.b15));
                nam.setText(getString(R.string.b15i));
                uni.setText(getString(R.string.b15ii));
                price.setText(getString(R.string.b15iii));
                leve.setText(getString(R.string.b15iv));

                mBuilder.setView(mView);
                AlertDialog dialog15=mBuilder.create();
                dialog15.show();
                break;


            case "BETAXOID1":
                cod.setText(getString(R.string.b16));
                nam.setText(getString(R.string.b16i));
                uni.setText(getString(R.string.b16ii));
                price.setText(getString(R.string.b16iii));
                leve.setText(getString(R.string.b16iv));

                mBuilder.setView(mView);
                AlertDialog dialog16=mBuilder.create();
                dialog16.show();
                break;


            case "BISACOTA1":
                cod.setText(getString(R.string.b17));
                nam.setText(getString(R.string.b17i));
                uni.setText(getString(R.string.b17ii));
                price.setText(getString(R.string.b17iii));
                leve.setText(getString(R.string.b17iv));

                mBuilder.setView(mView);
                AlertDialog dialog17=mBuilder.create();
                dialog17.show();
                break;


                /*Intent intent17 = new Intent(Medicines.this, MedExp.class);
                    intent17.putExtra("code", getString(R.string.b17));
                    intent17.putExtra("name", getString(R.string.b17i));
                    intent17.putExtra("unit",getString(R.string.b17ii));
                    intent17.putExtra("pricing", getString(R.string.b17iii));
                    intent17.putExtra("level", getString(R.string.b17iv));
                    startActivity(intent17);
                    break;*/

            case "BROMOCTA1":
                cod.setText(getString(R.string.b18));
                nam.setText(getString(R.string.b18i));
                uni.setText(getString(R.string.b18ii));
                price.setText(getString(R.string.b18iii));
                leve.setText(getString(R.string.b18iv));

                mBuilder.setView(mView);
                AlertDialog dialog18=mBuilder.create();
                dialog18.show();
                break;


                /*Intent intent18 = new Intent(Medicines.this, MedExp.class);
                    intent18.putExtra("code", getString(R.string.b18));
                    intent18.putExtra("name", getString(R.string.b18i));
                    intent18.putExtra("unit",getString(R.string.b18ii));
                    intent18.putExtra("pricing", getString(R.string.b18iii));
                    intent18.putExtra("level", getString(R.string.b18iv));
                    startActivity(intent18);
                    break;*/

            case "BUDFORGA2":
                cod.setText(getString(R.string.b19));
                nam.setText(getString(R.string.b19i));
                uni.setText(getString(R.string.b19ii));
                price.setText(getString(R.string.b19iii));
                leve.setText(getString(R.string.b19iv));

                mBuilder.setView(mView);
                AlertDialog dialog19=mBuilder.create();
                dialog19.show();
                break;


               /* Intent intent19 = new Intent(Medicines.this, MedExp.class);
                    intent19.putExtra("code", getString(R.string.b19));
                    intent19.putExtra("name", getString(R.string.b19i));
                    intent19.putExtra("unit",getString(R.string.b19ii));
                    intent19.putExtra("pricing", getString(R.string.b19iii));
                    intent19.putExtra("level", getString(R.string.b19iv));
                    startActivity(intent19);
                    break;*/

            case "BUDFORGA1":
                cod.setText(getString(R.string.b20));
                nam.setText(getString(R.string.b20i));
                uni.setText(getString(R.string.b20ii));
                price.setText(getString(R.string.b20iii));
                leve.setText(getString(R.string.b20iv));

                mBuilder.setView(mView);
                AlertDialog dialog20=mBuilder.create();
                dialog20.show();
                break;


                /*Intent intent20 = new Intent(Medicines.this, MedExp.class);
                    intent20.putExtra("code", getString(R.string.b20));
                    intent20.putExtra("name", getString(R.string.b20i));
                    intent20.putExtra("unit",getString(R.string.b20ii));
                    intent20.putExtra("pricing", getString(R.string.b20iii));
                    intent20.putExtra("level", getString(R.string.b20iv));
                    startActivity(intent20);
                    break;*/

            case "BUDESOGA1":
                cod.setText(getString(R.string.b21));
                nam.setText(getString(R.string.b21i));
                uni.setText(getString(R.string.b21ii));
                price.setText(getString(R.string.b21iii));
                leve.setText(getString(R.string.b21iv));

                mBuilder.setView(mView);
                AlertDialog dialog21=mBuilder.create();
                dialog21.show();
                break;


              /*  Intent intent21 = new Intent(Medicines.this, MedExp.class);
                    intent21.putExtra("code", getString(R.string.b21));
                    intent21.putExtra("name", getString(R.string.b21i));
                    intent21.putExtra("unit",getString(R.string.b21ii));
                    intent21.putExtra("pricing", getString(R.string.b21iii));
                    intent21.putExtra("level", getString(R.string.b21iv));
                    startActivity(intent21);
                    break;*/

            case "BUDESOGA2":
                cod.setText(getString(R.string.b22));
                nam.setText(getString(R.string.b22i));
                uni.setText(getString(R.string.b22ii));
                price.setText(getString(R.string.b22iii));
                leve.setText(getString(R.string.b22iv));

                mBuilder.setView(mView);
                AlertDialog dialog22=mBuilder.create();
                dialog22.show();
                break;


                /*Intent intent22 = new Intent(Medicines.this, MedExp.class);
                    intent22.putExtra("code", getString(R.string.b22));
                    intent22.putExtra("name", getString(R.string.b22i));
                    intent22.putExtra("unit",getString(R.string.b22ii));
                    intent22.putExtra("pricing", getString(R.string.b22iii));
                    intent22.putExtra("level", getString(R.string.b22iv));
                    startActivity(intent22);
                    break;*/
        }
        //}
        //else if(groupPosition==2){
        switch (selected){


            case "CALAMICR1":
                cod.setText("CALAMICR1");
                nam.setText("CALAMICR1 Tablet, 10 mg ");
                uni.setText("Tablet");
                price.setText("GHs 3.35");
                leve.setText("M");
                dialog12.show();
                break;

            case "CALAMILO1":

                cod.setText("CALAMILO1");
                nam.setText("Calamine Lotion, 15% ");
                uni.setText("200 mL");
                price.setText("GHs 4.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "CALCIFTA1":
                cod.setText("CALCIFTA1");
                nam.setText("Calciferol Tablet, 10,000 units");
                uni.setText("Tablet");
                price.setText("GHs 2.90");
                leve.setText("D");
                dialog12.show();
                break;

            case "CALCARTA1":

                cod.setText("CALCARTA1");
                nam.setText("Calcium Carbonate Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CALGLUIN1":

                cod.setText("CALGLUIN1");
                nam.setText("Calcium  Gluconate Injection, 100 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 5.29");
                leve.setText("C");
                dialog12.show();
                break;


            case "CALVITTA1":

                cod.setText("CALVITTA1");
                nam.setText("Calcium with Vitamin D Tablet, (97 mg + 10 microgram)");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("C");
                dialog12.show();
                break;

            case "CAPECITA1":
                cod.setText("CAPECITA1");
                nam.setText("Capecitabine Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 19.00");
                leve.setText("D");
                dialog12.show();
                break;

            case "CARBAMTA1":

                cod.setText("CARBAMTA1");
                nam.setText("Carbamazepine Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.20");
                leve.setText("C");
                dialog12.show();
                break;

            case "CARBAMTA2":

                cod.setText("CARBAMTA2");
                nam.setText("Carbamazepine Tablet, 200 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.28");
                leve.setText("C");
                dialog12.show();
                break;

            case "CARBAMTA3":

                cod.setText("CARBAMTA3");
                nam.setText("Carbamazepine Sustained-Release Tablet, 200");
                uni.setText("Tablet");
                price.setText("GHs 1.04");
                leve.setText("SD");
                dialog12.show();
                break;

            case "CARBAMTA4":

                cod.setText("CARBAMTA4");
                nam.setText("Carbamazepine Sustained-Release Tablet, 400");
                uni.setText("Tablet");
                price.setText("GHs GHs 1.90");
                leve.setText("SD");
                dialog12.show();
                break;

            case "CARBIMTA1":

                cod.setText("CARBIMTA1");
                nam.setText("Carbimazole Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs GHs 0.54");
                leve.setText("C");
                dialog12.show();
                break;

            case "CARBIMTA2":

                cod.setText("CARBIMTA2");
                nam.setText("Carbimazole Tablet, 20 mg");
                uni.setText("Tablet");
                price.setText("GHs GHs 2.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CARBOCSY1":

                cod.setText("CARBOCSY1");
                nam.setText("Carbocisteine Paediatric Syrup , 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs GHs 6.50");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CARBOCSY2":

                cod.setText("CARBOCSY2");
                nam.setText("Carbocisteine Syrup, 250 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 7.75");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CEFACLCA1":

                cod.setText("CCEFACLCA1");
                nam.setText("Cefaclor Capsule, 250 mg");
                uni.setText("Capsule");
                price.setText("GHs 1.44");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CEFACLCA2":

                cod.setText("CEFACLCA2");
                nam.setText("Cefaclor Capsule, 500 mg");
                uni.setText("Capsule");
                price.setText("GHs 3.05");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CEFACLSU1":

                cod.setText("CEFACLSU1");
                nam.setText("Cefaclor Suspension, 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 12.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CEFACLSU2":
                cod.setText("CEFACLSU1");
                nam.setText("Cefaclor Suspension, 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 12.50");
                leve.setText("B2");
                dialog12.show();
                break;
            case "CEFOTAIN1":
                cod.setText("CEFOTAIN1");
                nam.setText("Cefotaxime Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 10.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CEFOTAIN2":

                cod.setText("CEFOTAIN2");
                nam.setText("Cefotaxime Injection, 1 g");
                uni.setText("Vial");
                price.setText("GHs 12.99");
                leve.setText("D");
                dialog12.show();
                break;

            case "CEFTRIIN2":
                cod.setText("CEFTRIIN2");
                nam.setText("Ceftriazone Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 5.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CEFTRIIN3":

                cod.setText("CEFTRIIN3");
                nam.setText("Ceftriazone Injection, 1 g");
                uni.setText("Vial");
                price.setText("GHs 6.75");
                leve.setText("C");
                dialog12.show();
                break;

            case "CEFUROIN1":


                cod.setText("CEFUROIN1");
                nam.setText("Ceftriazone Injection, 750 mg");
                uni.setText("Vial");
                price.setText("GHs 6.74");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CEFUROSU1":

                cod.setText("CEFUROSU1");
                nam.setText("Cefuroxime Suspension, 125 mg/5 mL");
                uni.setText("50 mL");
                price.setText("GHs 11.50");
                leve.setText("B2");
                break;
            case "CEFUROTA1":

                cod.setText("CEFUROTA1");
                nam.setText("Cefuroxime Tablet, 125 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.29");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CEFUROTA2":

                cod.setText("CEFUROTA2");
                nam.setText("Cefuroxime Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.60");
                leve.setText("C");
                dialog12.show();
                break;

            case "CETIRICA1":

                cod.setText("CETIRICA1");
                nam.setText("Cetirizine softgel Capsule, 10 mg");
                uni.setText("Capsule");
                price.setText("GHs 1.60");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CETIRISY1":
                cod.setText("CETIRISY1");
                nam.setText("Cetirizine Syrup, 5 mg/5 mL");
                uni.setText("30 mL");
                price.setText("GHs 3.20");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CETIRITA1":
                cod.setText("CETIRITA1");
                nam.setText("Cetirizine Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.15");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CETRIMSO1":
                cod.setText("CETRIMSO1");
                nam.setText("Cetrimide Solution");
                uni.setText("Tablet");
                price.setText("GHs 0.15");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CHLORAED1":

                cod.setText("CHLORAED1");
                nam.setText("Chloramphenicol Ear Drops, 5%");
                uni.setText("10 mL");
                price.setText("GHs 2.50");
                leve.setText("M");
                dialog12.show();
                break;

            case "CHLORAID1":

                cod.setText("CHLORAID1");
                nam.setText("Chloramphenicol Eye Drops, 0.5%");
                uni.setText("10 mL");
                price.setText("GHs 2.50");
                leve.setText("M");
                dialog12.show();
                break;

            case "CHLORAEO1":

                cod.setText("CHLORAEO1");
                nam.setText("Chloramphenicol Eye Ointment, 1%");
                uni.setText("5 G");
                price.setText("GHs 2.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "CHLORAIN1":

                cod.setText("CHLORAIN1");
                nam.setText("Chloramphenicol Injection, 1 g");
                uni.setText("1 G");
                price.setText("GHs 2.43");
                leve.setText("C");
                dialog12.show();
                break;

            case "CHLORASU1":

                cod.setText("CHLORASU1");
                nam.setText("Chloramphenicol Suspension, 125mg/5mL");
                uni.setText("100 mL");
                price.setText("GHs 3.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CHLORHCR1":

                cod.setText("CHLORHCR1");
                nam.setText("Chlorhexidine Cream, 1%");
                uni.setText("15 G");
                price.setText("GHs 5.00");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CHLORHMW1":

                cod.setText("CHLORHMW1");
                nam.setText("Chlorhexidine Mouthwash, 0.2%");
                uni.setText("200 mL");
                price.setText("GHs 6.12");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CHLORHSO1":

                cod.setText("CHLORHSO1");
                nam.setText("Chlorhexidine Solution, 2.5%");
                uni.setText("100 mL");
                price.setText("GHs 5.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "CHLPHESY1":

                cod.setText("CHLPHESY1");
                nam.setText("Chlorphenamine Syrup, 2 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 3.08");
                leve.setText("A");
                dialog12.show();
                break;

            case "CHLPHETA1":

                cod.setText("CHLPHETA1");
                nam.setText("Chlorphenamine Tablet, 4 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.01");
                leve.setText("A");
                dialog12.show();
                break;

            case "CHLPROIN1":


                cod.setText("CHLPROIN1");
                nam.setText("Chlorpromazine Injection, 25 mg/mL in 2 mL");
                uni.setText("Ampoule-");
                price.setText("GHs 2.65");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CHLPROTA1":

                cod.setText("CHLPROTA1");
                nam.setText("Chlorpromazine Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.15");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CHLPROTA2":

                cod.setText("CHLPROTA2");
                nam.setText("Chlorpromazine Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.15");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CHLPROTA3":

                cod.setText("CHLPROTA3");
                nam.setText("Chlorpromazine Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.30");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CHREFLIN1":

                cod.setText("CHREFLIN1");
                nam.setText("Cholera Replacement Fluid Injection, (5:4:1) 500 mL");
                uni.setText("500 mL");
                price.setText("GHs 4.94");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CHREFLIN2":

                cod.setText("CHREFLIN2");
                nam.setText("Cholera Replacement Fluid Injection, (5:4:1) 1000 mL");
                uni.setText("1000 mL");
                price.setText("GHs 6.15");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CIPROFID1":
                cod.setText("CIPROFID1");
                nam.setText("Ciprofloxacin Eye Drops, 0.3%");
                uni.setText("10 mL");
                price.setText("GHs 5.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CIPROFIN1":

                cod.setText("CIPROFIN1");
                nam.setText("Ciprofloxacin Infusion, 2 mg/mL in 100 mL");
                uni.setText("Bottle");
                price.setText("GHs 3.63");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CIPROFTA1":
                cod.setText("CIPROFTA1");
                nam.setText("Ciprofloxacin Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("M");
                dialog12.show();
                break;

            case "CIPROFTA2":

                cod.setText("CIPROFTA2");
                nam.setText("Ciprofloxacin Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.42");
                leve.setText("M");
                dialog12.show();
                break;

            case "CIPTINTA1":

                cod.setText("CIPTINTA1");
                nam.setText("Ciprofloxacin + Tinidazole Tablet, 500 mg +");
                uni.setText("Tablet");
                price.setText("GHs 1.20");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CLARITCA1":


                cod.setText("CLARITCA1");
                nam.setText("Clarithromycin Capsule, 250 mg");
                uni.setText("Capsule");
                price.setText("GHs 1.50");
                leve.setText("C");
                dialog12.show();
                break;


            case "CLARITCA2":

                cod.setText("CLARITCA2");
                nam.setText("Clarithromycin Capsule, 500 mg");
                uni.setText("Capsule");
                price.setText("GHs 3.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLARITSU1":

                cod.setText("CLARITSU1");
                nam.setText("Clarithromycin Paediatric Suspension, 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 22.50");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLINDACA1":

                cod.setText("CLINDACA1");
                nam.setText("Clindamycin Capsule, 150 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.80");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLINDAIN1":

                cod.setText("CLINDAIN1");
                nam.setText("Clindamycin Injection, 150 mg/mL in 2 mL");
                uni.setText("Vial");
                price.setText("GHs 13.80");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLINDASU1":
                cod.setText("CLINDASU1");
                nam.setText("Clindamycin Suspension, 75 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 36.70");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLINDASO1":

                cod.setText("CLINDASO1");
                nam.setText("Clindamycin Topical Solution, 1%");
                uni.setText("30 mL");
                price.setText("GHs 45.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLOPROCR1":

                cod.setText("CLOPROCR1");
                nam.setText("Clobetasol Propionate Cream, 0.05%");
                uni.setText("15 G");
                price.setText("GHs 3.50");
                leve.setText("SD");
                dialog12.show();
                break;

            case "CLOHYDCR1":
                cod.setText("CLOHYDCR1");
                nam.setText("Clotrimazole + Hydrocortisone Cream, 1% +");
                uni.setText("15 G");
                price.setText("GHs 4.29");
                leve.setText("C");
                dialog12.show();
                break;

            case "CLOTRICR1":

                cod.setText("CLOTRICR1");
                nam.setText("Clotrimazole Cream, 1%");
                uni.setText("15 G");
                price.setText("GHs 4.00");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CLOTRICR2":

                cod.setText("CLOTRICR2");
                nam.setText("Clotrimazole Cream, 2%");
                uni.setText("15 G");
                price.setText("GHs 4.30");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CLOTRIVP1":
                cod.setText("CLOTRIVP1");
                nam.setText("Clotrimazole Pessary, 100 mg");
                uni.setText("6 Pess.");
                price.setText("GHs 4.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "CLOTRIVP2":

                cod.setText("CLOTRIVP2");
                nam.setText("Clotrimazole Pessary, 200 mg");
                uni.setText("3 Pess.");
                price.setText("GHs 5.02");
                leve.setText("M");
                dialog12.show();
                break;

            case "CLOTRIVP3":


                cod.setText("CLOTRIVP3");
                nam.setText("Clotrimazole Pessary, 500 mg");
                uni.setText("1 Pess.");
                price.setText("GHs 6.85");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CLOXACIN1":


                cod.setText("CLOXACIN1");
                nam.setText("Cloxacillin Injection, 250 mg");
                uni.setText("Vial");
                price.setText("GHs 1.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "CLOXACIN2":

                cod.setText("CLOXACIN2");
                nam.setText("Cloxacillin Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 2.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "COAMOXIN1":


                cod.setText("COAMOXIN1");
                nam.setText("Amoxicillin + Clavulanic Acid Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 9.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "COAMOXIN2":

                cod.setText("COAMOXIN2");
                nam.setText("Amoxicillin + Clavulanic Acid Injection, 1.2g");
                uni.setText("Vial");
                price.setText("GHs 13.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "COAMOXSU1":

                cod.setText("COAMOXSU1");
                nam.setText("Amoxicillin + Clavulanic Acid Suspension, 250");
                uni.setText("70 mL");
                price.setText("GHs 13.00");
                leve.setText("B2");
                dialog12.show();
                break;
            case "COAMOXSU2":


                cod.setText("COAMOXSU2");
                nam.setText("Amoxicillin + Clavulanic Acid Suspension, 400");
                uni.setText("70 mL");
                price.setText("GHs 14.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "COAMOXTA1":

                cod.setText("COAMOXTA1");
                nam.setText("Amoxicillin + Clavulanic Acid Tablet, 500 mg + 125 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.32");
                leve.setText("B2");
                dialog12.show();
                break;

            case "COAMOXTA2":

                cod.setText("COAMOXTA2");
                nam.setText("Amoxicillin + Clavulanic Acid Tablet, 875 mg + 125 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.97");
                leve.setText("B2");
                dialog12.show();
                break;
            case "CODEINTA1":

                cod.setText("CODEINTA1");
                nam.setText("Codeine Tablet, 30 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("B2");
                break;
            case "COOENOTA1":


                cod.setText("COOENOTA1");
                nam.setText("Conjugated Oestrogen + Norgesterol Tablet, 625 microgram + 150 microgr");
                uni.setText("Tablet");
                price.setText("GHs 2.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "CONOESTA1":


                cod.setText("CONOESTA1");
                nam.setText("Conjugated Oestrogen Tablet, 625 microgram");
                uni.setText("Tablet");
                price.setText("GHs 1.00");
                leve.setText("D");
                dialog12.show();
                break;
            case "CONOESVC1":


                cod.setText("CONOESVC1");
                nam.setText("Conjugated Oestrogen Vaginal cream, 625 microgram/g");
                uni.setText("1 G");
                price.setText("GHs 1.25");
                leve.setText("D");
                dialog12.show();
                break;


            case "CORANTID1":
                cod.setText("CORANTID1");
                nam.setText("Corticosteroid + Antibiotic Eye Drops");
                uni.setText("10 mL");
                price.setText("GHs 8.50");
                leve.setText("D");
                dialog12.show();
                break;
            case "CORANTEO1":


                cod.setText("CORANTEO1");
                nam.setText("Corticosteroid + Antibiotic Eye Ointment");
                uni.setText("10 G");
                price.setText("GHs 7.70");
                leve.setText("C");
                dialog12.show();
                break;

            case "COTRIMSU1":
                cod.setText("COTRIMSU1");
                nam.setText("Co-trimoxazole Suspension, (200+40) mg/5");
                uni.setText("100 mL");
                price.setText("GHs 2.50");
                leve.setText("B1");
                dialog12.show();
                break;
            case "COTRIMTA1":
                cod.setText("COTRIMTA1");
                nam.setText("Cotrimoxazole Tablet, (400+80) mg");
                uni.setText("100 mL");
                price.setText("GHs 0.08");
                leve.setText("B1");
                dialog12.show();
                break;

            case "CYCLOPID1":
                cod.setText("CYCLOPID1");
                nam.setText("Cyclopentolate Eye Drops, 1%");
                uni.setText("5 mL");
                price.setText("GHs 15.00");
                leve.setText("SD");
                dialog12.show();
                break;

            case "CYCLOPIN1":
                cod.setText("CYCLOPIN1");
                nam.setText("Cyclophosphamide Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 8.27");
                leve.setText("D");
                dialog12.show();
                break;
        }
        //}
        //else if(groupPosition==3){
        switch(selected){
            case "DALSODIN1":
                cod.setText(getString(R.string.d));
                nam.setText(getString(R.string.di));
                uni.setText(getString(R.string.dii));
                price.setText(getString(R.string.diii));
                leve.setText(getString(R.string.div));

                mBuilder.setView(mView);
                AlertDialog dialog=mBuilder.create();
                dialog.show();
                break;


                /*Intent intent = new Intent(Medicines.this, MedExp.class);
                    intent.putExtra("code", getString(R.string.d));
                    intent.putExtra("name", getString(R.string.di));
                    intent.putExtra("unit",getString(R.string.dii));
                    intent.putExtra("pricing", getString(R.string.diii));
                    intent.putExtra("level", getString(R.string.div));
                    startActivity(intent);
                    break;*/

            case "DARROWIN1":
                cod.setText(getString(R.string.d1));
                nam.setText(getString(R.string.d1i));
                uni.setText(getString(R.string.d1ii));
                price.setText(getString(R.string.d1iii));
                leve.setText(getString(R.string.d1iv));

                mBuilder.setView(mView);
                AlertDialog dialog1=mBuilder.create();
                dialog1.show();
                break;


                /*Intent intent1 = new Intent(Medicines.this, MedExp.class);
                    intent1.putExtra("code", getString(R.string.d1));
                    intent1.putExtra("name", getString(R.string.d1i));
                    intent1.putExtra("unit",getString(R.string.d1ii));
                    intent1.putExtra("pricing", getString(R.string.d1iii));
                    intent1.putExtra("level", getString(R.string.d1iv));
                    startActivity(intent1);
                    break;*/

            case "DEXAMEID1":
                cod.setText(getString(R.string.d2));
                nam.setText(getString(R.string.d2i));
                uni.setText(getString(R.string.d2ii));
                price.setText(getString(R.string.d2iii));
                leve.setText(getString(R.string.d2iv));

                mBuilder.setView(mView);
                AlertDialog dialog2=mBuilder.create();
                dialog2.show();
                break;


               /* Intent intent2 = new Intent(Medicines.this, MedExp.class);
                    intent2.putExtra("code", getString(R.string.d2));
                    intent2.putExtra("name", getString(R.string.d2i));
                    intent2.putExtra("unit",getString(R.string.d2ii));
                    intent2.putExtra("pricing", getString(R.string.d2iii));
                    intent2.putExtra("level", getString(R.string.d2iv));
                    startActivity(intent2);
                    break;*/

            case "DEXAMEEO1":
                cod.setText(getString(R.string.d3));
                nam.setText(getString(R.string.d3i));
                uni.setText(getString(R.string.d3ii));
                price.setText(getString(R.string.d3iii));
                leve.setText(getString(R.string.d3iv));

                mBuilder.setView(mView);
                AlertDialog dialog3=mBuilder.create();
                dialog3.show();
                break;

            case "DEXAMEIN1":
                cod.setText(getString(R.string.d4));
                nam.setText(getString(R.string.d4i));
                uni.setText(getString(R.string.d4ii));
                price.setText(getString(R.string.d4iii));
                leve.setText(getString(R.string.d4iv));

                mBuilder.setView(mView);
                AlertDialog dialog4=mBuilder.create();
                dialog4.show();
                break;

            case "DEXAMEIN2":
                cod.setText(getString(R.string.d5));
                nam.setText(getString(R.string.d5i));
                uni.setText(getString(R.string.d5ii));
                price.setText(getString(R.string.d5iii));
                leve.setText(getString(R.string.d5iv));

                mBuilder.setView(mView);
                AlertDialog dialog5=mBuilder.create();
                dialog5.show();
                break;

            case "DEXAMETA1":
                cod.setText(getString(R.string.d6));
                nam.setText(getString(R.string.d6i));
                uni.setText(getString(R.string.d6ii));
                price.setText(getString(R.string.d6iii));
                leve.setText(getString(R.string.d6iv));

                mBuilder.setView(mView);
                AlertDialog dialog6=mBuilder.create();
                dialog6.show();
                break;

            case "DESOCHIN1":
                cod.setText(getString(R.string.d7));
                nam.setText(getString(R.string.d7i));
                uni.setText(getString(R.string.d7ii));
                price.setText(getString(R.string.d7iii));
                leve.setText(getString(R.string.d7iv));

                mBuilder.setView(mView);
                AlertDialog dialog7=mBuilder.create();
                dialog7.show();
                break;


            case "DESOCHIN2":
                cod.setText(getString(R.string.d8));
                nam.setText(getString(R.string.d8i));
                uni.setText(getString(R.string.d8ii));
                price.setText(getString(R.string.d8iii));
                leve.setText(getString(R.string.d8iv));

                mBuilder.setView(mView);
                AlertDialog dialog8=mBuilder.create();
                dialog8.show();
                break;

            case "DEXTROIN1":
                cod.setText(getString(R.string.d9));
                nam.setText(getString(R.string.d9i));
                uni.setText(getString(R.string.d9ii));
                price.setText(getString(R.string.d9iii));
                leve.setText(getString(R.string.d9iv));

                mBuilder.setView(mView);
                AlertDialog dialog9=mBuilder.create();
                dialog9.show();
                break;


            case "DEXTROIN2":
                cod.setText(getString(R.string.d10));
                nam.setText(getString(R.string.d10i));
                uni.setText(getString(R.string.d10ii));
                price.setText(getString(R.string.d10iii));
                leve.setText(getString(R.string.d10iv));

                mBuilder.setView(mView);
                AlertDialog dialog10=mBuilder.create();
                dialog10.show();
                break;



            case "DEXTROIN3":
                cod.setText(getString(R.string.d11));
                nam.setText(getString(R.string.d11i));
                uni.setText(getString(R.string.d11ii));
                price.setText(getString(R.string.d11iii));
                leve.setText(getString(R.string.d11iv));

                mBuilder.setView(mView);
                AlertDialog dialog11=mBuilder.create();
                dialog11.show();
                break;

            case "DEXTROIN4":
                cod.setText(getString(R.string.d12));
                nam.setText(getString(R.string.d12i));
                uni.setText(getString(R.string.d12ii));
                price.setText(getString(R.string.d12iii));
                leve.setText(getString(R.string.d12iv));
                dialog12.show();
                break;


            case "DEXTROIN6":
                cod.setText(getString(R.string.d13));
                nam.setText(getString(R.string.d13i));
                uni.setText(getString(R.string.d13ii));
                price.setText(getString(R.string.d13iii));
                leve.setText(getString(R.string.d13iv));
                dialog12.show();
                break;

            case "DIAZEPIN1":
                cod.setText(getString(R.string.d14));
                nam.setText(getString(R.string.d14i));
                uni.setText(getString(R.string.d14ii));
                price.setText(getString(R.string.d14iii));
                leve.setText(getString(R.string.d14iv));
                dialog12.show();
                break;

            case "DIAZEPRS1":
                cod.setText(getString(R.string.d15));
                nam.setText(getString(R.string.d15i));
                uni.setText(getString(R.string.d15ii));
                price.setText(getString(R.string.d15iii));
                leve.setText(getString(R.string.d15iv));
                dialog12.show();
                break;

            case "DIAZEPTA1":
                cod.setText(getString(R.string.d16));
                nam.setText(getString(R.string.d16i));
                uni.setText(getString(R.string.d16ii));
                price.setText(getString(R.string.d16iii));
                leve.setText(getString(R.string.d16iv));
                dialog12.show();
                break;

            case "DIAZEPTA2":
                cod.setText(getString(R.string.d17));
                nam.setText(getString(R.string.d17i));
                uni.setText(getString(R.string.d17ii));
                price.setText(getString(R.string.d17iii));
                leve.setText(getString(R.string.d17iv));
                dialog12.show();
                break;

            case "DICLOFCA1":
                cod.setText(getString(R.string.d18));
                nam.setText(getString(R.string.d18i));
                uni.setText(getString(R.string.d18ii));
                price.setText(getString(R.string.d18iii));
                leve.setText(getString(R.string.d18iv));
                dialog12.show();
                break;

            case "DICLOFGE1":
                cod.setText(getString(R.string.d19));
                nam.setText(getString(R.string.d19i));
                uni.setText(getString(R.string.d19ii));
                price.setText(getString(R.string.d19iii));
                leve.setText(getString(R.string.d19iv));
                dialog12.show();
                break;

            case "DICLOFIN1":
                cod.setText(getString(R.string.d20));
                nam.setText(getString(R.string.d20i));
                uni.setText(getString(R.string.d20ii));
                price.setText(getString(R.string.d20iii));
                leve.setText(getString(R.string.d20iv));
                dialog12.show();
                break;

            case "DICLOFRE1":
                cod.setText(getString(R.string.d21));
                nam.setText(getString(R.string.d21i));
                uni.setText(getString(R.string.d21ii));
                price.setText(getString(R.string.d21iii));
                leve.setText(getString(R.string.d21iv));
                dialog12.show();
                break;

            case "DICLOFRE2":
                cod.setText(getString(R.string.d22));
                nam.setText(getString(R.string.d22i));
                uni.setText(getString(R.string.d22ii));
                price.setText(getString(R.string.d22iii));
                leve.setText(getString(R.string.d22iv));
                dialog12.show();
                break;

            case "DICLOFTA1":
                cod.setText(getString(R.string.d23));
                nam.setText(getString(R.string.d23i));
                uni.setText(getString(R.string.d23ii));
                price.setText(getString(R.string.d23iii));
                leve.setText(getString(R.string.d23iv));
                dialog12.show();
                break;

            case "DICLOFTA2":
                cod.setText(getString(R.string.d24));
                nam.setText(getString(R.string.d24i));
                uni.setText(getString(R.string.d24ii));
                price.setText(getString(R.string.d24iii));
                leve.setText(getString(R.string.d24iv));
                dialog12.show();
                break;

            case "DIESTITA1":
                cod.setText(getString(R.string.d25));
                nam.setText(getString(R.string.d25i));
                uni.setText(getString(R.string.d25ii));
                price.setText(getString(R.string.d25iii));
                leve.setText(getString(R.string.d25iv));
                dialog12.show();
                break;

            case "DIESTITA2":
                cod.setText(getString(R.string.d26));
                nam.setText(getString(R.string.d26i));
                uni.setText(getString(R.string.d26ii));
                price.setText(getString(R.string.d26iii));
                leve.setText(getString(R.string.d26iv));
                dialog12.show();
                break;

            case "DIGOXIEL1":
                cod.setText(getString(R.string.d27));
                nam.setText(getString(R.string.d27i));
                uni.setText(getString(R.string.d27ii));
                price.setText(getString(R.string.d27iii));
                leve.setText(getString(R.string.d27iv));
                dialog12.show();
                break;

            case "DIGOXITA1":
                cod.setText(getString(R.string.d28));
                nam.setText(getString(R.string.d28i));
                uni.setText(getString(R.string.d28ii));
                price.setText(getString(R.string.d28iii));
                leve.setText(getString(R.string.d28iv));
                dialog12.show();
                break;

            case "DIGOXITA2":
                cod.setText(getString(R.string.d29));
                nam.setText(getString(R.string.d29i));
                uni.setText(getString(R.string.d29ii));
                price.setText(getString(R.string.d29iii));
                leve.setText(getString(R.string.d29iv));
                dialog12.show();
                break;

            case "DIGOXITA3":
                cod.setText(getString(R.string.d30));
                nam.setText(getString(R.string.d30i));
                uni.setText(getString(R.string.d30ii));
                price.setText(getString(R.string.d30iii));
                leve.setText(getString(R.string.d30iv));
                dialog12.show();
                break;

            case "DIHPIPPO1":
                cod.setText(getString(R.string.d31));
                nam.setText(getString(R.string.d31i));
                uni.setText(getString(R.string.d31ii));
                price.setText(getString(R.string.d31iii));
                leve.setText(getString(R.string.d31iv));
                dialog12.show();
                break;

            case "DIHYDRTA1":
                cod.setText(getString(R.string.d32));
                nam.setText(getString(R.string.d32i));
                uni.setText(getString(R.string.d32ii));
                price.setText(getString(R.string.d32iii));
                leve.setText(getString(R.string.d32iv));
                dialog12.show();
                break;

            case "DISOPYCA1":
                cod.setText(getString(R.string.d33));
                nam.setText(getString(R.string.d33i));
                uni.setText(getString(R.string.d33ii));
                price.setText(getString(R.string.d33iii));
                leve.setText(getString(R.string.d33iv));
                dialog12.show();
                break;

            case "DISPHOIN1":
                cod.setText(getString(R.string.d34));
                nam.setText(getString(R.string.d34i));
                uni.setText(getString(R.string.d34ii));
                price.setText(getString(R.string.d34iii));
                leve.setText(getString(R.string.d34iv));
                dialog12.show();
                break;

            case "DOCETAIN1":
                cod.setText(getString(R.string.d35));
                nam.setText(getString(R.string.d35i));
                uni.setText(getString(R.string.d35ii));
                price.setText(getString(R.string.d35iii));
                leve.setText(getString(R.string.d35iv));
                dialog12.show();
                break;

            case "DOMPERTA1":
                cod.setText(getString(R.string.d36));
                nam.setText(getString(R.string.d36i));
                uni.setText(getString(R.string.d36ii));
                price.setText(getString(R.string.d36iii));
                leve.setText(getString(R.string.d36iv));
                dialog12.show();
                break;

            case "DOPAMIIN1":
                cod.setText(getString(R.string.d37));
                nam.setText(getString(R.string.d37i));
                uni.setText(getString(R.string.d37ii));
                price.setText(getString(R.string.d37iii));
                leve.setText(getString(R.string.d37iv));
                dialog12.show();
                break;

            case "DOXAPRIN1":
                cod.setText(getString(R.string.d38));
                nam.setText(getString(R.string.d38i));
                uni.setText(getString(R.string.d38ii));
                price.setText(getString(R.string.d38iii));
                leve.setText(getString(R.string.d38iv));
                dialog12.show();
                break;

            case "DOXYCYCA1":
                cod.setText(getString(R.string.d39));
                nam.setText(getString(R.string.d39i));
                uni.setText(getString(R.string.d39ii));
                price.setText(getString(R.string.d39iii));
                leve.setText(getString(R.string.d39iv));
                dialog12.show();
                break;
        }
        switch (selected){
            case "ENOSODIN2":
                cod.setText(getString(R.string.e));
                nam.setText(getString(R.string.ei));
                uni.setText(getString(R.string.eii));
                price.setText(getString(R.string.eiii));
                leve.setText(getString(R.string.eiv));
                dialog12.show();
                break;

            case "EPHEDRIN1":
                cod.setText(getString(R.string.e1));
                nam.setText(getString(R.string.e1i));
                uni.setText(getString(R.string.e1ii));
                price.setText(getString(R.string.e1iii));
                leve.setText(getString(R.string.e1iv));
                dialog12.show();
                break;

            case "EPHEDRND1":
                cod.setText(getString(R.string.e2));
                nam.setText(getString(R.string.e2i));
                uni.setText(getString(R.string.e2ii));
                price.setText(getString(R.string.e2iii));
                leve.setText(getString(R.string.e2iv));
                dialog12.show();
                break;

            case "EPHEDRND2":
                cod.setText(getString(R.string.e3));
                nam.setText(getString(R.string.e3i));
                uni.setText(getString(R.string.e3ii));
                price.setText(getString(R.string.e3iii));
                leve.setText(getString(R.string.e3iv));
                dialog12.show();
                break;

            case "ERGOMEIN1":
                cod.setText(getString(R.string.e4));
                nam.setText(getString(R.string.e4i));
                uni.setText(getString(R.string.e4ii));
                price.setText(getString(R.string.e4iii));
                leve.setText(getString(R.string.e4iv));
                dialog12.show();
                break;

            case "ERGOMEIN2":
                cod.setText(getString(R.string.e5));
                nam.setText(getString(R.string.e5i));
                uni.setText(getString(R.string.e5ii));
                price.setText(getString(R.string.e5iii));
                leve.setText(getString(R.string.e5iv));
                dialog12.show();
                break;

            case "ERGOMETA1":
                cod.setText(getString(R.string.e6));
                nam.setText(getString(R.string.e6i));
                uni.setText(getString(R.string.e6ii));
                price.setText(getString(R.string.e6iii));
                leve.setText(getString(R.string.e6iv));
                dialog12.show();
                break;

            case "ERGOTATA1":
                cod.setText(getString(R.string.e7));
                nam.setText(getString(R.string.e7i));
                uni.setText(getString(R.string.e7ii));
                price.setText(getString(R.string.e7iii));
                leve.setText(getString(R.string.e7iv));
                dialog12.show();
                break;

            case "ERYTHRSY1":
                cod.setText(getString(R.string.e8));
                nam.setText(getString(R.string.e8i));
                uni.setText(getString(R.string.e8ii));
                price.setText(getString(R.string.e8iii));
                leve.setText(getString(R.string.e8iv));
                dialog12.show();
                break;

            case "ERYTHRTA1":
                cod.setText(getString(R.string.e9));
                nam.setText(getString(R.string.e9i));
                uni.setText(getString(R.string.e9ii));
                price.setText(getString(R.string.e9iii));
                leve.setText(getString(R.string.e9iv));
                dialog12.show();
                break;

            case "ESOMEPCA1":
                cod.setText(getString(R.string.e10));
                nam.setText(getString(R.string.e10i));
                uni.setText(getString(R.string.e10ii));
                price.setText(getString(R.string.e10iii));
                leve.setText(getString(R.string.e10iv));
                dialog12.show();
                break;

            case "ESOMEPCA2":
                cod.setText(getString(R.string.e11));
                nam.setText(getString(R.string.e11i));
                uni.setText(getString(R.string.e11ii));
                price.setText(getString(R.string.e11iii));
                leve.setText(getString(R.string.e11iv));
                dialog12.show();
                break;

            case "ETHOSUSY1":
                cod.setText(getString(R.string.e12));
                nam.setText(getString(R.string.e12i));
                uni.setText(getString(R.string.e12ii));
                price.setText(getString(R.string.e12iii));
                leve.setText(getString(R.string.e12iv));
                dialog12.show();
                break;

            case "ETHOSUTA1":
                cod.setText(getString(R.string.e13));
                nam.setText(getString(R.string.e13i));
                uni.setText(getString(R.string.e13ii));
                price.setText(getString(R.string.e13iii));
                leve.setText(getString(R.string.e13iv));
                dialog12.show();
                break;
        }

        switch (selected){
            case "FEAMCISU1":
                cod.setText(getString(R.string.f));
                nam.setText(getString(R.string.fi));
                uni.setText(getString(R.string.fii));
                price.setText(getString(R.string.fiii));
                leve.setText(getString(R.string.fiv));
                dialog12.show();
                break;


            case "FERFUMTA1":
                cod.setText("FERFUMTA1");
                nam.setText("Ferrous Fumarate Tablet, 100 mg (Elemental)");
                uni.setText("Tablet");
                price.setText("GHs 0.02");
                leve.setText("M");
                dialog12.show();
                break;

            case "FERSULSY1":
                cod.setText("FERSULSY1");
                nam.setText("Ferrous Sulphate (BPC) Syrup, 60 mg/5 mL");
                uni.setText("200 mL");
                price.setText("GHs 3.95");
                leve.setText("M");
                dialog12.show();
                break;


            case "FESUFOTA1":
                cod.setText("FESUFOTA1");
                nam.setText("Ferrous Sulphate + Folic Acid Tablet, 50 mg (Elemental Iron) + 400 mic");
                uni.setText("Tablet");
                price.setText("GHs 0.19");
                leve.setText("A");
                dialog12.show();
                break;

            case "FERSULTA1":
                cod.setText("FERSULTA1");
                nam.setText("Ferrous Sulphate Tablet, 60 mg (Elemental)");
                uni.setText("Tablet");
                price.setText("GHs 0.02");
                leve.setText("M");
                dialog12.show();
                break;


            case "FINASTTA1":
                cod.setText("FINASTTA1");
                nam.setText("Finasteride Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.89");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUCLOCA1":
                cod.setText("FLUCLOCA1");
                nam.setText("Flucloxacillin Capsule, 250 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.19");
                leve.setText("M");
                dialog12.show();
                break;


            case "FLUCLOIN1":
                cod.setText("FLUCLOIN1");
                nam.setText("Flucloxacillin Injection, 250 mg");
                uni.setText("Vial");
                price.setText("GHs 3.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCLOIN2":
                cod.setText("FLUCLOIN2");
                nam.setText("Flucloxacillin Injection, 500 mg");
                uni.setText("Vial");
                price.setText("GHs 4.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCLOSU1":
                cod.setText("FLUCLOSU1");
                nam.setText("Flucloxacillin Suspension, 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 4.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "FLUCONCA1":
                cod.setText("FLUCONCA1");
                nam.setText("Fluconazole Capsule, 150 mg");
                uni.setText("Capsule");
                price.setText("GHs 3.40");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCONCA2":
                cod.setText("FLUCONCA2");
                nam.setText("Fluconazole Capsule, 200 mg");
                uni.setText("Capsule");
                price.setText("GHs 3.90");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCONSU1":
                cod.setText("FLUCONSU1");
                nam.setText("Fluconazole Suspension, 10 mg/mL");
                uni.setText("35 mL");
                price.setText("GHs 6.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCONSU2":
                cod.setText("FLUCONSU2");
                nam.setText("Fluconazole Suspension, 50 mg/5mL");
                uni.setText("35 mL");
                price.setText("GHs 8.00");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUCONTA1":
                cod.setText("FLUCONTA1");
                nam.setText("Fluconazole Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 2.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "FLUDROTA1":
                cod.setText("FLUDROTA1");
                nam.setText("Fludrocortisone Tablet, 100 microgram");
                uni.setText("Tablet");
                price.setText("GHs 1.32");
                leve.setText("D");
                dialog12.show();
                break;

            case "FLUOXECA1":
                cod.setText("FLUDROTA1");
                nam.setText("Fluoxetine Capsule, 20 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.76");
                leve.setText("C");
                dialog12.show();
                break;

            case "FLUPENTA1":
                cod.setText("FLUPENTA1");
                nam.setText("Flupentixol Tablet, 500 microgram");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("C");
                dialog12.show();
                break;


            case "FLUPENTA2":
                cod.setText("FLUPENTA2");
                nam.setText("Flupentixol Tablet, 1000 microgram");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("C");
                dialog12.show();
                break;

            case "FLUDECIN1":
                cod.setText("FLUDECIN1");
                nam.setText("Fluphenazine Deconoate Injection, 25 mg/mL");
                uni.setText("1 mL");
                price.setText("GHs 18.25");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUSALGA1":
                cod.setText("FLUSALGA1");
                nam.setText("Fluticasone + Salmeterol Inhaler, 250 microgram/50 microgram (60 Doses)");
                uni.setText("Inhaler");
                price.setText("GHs 96.56");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUTICGA1":
                cod.setText("FLUTICGA1");
                nam.setText("Fluticasone MDI, 50 microgram (120 Doses)");
                uni.setText("Inhaler");
                price.setText("GHs 30.52");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUTICGA2":
                cod.setText("FLUTICGA2");
                nam.setText("Fluticasone MDI, 125 microgram (120 Doses)");
                uni.setText("Inhaler");
                price.setText("GHs 54.94");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUTICGA3":
                cod.setText("FLUTICGA3");
                nam.setText("Fluticasone MDI, 250 microgram (120 Doses)");
                uni.setText("Inhaler");
                price.setText("GHs 92.00");
                leve.setText("SD");
                dialog12.show();
                break;

            case "FLUVASCA1":
                cod.setText("FLUVASCA1");
                nam.setText("Fluvastatin Capsule, 20 mg");
                uni.setText("Capsule");
                price.setText("GHs 2.60");
                leve.setText("D");
                dialog12.show();
                break;

            case "FOLACITA1":
                cod.setText("FOLACITA1");
                nam.setText("Folic Acid Tablet, 5 mg (Blister Pack)");
                uni.setText("10 Tablets");
                price.setText("GHs 0.01");
                leve.setText("M");
                dialog12.show();
                break;

            case "FUROSEIN1":
                cod.setText("FUROSEIN1");
                nam.setText("Furosemide Injection, 10 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 0.50");
                leve.setText("B1");
                dialog12.show();
                break;

            case "FUROSETA1":
                cod.setText("FUROSETA1");
                nam.setText("Furosemide Tablet, 40 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.14");
                leve.setText("B1");
                dialog12.show();
                break;
        }

        switch (selected){
            case "GELATIIN1":
                cod.setText("GELATIIN1");
                nam.setText("Gelatin Infusion (Succinylated Gelatin)");
                uni.setText("500 mL");
                price.setText("GHs 14.50");
                leve.setText("B2");
                dialog12.show();
                break;

            case "GENTAMED1":
                cod.setText("GENTAMED1");
                nam.setText("Gentamicin Ear Drops, 0.3%");
                uni.setText("10 mL");
                price.setText("GHs 2.00");
                leve.setText("B1");
                dialog12.show();
                break;

            case "GENTAMID1":
                cod.setText("GENTAMID1");
                nam.setText("Gentamicin Eye Drops, 0.3%");
                uni.setText("10 mL");
                price.setText("GHs 2.00");
                leve.setText("B1");
                dialog12.show();
                break;

            case "GENTAMIN1":
                cod.setText("GENTAMIN1");
                nam.setText("Gentamicin Injection, 40 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 0.48");
                leve.setText("B2");
                dialog12.show();
                break;

            case "GLIBENTA1":
                cod.setText("GLIBENTA1");
                nam.setText("Glibenclamide Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.06");
                leve.setText("B2");
                dialog12.show();
                break;

            case "GLICLATA1":
                cod.setText("GLICLATA1");
                nam.setText("Gliclazide Tablet, 80 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.30");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLIMEPTA1":
                cod.setText("GLIMEPTA1");
                nam.setText("Glimepiride Tablet, 1 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.80");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLIMEPTA2":
                cod.setText("GLIMEPTA2");
                nam.setText("Glimepiride Tablet, 2 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLIMEPTA3":
                cod.setText("GLIMEPTA3");
                nam.setText("Glimepiride Tablet, 3 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLIMEPTA4":
                cod.setText("GLIMEPTA4");
                nam.setText("Glimepiride Tablet, 4 mg");
                uni.setText("Tablet");
                price.setText("GHs 2.50");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLUCAGIN1":
                cod.setText("GLUCAGIN1");
                nam.setText("Glucagon Injection, 1 mg");
                uni.setText("Ampoule");
                price.setText("GHs 78.55");
                leve.setText("C");
                dialog12.show();
                break;

            case "GLTRSUTA1":
                cod.setText("GLTRSUTA1");
                nam.setText("Glyceryl Trinitrate Sublingual Tablet, 500 microgram");
                uni.setText("100 Tablets");
                price.setText("GHs 39.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "GRANISIN1":
                cod.setText("GRANISIN1");
                nam.setText("Granisetron Injection, 1 mg/1mL");
                uni.setText("Ampoule");
                price.setText("GHs 29.53");
                leve.setText("D");
                dialog12.show();
                break;

            case "GRANISTA1":
                cod.setText("GRANISTA1");
                nam.setText("Granisetron Tablet, 1 mg");
                uni.setText("Tablet");
                price.setText("GHs 14.03");
                leve.setText("D");
                dialog12.show();
                break;

            case "GRISEOSU1":
                cod.setText("GRISEOSU1");
                nam.setText("Griseofulvin Suspension, 125 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 7.65");
                leve.setText("B2");
                dialog12.show();
                break;


            case "GRISEOTA1":
                cod.setText("GRISEOTA1");
                nam.setText("Griseofulvin Tablet, 125 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.10");
                leve.setText("B1");
                dialog12.show();
                break;

            case "GRISEOTA2":
                cod.setText("GRISEOTA2");
                nam.setText("Griseofulvin Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("B1");
                dialog12.show();
                break;

        }
        // }
        //else if(groupPosition==7){
        switch (selected){
            case "HALOPEIN1":
                cod.setText("HALOPEIN1");
                nam.setText("Haloperidol Injection, 5 mg/5 mL");
                uni.setText("Ampoule");
                price.setText("GHs 4.00");
                leve.setText("SD");
                dialog12.show();
                break;

            case "HALOPETA1":
                cod.setText("HALOPETA1");
                nam.setText("Haloperidol Tablet, 0.5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.52");
                leve.setText("C");
                dialog12.show();
                break;

            case "HALOPETA2":
                cod.setText("HALOPETA2");
                nam.setText("Haloperidol Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.68");
                leve.setText("C");
                dialog12.show();
                break;

            case "HALOPETA3":
                cod.setText("HALOPETA3");
                nam.setText("Haloperidol Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.20");
                leve.setText("C");
                dialog12.show();
                break;

            case "HEPARIIN1":
                cod.setText("HEPARIIN1");
                nam.setText("Heparin Injection, 1000 units/mL in 5 mL");
                uni.setText("Ampoule");
                price.setText("GHs 11.50");
                leve.setText("D");
                dialog12.show();
                break;

            case "HEPARIIN2":
                cod.setText("HEPARIIN2");
                nam.setText("Heparin Injection, 5000 units/mL in 1 mL");
                uni.setText("Ampoule");
                price.setText("GHs 10.00");
                leve.setText("D");
                dialog12.show();
                break;

            case "HEPARIIN3":
                cod.setText("HEPARIIN2");
                nam.setText("Heparin Injection, 5000 units/mL in 5 mL");
                uni.setText("Vial");
                price.setText("GHs 26.45");
                leve.setText("B2");
                dialog12.show();
                break;

            case "HUIMTEIN1":
                cod.setText("HUIMTEIN1");
                nam.setText("Human Immune Tetanus Globulins Injection, 250 IU/mL");
                uni.setText("1 mL");
                price.setText("GHs 26.60");
                leve.setText("B1");
                dialog12.show();
                break;

            case "HUIMTEIN2":
                cod.setText("HUIMTEIN2");
                nam.setText("Human Immune Tetanus Globulins Injection, 500 IU/mL");
                uni.setText("2 mL");
                price.setText("GHs 5.00");
                leve.setText("B1");
                dialog12.show();

            case "HYDRALIN1":
                cod.setText("HYDRALIN1");
                nam.setText("Hydralazine Injection, 20 mg");
                uni.setText("Ampoule");
                price.setText("GHs 22.30");
                leve.setText("C");
                dialog12.show();

            case "HYDRALTA1":
                cod.setText("HYDRALTA1");
                nam.setText("Hydralazine Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.00");
                leve.setText("C");
                dialog12.show();

            case "HYDROCCR1":
                cod.setText("HYDROCCR1");
                nam.setText("Hydrocortisone Cream, 1%");
                uni.setText("15 G");
                price.setText("GHs 5.75");
                leve.setText("B1");
                dialog12.show();

            case "HYDROCID1":
                cod.setText("HYDROCID1");
                nam.setText("Hydrocortisone Eye Drops, 1%");
                uni.setText("5 mL");
                price.setText("GHs 7.75");
                leve.setText("C");
                dialog12.show();

            case "HYDROCEO1":
                cod.setText("HYDROCEO1");
                nam.setText("Hydrocortisone Eye Ointment, 1%");
                uni.setText("5 G");
                price.setText("GHs 6.00");
                leve.setText("C");
                dialog12.show();

            case "HYSOSUIN1":
                cod.setText("HYDROCEO1");
                nam.setText("Hydrocortisone Sodium Succinate Injection, 100 mg");
                uni.setText("Vial");
                price.setText("GHs 3.00");
                leve.setText("M");
                dialog12.show();

            case "HYDROXIN1":
                cod.setText("HYDROXIN1");
                nam.setText("Hydroxocobalamin Injection, 1 mg/mL");
                uni.setText("1 mL");
                price.setText("GHs 7.50");
                leve.setText("D");
                dialog12.show();
                break;

            case "HYOBUTIN1":
                cod.setText("HYOBUTIN1");
                nam.setText("Hyoscine Butylbromide Injection, 20 mg/ mL");
                uni.setText("1 mL");
                price.setText("GHs 1.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "HYOBUTTA1":
                cod.setText("HYOBUTTA1");
                nam.setText("Hyoscine Butylbromide Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.67");
                leve.setText("M");
                dialog12.show();
                break;
        }
        // }
        //else if(groupPosition==8){
        switch(selected){
            case "IBUPROSU1":
                cod.setText("IBUPROSU1");
                nam.setText("Ibuprofen Suspension, 100 mg/5 mL");
                uni.setText("100 mL");
                price.setText("GHs 3.00");
                leve.setText("M");
                dialog12.show();
                break;

            case "IBUPROTA1":
                cod.setText("IBUPROTA1");
                nam.setText("Ibuprofen Tablet, 200 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("M");
                dialog12.show();
                break;

            case "IBUPROTA2":
                cod.setText("IBUPROTA2");
                nam.setText("Ibuprofen Tablet, 400 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.08");
                leve.setText("M");
                dialog12.show();
                break;

            case "IMIPRATA1":
                cod.setText("IMIPRATA1");
                nam.setText("Imipramine Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.08");
                leve.setText("M");
                dialog12.show();
                break;

            case "INPRMIIN1":
                cod.setText("INPRMIIN1");
                nam.setText("Insulin premixed (30/70) HM Injection, 100 units/mL in 10 mL");
                uni.setText("Vial");
                price.setText("GHs 41.60");
                leve.setText("B2");
                dialog12.show();
                break;

            case "INSSOLIN1":
                cod.setText("INPRMIIN1");
                nam.setText("Insulin premixed (30/70) HM Injection, 100 units/mL in 10 mL");
                uni.setText("Vial");
                price.setText("GHs 63.70");
                leve.setText("B2");
                dialog12.show();
                break;

            case "INTRALSO1":
                cod.setText("INTRALSO1");
                nam.setText("Intralipid Solution (for TPN)");
                uni.setText("500 mL");
                price.setText("GHs 51.50");
                leve.setText("D");
                dialog12.show();
                break;

            case "IROPOLCA1":
                cod.setText("IROPOLCA1");
                nam.setText("Iron (III) Polymaltose Complex Capsule");
                uni.setText("Capsule");
                price.setText("GHs 0.25");
                leve.setText("A");
                dialog12.show();
                break;


            case "IROPOLSU1":
                cod.setText("IROPOLSU1");
                nam.setText("Iron (III) Polymaltose Complex Suspension");
                uni.setText("200 mL");
                price.setText("GHs 5.05");
                leve.setText("A");
                dialog12.show();
                break;

            case "IRODEXIN1":
                cod.setText("IRODEXIN1");
                nam.setText("Iron Dextran Injection, 50 mg/mL");
                uni.setText("2 mL");
                price.setText("GHs 1.90");
                leve.setText("C");
                dialog12.show();
                break;

            case "IROSUCIN1":
                cod.setText("IROSUCIN1");
                nam.setText("Iron Sucrose Injection, 20 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 17.42");
                leve.setText("D");
                dialog12.show();
                break;

            case "ISOINSIN1":
                cod.setText("ISOINSIN1");
                nam.setText("Isophane Insulin Injection (HM), 100 units/mL in 10 mL");
                uni.setText("Vial");
                price.setText("GHs 63.70");
                leve.setText("C");
                dialog12.show();
                break;

            case "ISODINTA1":
                cod.setText("ISODINTA1");
                nam.setText("Isosorbide Dinitrate Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.50");
                leve.setText("C");
                dialog12.show();
                break;

            case "ITRACOCA1":
                cod.setText("ITRACOCA1");
                nam.setText("Itraconazole Capsule, 100 mg");
                uni.setText("Capusle");
                price.setText("GHs 6.00");
                leve.setText("D");
                dialog12.show();
                break;

            case "ITRACOSU1":
                cod.setText("ITRACOSU1");
                nam.setText("Itraconazole Suspension, 10 mg/mL");
                uni.setText("30 mL");
                price.setText("GHs 52.00");
                leve.setText("D");
                dialog12.show();
                break;

        }
        //}
        //else if(groupPosition==9){
        switch(selected){
            case "KETOCOCR1":
                cod.setText("KETOCOCR1");
                nam.setText("Ketoconazole Cream, 30g");
                uni.setText("Tube");
                price.setText("GHs 5.50");
                leve.setText("B1");
                dialog12.show();
                break;

            case "KETOCOTA1":
                cod.setText("KETOCOCR1");
                nam.setText("Ketoconazole Tablet, 200g");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("C");
                dialog12.show();
                break;

        }
        //}
        //else if(groupPosition==10){
        switch (selected){
            case "LABETAIN1":
                cod.setText("LABETAIN1");
                nam.setText("Labetalol Injection, 5 mg/mL in 20 mL");
                uni.setText("Ampoule");
                price.setText("GHs 70.00");
                leve.setText("D");
                dialog12.show();
                break;

            case "LABETATA1":
                cod.setText("LABETATA1");
                nam.setText("Labetalol Tablet, 20 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.04");
                leve.setText("D");
                dialog12.show();
                break;

            case "LABETATA2":cod.setText("LABETATA2");
                nam.setText("Labetalol Tablet, 200 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.95");
                leve.setText("D");
                dialog12.show();
                break;

            case "LACTULLI1":
                cod.setText("LACTULLI1");
                nam.setText("Lactulose Liquid ϯ.ϭ–ϯ.7 g/ϱ mL");
                uni.setText("300 mL");
                price.setText("GHs 24.65");
                leve.setText("C");
                dialog12.show();
                break;

            case "LEVSODTA1":
                cod.setText("LEVSODTA1");
                nam.setText("Levothyroxine Sodium Tablet, 25 microgram");
                uni.setText("Tablet");
                price.setText("GHs 0.60");
                leve.setText("C");
                dialog12.show();
                break;

            case "LEVSODTA2":
                cod.setText("LEVSODTA2");
                nam.setText("Levothyroxine Sodium Tablet, 50 microgram");
                uni.setText("Tablet");
                price.setText("GHs 0.70");
                leve.setText("C");
                dialog12.show();
                break;

            case "LEVSODTA3":
                cod.setText("LEVSODTA3");
                nam.setText("Levothyroxine Sodium Tablet, 100 microgram");
                uni.setText("Tablet");
                price.setText("GHs 0.80");
                leve.setText("C");
                dialog12.show();
                break;

            case "LIDOCACR1":
                cod.setText("LIDOCACR1");
                nam.setText("Lidocaine Cream, 2%");
                uni.setText("15 G");
                price.setText("GHs 9.00");
                leve.setText("A");
                dialog12.show();
                break;

            case "LIDOCAGE1":
                cod.setText("LIDOCAGE1");
                nam.setText("Lidocaine Gel, 4%");
                uni.setText("15 G");
                price.setText("GHs 11.25");
                leve.setText("M");
                dialog12.show();
                break;

            case "LISHYDTA1":
                cod.setText("LISHYDTA1");
                nam.setText("Lisinopril + Hydrochlorthiazide Tablet, (10 mg + 12.5 mg)");
                uni.setText("Tablet");
                price.setText("GHs 1.20");
                leve.setText("C");
                dialog12.show();
                break;

            case "LISHYDTA2":
                cod.setText("LISHYDTA2");
                nam.setText("Lisinopril + Hydrochlorthiazide Tablet, (20 mg + 12.5 mg)");
                uni.setText("Tablet");
                price.setText("GHs 1.90");
                leve.setText("C");
                dialog12.show();
                break;

            case "LISINOTA1":
                cod.setText("LISINOTA1");
                nam.setText("Lisinopril Tablet, 2.5 mg)");
                uni.setText("Tablet");
                price.setText("GHs 0.21");
                leve.setText("B2");
                dialog12.show();
                break;

            case "LISINOTA2":
                cod.setText("LISINOTA2");
                nam.setText("Lisinopril Tablet, 5 mg)");
                uni.setText("Tablet");
                price.setText("GHs 0.21");
                leve.setText("B2");
                dialog12.show();
                break;

            case "LISINOTA3":
                cod.setText("LISINOTA3");
                nam.setText("Lisinopril Tablet, 10 mg)");
                uni.setText("Tablet");
                price.setText("GHs 0.30");
                leve.setText("B2");
                dialog12.show();
                break;

            case "LISINOTA4":
                cod.setText("LISINOTA4");
                nam.setText("Lisinopril Tablet, 20 mg)");
                uni.setText("Tablet");
                price.setText("GHs 0.43");
                leve.setText("B2");
                dialog12.show();
                break;

            case "LODOXAID1":
                cod.setText("LODOXAID1");
                nam.setText("Lodoxamide Eye Drops, 0.1%");
                uni.setText("10 mL");
                price.setText("GHs32.00");
                leve.setText("C");
                dialog12.show();
                break;

            case "LORAZEIN1":
                cod.setText("LORAZEIN1");
                nam.setText("Lorazepam Injection, 4 mg/mL in 1mL");
                uni.setText("Ampoule");
                price.setText("GHs 3.10");
                leve.setText("D");
                dialog12.show();
                break;

            case "LORAZETA1":
                cod.setText("LORAZETA1");
                nam.setText("Lorazepam Tablet, 1 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.45");
                leve.setText("B2");
                dialog12.show();
                break;

            case "LORAZETA2":

                cod.setText("LORAZETA2");
                nam.setText("Lorazepam Tablet, 2 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.47");
                leve.setText("B2");
                dialog12.show();
                break;


            case "LORAZETA3":


                cod.setText("LORAZETA3");
                nam.setText("Lorazepam Tablet, 2.5 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("D");
                dialog12.show();
                break;

            case "LOSARTTA1":

                cod.setText("LOSARTTA1");
                nam.setText("Losartan Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.40");
                leve.setText("D");
                dialog12.show();
                break;

            case "LOSARTTA2":

                cod.setText("LOSARTTA2");
                nam.setText("Losartan Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.50");
                leve.setText("C");
                dialog12.show();
                break;

            case "LOSARTTA3":


                cod.setText("LOSARTTA3");
                nam.setText("Losartan Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.80");
                leve.setText("D");
                dialog12.show();
                break;

        }
        //}
        //else if(groupPosition==11){
        switch(selected){


            case "MAGSULIN1":
                cod.setText("MAGSULIN1");
                nam.setText("Magnesium Sulphate Injection, 20% (10 mL)");
                uni.setText("Ampoule");
                price.setText("GHs 2.05");
                leve.setText("M");
                dialog12.show();
                break;


            case "MAGSULIN3":
                cod.setText("MAGSULIN3");
                nam.setText("Magnesium Sulphate Injection, 50% (10 mL)");
                uni.setText("Ampoule");
                price.setText("GHs 6.8");
                leve.setText("C");
                dialog12.show();
                break;

            case "MAGSULPO1":
                cod.setText("MAGSULPO1");
                nam.setText("Magnesium Sulphate Salt");
                uni.setText("1G");
                price.setText("GHs 0.5");
                leve.setText("C");
                dialog12.show();
                break;


            case "MATRALMI1":
                cod.setText("MATRALMI1");
                nam.setText("Magnesium Trisilicate + Aluminium Hydroxide Mixture");
                uni.setText("200mL");
                price.setText("GHs 4.18");
                leve.setText("A");
                dialog12.show();
                break;


            case "MATRALTA1":
                cod.setText("MATRALTA1");
                nam.setText("Magnesium Trisilicate + Aluminium Hydroxide Tablet");
                uni.setText("Tablet");
                price.setText("GHs 0.1");
                leve.setText("A");
                dialog12.show();
                break;

            case "MAGTRIMI1":
                cod.setText("MAGTRIMI1");
                nam.setText("Magnesium Trisilicate Mixture");
                uni.setText("200mL");
                price.setText("GHs 2.5");
                leve.setText("A");
                dialog12.show();
                break;

            case "MAGTRITA1":
                cod.setText("MAGTRITA1");
                nam.setText("Magnesium Trisilicate Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.07");
                leve.setText("A");
                dialog12.show();
                break;

            case "MANNITIN1":
                cod.setText("MANNITIN1");
                nam.setText("Mannitol Injection, 10%");
                uni.setText("500mL");
                price.setText("GHs 7.87");
                leve.setText("C");
                dialog12.show();
                break;


            case "MANNITIN2":
                cod.setText("MANNITIN2");
                nam.setText("Mannitol Injection, 20%");
                uni.setText("500mL");
                price.setText("GHs 9.89");
                leve.setText("C");
                dialog12.show();
                break;


            case "MEBENDSU1":
                cod.setText("MEBENDSU1");
                nam.setText("Mebendazole Suspension, 100 mg/5 mL");
                uni.setText("30mL");
                price.setText("GHs 4");
                leve.setText("A");
                dialog12.show();
                break;


            case "MEBENDTA1":
                cod.setText("MEBENDTA1");
                nam.setText("Mebendazole Tablet, 100 mg");
                uni.setText("6 Tablets");
                price.setText("GHs 1.8");
                leve.setText("A");
                dialog12.show();
                break;


            case "MEBENDTA2":
                cod.setText("MEBENDTA2");
                nam.setText("Mebendazole Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 2.5");
                leve.setText("A");
                dialog12.show();
                break;


            case "MEBEVETA1":
                cod.setText("MEBEVETA1");
                nam.setText("Mebeverine Tablet, 135 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.81");
                leve.setText("C");
                dialog12.show();
                break;


            case "MEDACETA1":
                cod.setText("MEDACETA1");
                nam.setText("Medroxyprogesterone Acetate Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("C");
                dialog12.show();
                break;

            case "MEFACICA1":
                cod.setText("MEFACICA1");
                nam.setText("Mefenamic Acid Capsule, 250 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.3");
                leve.setText("M");
                dialog12.show();
                break;


            case "MEFACITA1":
                cod.setText("MEFACIAT1");
                nam.setText("Mefenamic Acid Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.4");
                leve.setText("M");
                dialog12.show();
                break;



            case "METFORTA1":
                cod.setText("METFORTA1");
                nam.setText("Metformin Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.11");
                leve.setText("B2");
                dialog12.show();
                break;



            case "METHOTIN1":
                cod.setText("METHOTIN1");
                nam.setText("Methotrexate Injection, 2.5 mg/ mL");
                uni.setText("Ampoule");
                price.setText("GHs 5.5");
                leve.setText("D");
                dialog12.show();
                break;


            case "METHOTIN2":
                cod.setText("METHOTIN2");
                nam.setText("Methotrexate Injection, 25 mg/ mL in 2mL");
                uni.setText("Ampoule");
                price.setText("GHs 15");
                leve.setText("D");
                dialog12.show();
                break;


            case "METHOTTA1":
                cod.setText("METHOTTA1");
                nam.setText("Methotrexate Tablet, 2.5 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.51");
                leve.setText("D");
                dialog12.show();
                break;


            case "METHOTTA2":
                cod.setText("METHOTTA2");
                nam.setText("Methotrexate Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 2.5");
                leve.setText("D");
                dialog12.show();
                break;



            case "METCELID1":
                cod.setText("METCELID1");
                nam.setText("Methyl Cellulose Eye Drops, 0.3%");
                uni.setText("10mL");
                price.setText("GHs 14");
                leve.setText("B2");
                dialog12.show();
                break;



            case "METHYLTA1":
                cod.setText("METHYLTA1");
                nam.setText("Methyldopa Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.35");
                leve.setText("B2");
                dialog12.show();
                break;


            case "METOCLIN1":
                cod.setText("METOCLIN1");
                nam.setText("Metoclopramide Injection, 5 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 1.5");
                leve.setText("C");
                dialog12.show();
                break;

            case "METOCLSY1":
                cod.setText("METOCLSY1");
                nam.setText("Metoclopramide Syrup, 5 mg/5 mL");
                uni.setText("200mL");
                price.setText("GHs 21.15");
                leve.setText("C");
                dialog12.show();
                break;


            case "METOCLTA1":
                cod.setText("METOCLTA1");
                nam.setText("Metoclopramide Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.3");
                leve.setText("C");
                dialog12.show();
                break;


            case "METOLATA1":
                cod.setText("METOLATA1");
                nam.setText("Metolazone Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.1");
                leve.setText("D");
                dialog12.show();
                break;


            case "METRONRE1":
                cod.setText("METRONRE1");
                nam.setText("Metronidazole Suppository, 500mg");
                uni.setText("Supp");
                price.setText("GHs 2");
                leve.setText("B2");
                dialog12.show();
                break;



            case "METRONSU1":
                cod.setText("METRONSU1");
                nam.setText("Metronidazole Suspension, 100 mg/5 mL (as benzoate)");
                uni.setText("100mL");
                price.setText("GHs 3");
                leve.setText("M");
                dialog12.show();
                break;

            case "METRONSU2":
                cod.setText("METRONSU2");
                nam.setText("Metronidazole Suspension, 200 mg/5 mL (as benzoate)");
                uni.setText("100mL");
                price.setText("GHs 3");
                leve.setText("M");
                dialog12.show();
                break;


            case "METRONTA1":
                cod.setText("METRONTA1");
                nam.setText("Metronidazole  Tablet, 200mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("M");
                dialog12.show();
                break;


            case "METRONTA2":
                cod.setText("METRONTA2");
                nam.setText("Metronidazole  Tablet, 400mg");
                uni.setText("Tablet");
                price.setText("GHs 0.08");
                leve.setText("M");
                dialog12.show();
                break;

            case "MICHYDCR1":
                cod.setText("MICHYDCR1");
                nam.setText("Miconazole + Hydrocortisone Cream, 2% + 1%");
                uni.setText("15G");
                price.setText("GHs 7");
                leve.setText("C");
                dialog12.show();
                break;


            case "MICONACR1":
                cod.setText("MICONACR1");
                nam.setText("Miconazole Cream, 2% ");
                uni.setText("15G");
                price.setText("GHs 6");
                leve.setText("B2");
                dialog12.show();
                break;


            case "MICONAOG1":
                cod.setText("MICONAOG1");
                nam.setText("Miconazole Oral Gel, 25 mg/mL");
                uni.setText("40G");
                price.setText("GHs 30");
                leve.setText("B2");
                dialog12.show();
                break;

            case "MICONAVP1":
                cod.setText("MICONAVP1");
                nam.setText("Miconazole Ovule, 400 mg");
                uni.setText("3 Ovules");
                price.setText("GHs 11.09");
                leve.setText("A");
                dialog12.show();
                break;


            case "MIDAZOIN1":
                cod.setText("MIDAZOIN1");
                nam.setText("Midazolam Injection, 5 mg/5mL");
                uni.setText("Ampoule");
                price.setText("GHs 12.26");
                leve.setText("C");
                dialog12.show();
                break;


            case "MIDAZOTA1":
                cod.setText("MIDAZOTA1");
                nam.setText("Midazolam Tablet, 15 mg");
                uni.setText("Tablet");
                price.setText("GHs 3");
                leve.setText("C");
                dialog12.show();
                break;

            case "MORPHIIN1":
                cod.setText("MORPHIIN1");
                nam.setText("Morphine Injection, 10 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 6");
                leve.setText("C");
                dialog12.show();
                break;
            case "MORPHIIN2":
                cod.setText("MORPHIIN2");
                nam.setText("Morphine Injection, 10 mg/mL(Preservative)");
                uni.setText("Ampoule");
                price.setText("GHs 14");
                leve.setText("SD");
                dialog12.show();
                break;


            case "MORSULTA1":
                cod.setText("MORSULTA1");
                nam.setText("Morphine Sulphate Tablet, 10 mg (Slow");
                uni.setText("Tablet");
                price.setText("GHs 2");
                leve.setText("C");
                dialog12.show();
                break;


            case "MORSULTA2":
                cod.setText("MORSULTA2");
                nam.setText("Morphine Sulphate Tablet, 30 mg (Slow");
                uni.setText("Tablet");
                price.setText("GHs 5.03");
                leve.setText("C");
                dialog12.show();
                break;


            case "MULTIVDR1":
                cod.setText("MULTIVDR1");
                nam.setText("Multivitamin Drops");
                uni.setText("20mL");
                price.setText("GHs 4.58");
                leve.setText("A");
                dialog12.show();
                break;


            case "MULTIVSY1":
                cod.setText("MULTIVSY1");
                nam.setText("Multivitamin Syrup");
                uni.setText("125mL");
                price.setText("GHs 2.6");
                leve.setText("A");
                dialog12.show();
                break;


            case "MULTIVTA1":
                cod.setText("MULTIVTA1");
                nam.setText("Multivitamin Tablet (Blister Pack)");
                uni.setText("10 Tablets");
                price.setText("GHs 0.14");
                leve.setText("A");
                dialog12.show();
                break;

        }

//N
        switch(selected){
            case "NALOXOIN1":
                cod.setText("NALOXOIN1");
                nam.setText("Naloxone Injection, 400 microgram/mL in 1mL");
                uni.setText("Ampoule");
                price.setText("GHs 11.3");
                leve.setText("C");
                dialog12.show();
                break;

            case "NEOMYCTA1":
                cod.setText("NEOMYCTA1");
                nam.setText("Neomycin Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 3.25");
                leve.setText("C");
                dialog12.show();
                break;


            case "NEOBROTA1":
                cod.setText("NEOBROTA1");
                nam.setText("Neostigmine Bromide Tablet, 15 mg");
                uni.setText("Tablet");
                price.setText("GHs 7");
                leve.setText("D");
                dialog12.show();
                break;


            case "NEOSTIIN1":
                cod.setText("NEOSTIIN1");
                nam.setText("Neostigmine Injection, 2.5 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 5.9");
                leve.setText("C");
                dialog12.show();
                break;



            case "NIFEDICA1":
                cod.setText("NIFEDICA1");
                nam.setText("Nifedipine Capsule, 10 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.49");
                leve.setText("D");
                dialog12.show();
                break;



            case "NIFEDITA1":
                cod.setText("NIFEDITA1");
                nam.setText("Nifedipine Tablet, 10 mg (slow release)");
                uni.setText("Tablet");
                price.setText("GHs 0.25");
                leve.setText("B1");
                dialog12.show();
                break;



            case "NIFEDITA2":
                cod.setText("NIFEDITA2");
                nam.setText("Nifedipine Tablet, 20 mg (slow release)");
                uni.setText("Tablet");
                price.setText("GHs 0.17");
                leve.setText("B1");
                dialog12.show();
                break;

            case "NIFEDITA3":
                cod.setText("NIFEDITA3");
                nam.setText("Nifedipine Tablet, 30 mg (GITS)");
                uni.setText("Tablet");
                price.setText("GHs 0.55");
                leve.setText("B2");
                dialog12.show();
                break;


            case "NITROFTA1":
                cod.setText("NITROFTA1");
                nam.setText("Nitrofurantoin Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.8");
                leve.setText("B2");
                dialog12.show();
                break;



            case "NORETHTA1":
                cod.setText("NORETHTA1");
                nam.setText("Norethisterone Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.62");
                leve.setText("D");
                dialog12.show();
                break;



            case "NYSTATOI1":
                cod.setText("NYSTATOI1");
                nam.setText("Nystatin Ointment, 100,000 IU");
                uni.setText("30G");
                price.setText("GHs 15");
                leve.setText("B2");
                dialog12.show();
                break;


            case "NYSTATTA1":
                cod.setText("NYSTATTA1");
                nam.setText("Nystatin Pessary, 100,000 IU");
                uni.setText("Pessary");
                price.setText("GHs 0.85");
                leve.setText("B1");
                dialog12.show();
                break;



            case "NYSTATSU1":
                cod.setText("NYSTATSU1");
                nam.setText("Nystatin Suspension, 100,000 IU/mL");
                uni.setText("15mL");
                price.setText("GHs 9");
                leve.setText("B2");
                dialog12.show();
                break;


            case "NYSTATTA2":
                cod.setText("NYSTATTA2");
                nam.setText("Nystatin Tablet, 500,000 IU");
                uni.setText("Tablet");
                price.setText("GHs 0.72");
                leve.setText("B1");
                dialog12.show();
                break;

        }



//O
        switch(selected){


            case "OMEPRAIN2":
                cod.setText("OMEPRAIN2");
                nam.setText("Omeprazole Injection, 40 mg");
                uni.setText("Vial");
                price.setText("GHs 12");
                leve.setText("B2");
                dialog12.show();
                break;


            case "OMEPRATA2":
                cod.setText("OMEPRATA2");
                nam.setText("Omeprazole Tablet, 20 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.3");
                leve.setText("B2");
                dialog12.show();
                break;


            case "ORRESAPO1":
                cod.setText("ORRESAPO1");
                nam.setText("Oral Rehydration Salts Powder");
                uni.setText("Satchet");
                price.setText("GHs 0.6");
                leve.setText("A");
                dialog12.show();
                break;


            case "OXYTOCIN1":
                cod.setText("OXYTOCIN1");
                nam.setText("Oxytocin Injection, 5 units/mL");
                uni.setText("Ampoule");
                price.setText("GHs 0.15");
                leve.setText("A");
                dialog12.show();
                break;


            case "OXYTOCIN2":
                cod.setText("OXYTOCIN2");
                nam.setText("Oxytocin Injection, 10 units/mL");
                uni.setText("Ampoule");
                price.setText("GHs 0.3");
                leve.setText("A");
                dialog12.show();
                break;


        }



//P
        switch(selected){
            case "PACLITIN1":
                cod.setText("PACLITIN1");
                nam.setText("Paclitaxel Injection, 6 mg/mL in 5 mL");
                uni.setText("Vial");
                price.setText("GHs 200");
                leve.setText("D");
                dialog12.show();
                break;


            case "PARACERE1":
                cod.setText("PARACERE1");
                nam.setText("Paracetamol Suppository, 125 mg");
                uni.setText("Supp");
                price.setText("GHs 0.56");
                leve.setText("A");
                dialog12.show();
                break;


            case "PARACERE2":
                cod.setText("PARACERE2");
                nam.setText("Paracetamol Suppository, 250 mg");
                uni.setText("Supp");
                price.setText("GHs 0.7");
                leve.setText("A");
                dialog12.show();
                break;


            case "PARACERE3":
                cod.setText("PARACERE3");
                nam.setText("Paracetamol Suppository, 500 mg");
                uni.setText("Supp");
                price.setText("GHs 0.82");
                leve.setText("A");
                dialog12.show();
                break;



            case "PARACESY1":
                cod.setText("PARACESY1");
                nam.setText("Paracetamol Syrup, 120 mg/5 mL");
                uni.setText("125mL");
                price.setText("GHs 3.4");
                leve.setText("A");
                dialog12.show();
                break;


            case "PARACETA1":
                cod.setText("PARACETA1");
                nam.setText("Paracetamol Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.03");
                leve.setText("A");
                dialog12.show();
                break;
            case "PARAFFLI1":
                cod.setText("PARAFFLI1");
                nam.setText("Paraffin Liquid");
                uni.setText("100mL");
                price.setText("GHs 3.36");
                leve.setText("A");
                dialog12.show();
                break;


            case "PETHIDIN1":
                cod.setText("PETHIDIN1");
                nam.setText("Pethidine Injection, 50 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 5.7");
                leve.setText("B1");
                dialog12.show();
                break;


            case "PHENOBEL1":
                cod.setText("PHENOBEL1");
                nam.setText("Phenobarbital Elixir, 15 mg/5 mL");
                uni.setText("100mL");
                price.setText("GHs 5.91");
                leve.setText("B1");
                dialog12.show();
                break;




            case "PHENOBIN1":
                cod.setText("PHENOBIN1");
                nam.setText("Phenobarbital Injection, 200 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 5.7");
                leve.setText("B1");
                dialog12.show();
                break;



            case "PHENOBTA1":
                cod.setText("PHENOBTA1");
                nam.setText("Phenobarbital Tablet, 30 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.03");
                leve.setText("B1");
                dialog12.show();
                break;

            case "PHENOBTA2":
                cod.setText("PHENOBTA2");
                nam.setText("Phenobarbital Tablet, 60 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.04");
                leve.setText("B1");
                dialog12.show();
                break;


            case "PHENOLIN1":
                cod.setText("PHENOLIN1");
                nam.setText("Phenol 5% in Almond Oil Injection");
                uni.setText("50mL");
                price.setText("GHs 7.48");
                leve.setText("SD");
                dialog12.show();
                break;
            case "PHEPENTA1":
                cod.setText("PHEPENTA1");
                nam.setText("Phenoxymethyl Penicillin Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.09");
                leve.setText("M");
                dialog12.show();
                break;


            case "PHENYTIN1":
                cod.setText("PHENYTIN1");
                nam.setText("Phenytoin Injection, 50 mg/mL  in 5 mL");
                uni.setText("Ampoule");
                price.setText("GHs 24");
                leve.setText("D");
                dialog12.show();
                break;


            case "PHENYTCA1":
                cod.setText("PHENYTCA1");
                nam.setText("Phenytoin Sodium Capsule, 50 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.3");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PHENYTCA2":
                cod.setText("PHENYTCA2");
                nam.setText("Phenytoin Sodium Capsule, 100 mg");
                uni.setText("Capsule");
                price.setText("GHs 0.32");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PHENYTTA1":
                cod.setText("PHENYTTA1");
                nam.setText("Phenytoin Sodium Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.25");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PHYTOMIN1":
                cod.setText("PHYTOMIN1");
                nam.setText("Phytomenadione Injection, 1 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 2.25");
                leve.setText("M");
                dialog12.show();
                break;


            case "PHYTOMIN2":
                cod.setText("PHYTOMIN2");
                nam.setText("Phytomenadione Injection, 10 mg/mL");
                uni.setText("Ampoule");
                price.setText("GHs 2.5");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PILOCAID1":
                cod.setText("PILOCAID1");
                nam.setText("Pilocarpine Eye Drops, 2%");
                uni.setText("10mL");
                price.setText("GHs 12.5");
                leve.setText("C");
                dialog12.show();
                break;


            case "PILOCAID2":
                cod.setText("PILOCAID2");
                nam.setText("Pilocarpine Eye Drops, 4%");
                uni.setText("10mL");
                price.setText("GHs 14");
                leve.setText("C");
                dialog12.show();
                break;


            case "PIOGLITA1":
                cod.setText("PIOGLITA1");
                nam.setText("Pioglitazone Tablet, 15 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.34");
                leve.setText("C");
                dialog12.show();
                break;
            case "PIOGLITA2":
                cod.setText("PIOGLITA2");
                nam.setText("Pioglitazone Tablet, 30 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.4");
                leve.setText("C");
                dialog12.show();
                break;


            case "PIRACETA1":
                cod.setText("PIRACETA1");
                nam.setText("Piracetam Tablet, 800 mg");
                uni.setText("Tablet");
                price.setText("GHs 2");
                leve.setText("C");
                dialog12.show();
                break;


            case "POTCHLIN1":
                cod.setText("POTCHLIN1");
                nam.setText("Potassium Chloride Injection, 20 mEq/10 mL");
                uni.setText("Vial");
                price.setText("GHs 5");
                leve.setText("C");
                dialog12.show();
                break;



            case "POTCHLTA1":
                cod.setText("POTCHLTA1");
                nam.setText("Potassium Chloride Tablet, 600 mg (Enteric Coated)");
                uni.setText("Tablet");
                price.setText("GHs 0.33");
                leve.setText("B2");
                dialog12.show();
                break;

            case "POTCITMI1":
                cod.setText("POTCITMI1");
                nam.setText("Potassium Citrate Mixture BP");
                uni.setText("200mL");
                price.setText("GHs 2.9");
                leve.setText("A");
                dialog12.show();
                break;


            case "POVIDOSO1":
                cod.setText("POVIDOSO1");
                nam.setText("Povidone Iodine Aqueous Solution, 10%");
                uni.setText("100mL");
                price.setText("GHs 7");
                leve.setText("B1");
                dialog12.show();
                break;


            case "POVIDOOI1":
                cod.setText("POVIDOOI1");
                nam.setText("Povidone Iodine Ointment, 10%");
                uni.setText("10G");
                price.setText("GHs 4.2");
                leve.setText("A");
                dialog12.show();
                break;


            case "PRAZIQTA1":
                cod.setText("PRAZIQTA1");
                nam.setText("Praziquantel Tablet, 600 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.56");
                leve.setText("B1");
                dialog12.show();
                break;


            case "PRAZOSTA1":
                cod.setText("PRAZOSTA1");
                nam.setText("Prazosin Tablet, 500 microgram");
                uni.setText("Tablet");
                price.setText("GHs 0.47");
                leve.setText("D");
                dialog12.show();
                break;


            case "PREDNIID1":
                cod.setText("PREDNIID1");
                nam.setText("Prednisolone Eye Drops, 0.5%");
                uni.setText("10mL");
                price.setText("GHs 4.25");
                leve.setText("SD");
                dialog12.show();
                break;


            case "PREDNIID2":
                cod.setText("PREDNIID2");
                nam.setText("Prednisolone Eye Drops, 1%");
                uni.setText("10mL");
                price.setText("GHs 5.9");
                leve.setText("SD");
                dialog12.show();
                break;


            case "PREDNITA1":
                cod.setText("PREDNITA1");
                nam.setText("Prednisolone Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.08");
                leve.setText("B2");
                dialog12.show();
                break;

            case "PRIMIDTA1":
                cod.setText("PRIMIDTA1");
                nam.setText("Primidone Tablet, 250 mg");
                uni.setText("Tablet");
                price.setText("GHs 1");
                leve.setText("C");
                dialog12.show();
                break;


            case "PROBENIN1":
                cod.setText("PROBENIN1");
                nam.setText("Procaine Benzylpenicillin Injection, 4 MU");
                uni.setText("Vial");
                price.setText("GHs 1.1");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PROHYDEL1":
                cod.setText("PROHYDEL1");
                nam.setText("Promethazine Hydrochloride Elixir, 5 mg/5 mL");
                uni.setText("60mL");
                price.setText("GHs 2");
                leve.setText("M");
                dialog12.show();
                break;


            case "PROHYDIN1":
                cod.setText("PROHYDIN1");
                nam.setText("Promethazine Hydrochloride Injection, 25 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 0.6");
                leve.setText("M");
                dialog12.show();
                break;

            case "PROMETTA1":
                cod.setText("PROMETTA1");
                nam.setText("Promethazine Hydrochloride Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.06");
                leve.setText("M");
                dialog12.show();
                break;


            case "PROTHETA1":
                cod.setText("PROTHETA1");
                nam.setText("Promethazine Theoclate Tablet, 25 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.2");
                leve.setText("M");
                dialog12.show();
                break;


            case "PROPRAIN1":
                cod.setText("PROPRAIN1");
                nam.setText("Propranolol Injection, 1 mg/mL in 1mL");
                uni.setText("Ampoule");
                price.setText("GHs 2.05");
                leve.setText("D");
                dialog12.show();
                break;


            case "PROPRATA1":
                cod.setText("PROPRATA1");
                nam.setText("Propranolol Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.2");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PROPRATA2":
                cod.setText("PROPRATA2");
                nam.setText("Propranolol Tablet, 40 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.06");
                leve.setText("B2");
                dialog12.show();
                break;


            case "PROPRATA3":
                cod.setText("PROPRATA3");
                nam.setText("Propranolol Tablet, 80 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.18");
                leve.setText("B2");
                dialog12.show();
                break;

            case "PROPYLTA1":
                cod.setText("PROPYLTA1");
                nam.setText("Propylthiouracil Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 6.75");
                leve.setText("D");
                dialog12.show();
                break;

            case "PROSULIN1":
                cod.setText("PROSULIN1");
                nam.setText("Protamine Sulphate Injection, 10 mg/mL in 5");
                uni.setText("Ampoule");
                price.setText("GHs 50");
                leve.setText("D");
                dialog12.show();
                break;


        }


//Q
        switch(selected){

            case "QUINININ1":
                cod.setText("QUINININ1");
                nam.setText("Quinine Injection, 300 mg/mL in 2 mL");
                uni.setText("Ampoule");
                price.setText("GHs 2.14");
                leve.setText("B2");
                dialog12.show();
                break;

            case "QUININSY1":
                cod.setText("QUININSY1");
                nam.setText("Quinine Syrup, 75 mg/5 mL");
                uni.setText("125mL");
                price.setText("GHs 8.5");
                leve.setText("B1");
                dialog12.show();
                break;
            case "QUININTA1":
                cod.setText("QUININTA1");
                nam.setText("Quinine Tablet, 300 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.35");
                leve.setText("M");
                dialog12.show();
                break;


        }


//R
        switch(selected){


            case "RAMIPRTA1":
                cod.setText("RAMIPRTA1");
                nam.setText("Ramipril Tablet, 2.5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.3");
                leve.setText("C");
                dialog12.show();
                break;




            case "RAMIPRTA2":
                cod.setText("RAMIPRTA2");
                nam.setText("Ramipril Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.5");
                leve.setText("C");
                dialog12.show();
                break;

            case "RANITITA1":
                cod.setText("RANITITA1");
                nam.setText("Ranitidine Tablet, 150 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.3");
                leve.setText("C");
                dialog12.show();
                break;



            case "RETSOFCA2":
                cod.setText("RETSOFCA2");
                nam.setText("Retinol Soft Capsule, 200,000 IU");
                uni.setText("Capsule");
                price.setText("GHs 0.2");
                leve.setText("A");
                dialog12.show();
                break;


            case "RINLACSO1":
                cod.setText("RINLACSO1");
                nam.setText("Ringer - Lactate Solution, 500 mL");
                uni.setText("500mL");
                price.setText("GHs 4.2");
                leve.setText("M");
                dialog12.show();
                break;


            case "RISPERLI1":
                cod.setText("RISPERLI1");
                nam.setText("Risperidone Liquid, 1 mg/mL");
                uni.setText("10mL");
                price.setText("GHs 49.7");
                leve.setText("SD");
                dialog12.show();
                break;


            case "RISPERTA1":
                cod.setText("RISPERTA1");
                nam.setText("Risperidone Tablet, 500 microgram");
                uni.setText("Tablet");
                price.setText("GHs 1.25");
                leve.setText("SD");
                dialog12.show();
                break;


            case "RISPERTA2":
                cod.setText("RISPERTA2");
                nam.setText("Risperidone Tablet, 1 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.2");
                leve.setText("SD");
                dialog12.show();
                break;



            case "RISPERTA3":
                cod.setText("RISPERTA3");
                nam.setText("Risperidone Tablet, 2 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.6");
                leve.setText("SD");
                dialog12.show();
                break;
        }




//S
        switch(selected){
            case "SALBUTGA1":
                cod.setText("SALBUTGA1");
                nam.setText("Salbutamol Inhaler, 100 microgram/metered dose, 200 doses");
                uni.setText("Inhaler");
                price.setText("GHs 18");
                leve.setText("B1");
                dialog12.show();
                break;


            case "SALBUTGA2":
                cod.setText("SALBUTGA2");
                nam.setText("Salbutamol Nebules, 2.5 mg");
                uni.setText("Dose");
                price.setText("GHs 2");
                leve.setText("B1");
                dialog12.show();
                break;

            case "SALBUTGA3":
                cod.setText("SALBUTGA3");
                nam.setText("Salbutamol Nebules, 5 mg");
                uni.setText("Dose");
                price.setText("GHs 2.5");
                leve.setText("B1");
                dialog12.show();
                break;

            case "SALSULIN1":
                cod.setText("SALSULIN1");
                nam.setText("Salbutamol Sulphate Injection, 500 microgram/mL in 1mL");
                uni.setText("Ampoule");
                price.setText("GHs 6.5");
                leve.setText("B2");
                dialog12.show();
                break;

            case "SALBUTSY1":
                cod.setText("SALBUTSY1");
                nam.setText("Salbutamol Syrup, 2 mg/5 mL");
                uni.setText("200mL");
                price.setText("GHs 4.25");
                leve.setText("B1");
                dialog12.show();
                break;


            case "SALBUTTA1":
                cod.setText("SALBUTTA1");
                nam.setText("Salbutamol Tablet, 2 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("B1");
                dialog12.show();
                break;


            case "SALBUTTA2":
                cod.setText("SALBUTTA2");
                nam.setText("Salbutamol Tablet, 4 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("B1");
                dialog12.show();
                break;

            case "SALACIOI1":
                cod.setText("SALACIOI1");
                nam.setText("Salicylic Acid Ointment, 2%");
                uni.setText("40G");
                price.setText("GHs 3");
                leve.setText("B1");
                dialog12.show();
                break;


            case "SECNIDTA1":
                cod.setText("SECNIDTA1");
                nam.setText("Secnidazole Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.5");
                leve.setText("C");
                dialog12.show();
                break;



            case "SELSULSH1":
                cod.setText("SELSULSH1");
                nam.setText("Selenium Sulphide Shampoo, 2.5%");
                uni.setText("50mL");
                price.setText("GHs 13.1");
                leve.setText("C");
                dialog12.show();
                break;



            case "SERTRATA1":
                cod.setText("SERTRATA1");
                nam.setText("Sertraline Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.5");
                leve.setText("SD");
                dialog12.show();
                break;




            case "SERTRATA2":
                cod.setText("SERTRATA2");
                nam.setText("Sertraline Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.3");
                leve.setText("SD");
                dialog12.show();
                break;



            case "SILSULCR1":
                cod.setText("SILSULCR1");
                nam.setText("Silver Sulphadiazine Cream, 1%");
                uni.setText("50G");
                price.setText("GHs 6.5");
                leve.setText("C");
                dialog12.show();
                break;
            case "SIMLINSY1":
                cod.setText("SIMLINSY1");
                nam.setText("Simple Linctus  BPC (Paediatric)");
                uni.setText("125mL");
                price.setText("GHs 3");
                leve.setText("A");
                dialog12.show();
                break;

            case "SIMLINSY2":
                cod.setText("SIMLINSY2");
                nam.setText("Simple Linctus  BPC");
                uni.setText("200mL");
                price.setText("GHs 5.9");
                leve.setText("A");
                dialog12.show();
                break;


            case "SIMVASTA1":
                cod.setText("SIMVASTA1");
                nam.setText("Simvastatin Tablet, 10 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.4");
                leve.setText("C");
                dialog12.show();
                break;


            case "SIMVASTA2":
                cod.setText("SIMVASTA2");
                nam.setText("Simvastatin Tablet, 20 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.5");
                leve.setText("C");
                dialog12.show();
                break;

            case "SIMVASTA3":
                cod.setText("SIMVASTA3");
                nam.setText("Simvastatin Tablet, 40 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.9");
                leve.setText("D");
                dialog12.show();
                break;


            case "SIMVASTA4":
                cod.setText("SIMVASTA4");
                nam.setText("Simvastatin Tablet, 80 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.32");
                leve.setText("D");
                dialog12.show();
                break;


            case "SODBICIN1":
                cod.setText("SODBICIN1");
                nam.setText("Sodium Bicarbonate Injection, 8.4% in 10 mL");
                uni.setText("Ampoule");
                price.setText("GHs 3.7");
                leve.setText("C");
                dialog12.show();
                break;


            case "SODCHLIN1":
                cod.setText("SODCHLIN1");
                nam.setText("Sodium Chloride Infusion, 0.45% (250 mL)");
                uni.setText("250mL");
                price.setText("GHs 3.81");
                leve.setText("B2");
                dialog12.show();
                break;


            case "SODCHLIN3":
                cod.setText("SODCHLIN3");
                nam.setText("Sodium Chloride Infusion, 0.9% (500 mL)");
                uni.setText("500mL");
                price.setText("GHs 4.69");
                leve.setText("B1");
                dialog12.show();
                break;

            case "SODCHLND1":
                cod.setText("SODCHLND1");
                nam.setText("Sodium Chloride Nasal Drops, 0.9%");
                uni.setText("10mL");
                price.setText("GHs 3");
                leve.setText("A");
                dialog12.show();
                break;
            case "SODVALCA1":
                cod.setText("SODVALCA1");
                nam.setText("Sodium Valproate Capsule, 200 mg");
                uni.setText("Capsule");
                price.setText("GHs 1.12");
                leve.setText("D");
                dialog12.show();
                break;


            case "SODVALCA2":
                cod.setText("SODVALCA2");
                nam.setText("Sodium Valproate Capsule(Slow Release) ");
                uni.setText("Capsule");
                price.setText("GHs 2.99");
                leve.setText("D");
                dialog12.show();
                break;



            case "SODVALSY1":
                cod.setText("SODVALSY1");
                nam.setText("Sodium Valproate Capsule(Slow Release) ");
                uni.setText("Capsule");
                price.setText("GHs 2.99");
                leve.setText("D");
                dialog12.show();
                break;




            case "SODVALTA1":
                cod.setText("SODVALTA1");
                nam.setText("Sodium Valproate Tablet, 200 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.12");
                leve.setText("D");
                dialog12.show();
                break;

            case "SOANSTOI1":
                cod.setText("SOANSTOI1");
                nam.setText("Soothing Agent + Local Anaesthetic + Steroid Ointment");
                uni.setText("T15G");
                price.setText("GHs 15");
                leve.setText("B2");
                dialog12.show();
                break;

            case "SOANSTRE1":
                cod.setText("SOANSTRE1");
                nam.setText("Soothing Agent + Local Anaesthetic + Steroid Suppository ");
                uni.setText("Supp");
                price.setText("GHs 2");
                leve.setText("B2");
                dialog12.show();
                break;



            case "SOOANAOI1":
                cod.setText("SOOANAOI1");
                nam.setText("Soothing Agent + Local Anaesthetic Ointment ");
                uni.setText("15G");
                price.setText("GHs 14");
                leve.setText("M");
                dialog12.show();
                break;

            case "SOOANARE1":
                cod.setText("SOOANARE1");
                nam.setText("Soothing Agent + Local Anaesthetic");
                uni.setText("Supp");
                price.setText("GHs 2.5");
                leve.setText("M");
                dialog12.show();
                break;



            case "SPIRONTA1":
                cod.setText("SPIRONTA1");
                nam.setText("Spironolactone Tablet, 25 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.51");
                leve.setText("C");
                dialog12.show();
                break;


            case "SPIRONTA2":
                cod.setText("SPIRONTA2");
                nam.setText("Spironolactone Tablet, 50 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.8");
                leve.setText("C");
                dialog12.show();
                break;




            case "STREPTIN1":
                cod.setText("STREPTIN1");
                nam.setText("Streptokinase Injection, 100,000 unit-vial ");
                uni.setText("Vial");
                price.setText("GHs 8.5");
                leve.setText("D");
                dialog12.show();
                break;


            case "STREPTIN2":
                cod.setText("STREPTIN2");
                nam.setText("Streptokinase Injection, 250,000 unit-vial");
                uni.setText("Vial");
                price.setText("GHs 110");
                leve.setText("D");
                dialog12.show();
                break;

            case "STREPTIN3":
                cod.setText("STREPTIN3");
                nam.setText("Streptokinase Injection, 750,000 unit-vial ");
                uni.setText("Vial");
                price.setText("GHs 180");
                leve.setText("D");
                dialog12.show();
                break;



            case "SULFASTA1":
                cod.setText("SULFASTA1");
                nam.setText("Sulfasalazine Tablet, 500 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.1");
                leve.setText("SD");
                dialog12.show();
                break;
        }

//T
        switch(selected){
            case "TAMOXITA1":
                cod.setText("TAMOXITA1");
                nam.setText("Tamoxifen Tablet, 10 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.5");
                leve.setText("SD");
                dialog12.show();
                break;


            case "TAMOXITA2":
                cod.setText("TAMOXITA2");
                nam.setText("Tamoxifen Tablet, 20 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.2");
                leve.setText("SD");
                dialog12.show();
                break;


            case "TAMSULCA1":
                cod.setText("TAMSULCA1");
                nam.setText("Tamsulosin Capsule, 400 microgram ");
                uni.setText("Capsule");
                price.setText("GHs 1");
                leve.setText("SD");
                dialog12.show();
                break;


            case "TERAZOTA1":
                cod.setText("TERAZOTA1");
                nam.setText("Terazosin Tablet, 2 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.95");
                leve.setText("SD");
                dialog12.show();
                break;

            case "TERAZOTA2":
                cod.setText("TERAZOTA2");
                nam.setText("Terazosin Tablet, 5 mg");
                uni.setText("Tablet");
                price.setText("GHs 1.43");
                leve.setText("SD");
                dialog12.show();
                break;


            case "TERBINTA1":
                cod.setText("TERBINTA1");
                nam.setText("Terbinafine HCl Tablet, 250 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.5");
                leve.setText("D");
                dialog12.show();
                break;



            case "TETRACCA1":
                cod.setText("TETRACCA1");
                nam.setText("Tetracycline Capsule, 250 mg ");
                uni.setText("Capsule");
                price.setText("GHs 0.07");
                leve.setText("SD");
                dialog12.show();
                break;


            case "TETRACEO1":
                cod.setText("TETRACEO1");
                nam.setText("Tetracycline Eye Ointment, 0.5% ");
                uni.setText("5G");
                price.setText("GHs 1.5");
                leve.setText("B1");
                dialog12.show();
                break;


            case "TETRACEO2":
                cod.setText("TETRACEO2");
                nam.setText("Tetracycline Eye Ointment, 1% ");
                uni.setText("5G");
                price.setText("GHs 1.9");
                leve.setText("B1");
                dialog12.show();
                break;

            case "THEOPHTA1":
                cod.setText("THEOPHTA1");
                nam.setText("Theophylline Tablet, 200 mg (slow release) ");
                uni.setText("Tablet");
                price.setText("GHs 1");
                leve.setText("C");
                dialog12.show();
                break;



            case "THIAMIIN1":
                cod.setText("THIAMIIN1");
                nam.setText("Thiamine Injection, 100 mg");
                uni.setText("Vial");
                price.setText("GHs 1.22");
                leve.setText("C");
                dialog12.show();
                break;


            case "THIAMITA1":
                cod.setText("THIAMITA1");
                nam.setText("Thiamine Tablet, 50 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.47");
                leve.setText("C");
                dialog12.show();
                break;


            case "THIAMITA2":
                cod.setText("THIAMITA2");
                nam.setText("Thiamine Tablet, 100 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.5");
                leve.setText("C");
                dialog12.show();
                break;


            case "TIABENTA1":
                cod.setText("TIABENTA1");
                nam.setText("Tiabendazole Tablet, 500 mg ");
                uni.setText("Tablet");
                price.setText("GHs 2.75");
                leve.setText("B2");
                dialog12.show();
                break;


            case "TIMMALID1":
                cod.setText("TIMMALID1");
                nam.setText("Timolol Maleate Eye Drops, 0.5%");
                uni.setText("10mL");
                price.setText("GHs 7.39");
                leve.setText("C");
                dialog12.show();
                break;

            case "TINIDACA1":
                cod.setText("TINIDACA1");
                nam.setText("Tinidazole Capsule, 500 mg ");
                uni.setText("Capsule");
                price.setText("GHs 4.4");
                leve.setText("B2");
                dialog12.show();
                break;


            case "TIROFIIN1":
                cod.setText("TIROFIIN1");
                nam.setText("Tirofiban Infusion, 50 micrograms/mL ");
                uni.setText("100mL");
                price.setText("GHs 270.01");
                leve.setText("D");
                dialog12.show();
                break;


            case "TIROFIIN2":
                cod.setText("TIROFIIN2");
                nam.setText("Tirofiban Infusion, 250 micrograms/mL (concentrate) ");
                uni.setText("100mL");
                price.setText("GHs 320");
                leve.setText("D");
                dialog12.show();
                break;


            case "TOLBUTTA1":
                cod.setText("TOLBUTTA1");
                nam.setText("Tolbutamide Tablet, 500 mg");
                uni.setText("Tablet");
                price.setText("GHs 0.6");
                leve.setText("B2");
                dialog12.show();
                break;

            case "TRAACICA1":
                cod.setText("TRAACICA1");
                nam.setText("Tranexamic Acid Capsule, 250 mg");
                uni.setText("Capsule");
                price.setText("GHs 1.6");
                leve.setText("C");
                dialog12.show();
                break;


            case "TRAACIIN1":
                cod.setText("TRAACIIN1");
                nam.setText("Tranexamic Acid Injection, 500 mg/5mL ");
                uni.setText("Ampoule");
                price.setText("GHs 8.5");
                leve.setText("C");
                dialog12.show();
                break;



            case "TRAACITA1":
                cod.setText("TRAACITA1");
                nam.setText("Tranexamic Acid Tablet, 500 mg ");
                uni.setText("Tablet");
                price.setText("GHs 2.06");
                leve.setText("C");
                dialog12.show();
                break;


            case "TRIHEXTA1":
                cod.setText("TRIHEXTA1");
                nam.setText("Trihexyphenidyl Tablet, 2 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.11");
                leve.setText("C");
                dialog12.show();
                break;


            case "TRIHEXTA2":
                cod.setText("TRIHEXTA2");
                nam.setText("Trihexyphenidyl Tablet, 5 mg ");
                uni.setText("Tablet");
                price.setText("GHs 1.2");
                leve.setText("C");
                dialog12.show();
                break;


        }



//V
        switch(selected){


            case "VERAPATA1":
                cod.setText("VERAPATA1");
                nam.setText("Verapamil Tablet, 40 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.72");
                leve.setText("D");
                dialog12.show();
                break;


            case "VERAPATA2":
                cod.setText("VERAPATA2");
                nam.setText("Verapamil Tablet, 80 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.99");
                leve.setText("D");
                dialog12.show();
                break;


        }


//W
        switch(selected){


            case "WARFARTA1":
                cod.setText("WARFARTA1");
                nam.setText("Warfarin Tablet, 1 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.2");
                leve.setText("D");
                dialog12.show();
                break;


            case "WARFARTA2":
                cod.setText("WARFARTA2");
                nam.setText("Warfarin Tablet, 3 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.25");
                leve.setText("D");
                dialog12.show();
                break;

            case "WARFARTA3":
                cod.setText("WARFARTA3");
                nam.setText("Warfarin Tablet, 5 mg (scored) ");
                uni.setText("Tablet");
                price.setText("GHs 0.37");
                leve.setText("D");
                dialog12.show();
                break;



            case "WATFORIN1":
                cod.setText("WATFORIN1");
                nam.setText("Water for Injection ");
                uni.setText("10mL");
                price.setText("GHs 0.2");
                leve.setText("A");
                dialog12.show();
                break;
        }



//V
        switch(selected){


            case "ZINCOOTA1":
                cod.setText("ZINCOOTA1");
                nam.setText("Zinc Tablet, 10 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.05");
                leve.setText("A");
                dialog12.show();
                break;


            case "ZINCOOTA2":
                cod.setText("ZINCOOTA2");
                nam.setText("Zinc Tablet, 20 mg ");
                uni.setText("Tablet");
                price.setText("GHs 0.08");
                leve.setText("A");
                dialog12.show();
                break;


//            case "5FLUORIN1":
//                cod.setText("5FLUORIN1");
//                nam.setText("5-Fluorouracil Injection, 50 mg/mL ");
//                uni.setText("10mL");
//                price.setText("GHs 9");
//                leve.setText("D");


        }


        //}
        return true;
    }

    @Override
    public boolean onClose() {
        listAdapter.fitlerData("");
        //expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        listAdapter.fitlerData(query);
        //expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.fitlerData(newText);
        //expandAll();
        return false;
    }
}
