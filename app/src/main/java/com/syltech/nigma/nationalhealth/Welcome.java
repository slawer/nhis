package com.syltech.nigma.nationalhealth;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Welcome extends AppCompatActivity implements View.OnClickListener{

    private TextView txt,txtGet,txtBene,txtMed,txtLoc,txtFaq,txtAbout;
    private ImageButton imgGet,imgBen,imgFaq,imgMed,imgLoc,imgAbout,imgInfo;
    Typeface tfc1,tfcGet,tfcBene,tfcMed,tfcLoc,tfcFaq,tfcAbout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        txt=(TextView) findViewById(R.id.textView2);
        txtGet=(TextView) findViewById(R.id.textViewGet);
        txtBene=(TextView) findViewById(R.id.textViewBene);
        txtMed=(TextView) findViewById(R.id.textViewMed);
        txtLoc=(TextView) findViewById(R.id.textViewLoc);
        txtFaq=(TextView) findViewById(R.id.textViewFaq);
        txtAbout=(TextView) findViewById(R.id.textViewAbout);
        TextView lblList=(TextView)findViewById(R.id.lblHeader);

        imgGet=(ImageButton) findViewById(R.id.imageButtonGet);
        imgBen=(ImageButton) findViewById(R.id.imageButtonBene);
        imgFaq=(ImageButton) findViewById(R.id.imageButtonFaqs);
        imgMed=(ImageButton) findViewById(R.id.imageButtonMed);
        imgLoc=(ImageButton) findViewById(R.id.imageButtonLoc);
        imgAbout=(ImageButton) findViewById(R.id.imageButtonAbout);
        imgInfo=(ImageButton) findViewById(R.id.imageButtonInfo);


        imgInfo.setOnClickListener(this);
        imgGet.setOnClickListener(this);
        imgBen.setOnClickListener(this);
        imgFaq.setOnClickListener(this);
        imgMed.setOnClickListener(this);
        imgLoc.setOnClickListener(this);
        imgAbout.setOnClickListener(this);

        tfc1= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcGet= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcBene= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcMed= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcLoc= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcFaq= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        tfcAbout= Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");

        txt.setTypeface(tfc1);
        txtGet.setTypeface(tfcGet);
        txtBene.setTypeface(tfcBene);
        txtMed.setTypeface(tfcMed);
        txtLoc.setTypeface(tfcLoc);
        txtFaq.setTypeface(tfcFaq);
        txtAbout.setTypeface(tfcAbout);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.imageButtonGet:
                startActivity(new Intent(this,GettingStarted.class));
                break;
            case R.id.imageButtonBene:
                startActivity(new Intent(this,Benefits.class));
                break;
            case R.id.imageButtonFaqs:
                startActivity(new Intent(this,FAQs.class));
                break;
            case R.id.imageButtonMed:
                startActivity(new Intent(this,Medicines.class));
                break;
            case R.id.imageButtonLoc:
                startActivity(new Intent(this,Offices.class));
                break;
            case R.id.imageButtonAbout:
                startActivity(new Intent(this,About.class));
                break;

            case R.id.imageButtonInfo:
                startActivity(new Intent(this,Information.class));
                /*AlertDialog.Builder mBuilder=new AlertDialog.Builder(Welcome.this);
                View mView=getLayoutInflater().inflate(R.layout.information,null);
                mBuilder.setView(mView);
                mBuilder.setCancelable(true);
                mBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog=mBuilder.create();
                dialog.show();*/
                break;
        }
    }
}
