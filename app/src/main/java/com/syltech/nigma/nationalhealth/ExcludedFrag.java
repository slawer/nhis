package com.syltech.nigma.nationalhealth;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class ExcludedFrag extends Fragment implements View.OnClickListener{

    View myFragment;
    private ImageButton btnBack;

    public static ExcludedFrag newInstance() {
        ExcludedFrag fragment = new ExcludedFrag();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragment=inflater.inflate(R.layout.fragment_excluded, container, false);


        btnBack=(ImageButton) myFragment.findViewById(R.id.imageButtonGetBack);

        btnBack.setOnClickListener(this);

        return myFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                getActivity().finish();
                break;
        }
    }
}
