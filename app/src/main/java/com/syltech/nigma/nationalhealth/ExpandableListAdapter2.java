package com.syltech.nigma.nationalhealth;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nigma on 22/06/2017.
 */

public class ExpandableListAdapter2 extends BaseExpandableListAdapter{
    private Context context;
    private ArrayList<Region> locationList;
    private ArrayList<Region> originalList;

    public ExpandableListAdapter2(Context context, ArrayList<Region> locationList) {
        this.context = context;
        this.locationList = new ArrayList<Region>();//locationList;
        this.locationList.addAll(locationList);

        this.originalList=new ArrayList<Region>();
        this.originalList.addAll(locationList);
    }



    @Override
    public int getGroupCount() {
        return locationList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Location> tabList=locationList.get(groupPosition).getLoc();
        return tabList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return locationList.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return locationList.get(groupPosition).getLoc().get(childPosition).getLblListItem();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Typeface tfcFaqs;

        //String headerTitle=(String)getGroup(groupPosition);
        Region headerTitle=(Region) getGroup(groupPosition);
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_group_4,null);
        }


        TextView lblListHeader=(TextView)convertView.findViewById(R.id.lblHeader);

        tfcFaqs= Typeface.createFromAsset(context.getAssets(),"fonts/ARLRDBD.ttf");

        lblListHeader.setTypeface(tfcFaqs);
        lblListHeader.setText(headerTitle.getName().trim());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText=(String)getChild(groupPosition,childPosition);

        //Tablets childText = (Tablets) getChild(groupPosition,childPosition);
        Typeface tfcFaqs;
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_item_3,null);
        }
        TextView txtListChild=(TextView)convertView.findViewById(R.id.lblListItem);

        tfcFaqs= Typeface.createFromAsset(context.getAssets(),"fonts/HelveticaNeue-Thin.ttf");

        txtListChild.setTypeface(tfcFaqs);
        txtListChild.setText(childText);//.getLblListItem().trim());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void fitlerData(String query){
        query=query.toLowerCase();
        Log.v("ExpandableListAdapter2",String.valueOf(locationList.size()));
        locationList.clear();

        if(query.isEmpty()){
            locationList.addAll(originalList);
        }
        else{
            for (Region region:originalList){
                ArrayList<Location> pillsList=region.getLoc();
                ArrayList<Location> newList=new ArrayList<Location>();

                for(Location location: pillsList){
                    if (location.getLblListItem().toLowerCase().contains(query)){
                        newList.add(location);
                    }
                }
                if(newList.size()>0)
                {
                    Region mRegion=new Region(region.getName(),newList);;
                    locationList.add(mRegion);
                }
            }
        }
        Log.v("ExpandableListAdapter2",String.valueOf(locationList.size()));
        notifyDataSetChanged();
    }
}
