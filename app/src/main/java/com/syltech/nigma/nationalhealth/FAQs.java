package com.syltech.nigma.nationalhealth;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FAQs extends AppCompatActivity implements View.OnClickListener {

    private ExpandableListView list;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataAdapter;
    private HashMap<String,List<String>> listHash;
    private ImageButton btnBack;
     //Typeface tfcFaq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);

        initData();

        btnBack=(ImageButton) findViewById(R.id.imageButtonGetBack);

        btnBack.setOnClickListener(this);

        list=(ExpandableListView) findViewById(R.id.list);
        listAdapter=new ExpandableListAdapter(this,listDataAdapter,listHash);
        list.setAdapter(listAdapter);



    }

    private void initData() {
        listDataAdapter=new ArrayList<>();
        listHash=new HashMap<>();

        listDataAdapter.add("Membership Registration");
        listDataAdapter.add("Where can one register?");
        listDataAdapter.add("Premiums");
        listDataAdapter.add("Complaints");
        listDataAdapter.add("I have a new born baby. Do I have to register him before he can be treated under the NHIS?");
        listDataAdapter.add("How long does it take to renew my membership?");
        listDataAdapter.add("Can I renew my membership at a district office other than where I registered?");
        listDataAdapter.add("Can I renew my membership online?");
        listDataAdapter.add("If I have to undergo a surgical operation do I have to pay any fees to the hospital?");
        listDataAdapter.add("If I use the amenity ward (VIP ward) will the NHIS pay?");
        listDataAdapter.add("How much do I pay to get my child under 18 years of age registered?");
        listDataAdapter.add("How much do I pay to get my 70-year-old father registered?");


        List<String>reg=new ArrayList<>();
        reg.add("Every person resident in Ghana other than Armed Forces of Ghana and the Ghana Police Service shall belong to a health insurance scheme licensed under this Act.");

        List<String>reg1=new ArrayList<>();
        reg1.add("District NHIS office.");


        List<String>reg2=new ArrayList<>();
        reg2.add("Premium is set from GH¢7.2 (minimum) to GH¢48.00 (maximum). Payment could be made at NHIS district office.");

        List<String>reg3=new ArrayList<>();
        reg3.add("NHIS members can lodge complaints with the nearest NHIS office or NHIA in Accra.\n" +
                "NHIS Call Center Number: 054 444 6447, short code 6447 (MTN and Vodafone).\n");

        List<String>reg4=new ArrayList<>();
        reg4.add("No. Your child can be treated using your NHIS card until he/she is three months old. However, your child should be registered by the time he/she is three months old.");

        List<String>reg5=new ArrayList<>();
        reg5.add("Renewal of membership is instant if you are not a defaulting member. Defaulting members serve a one (1) month waiting period before they can access health care on the NHIS.");

        List<String>reg6=new ArrayList<>();
        reg6.add("Yes, you can");

        List<String>reg7=new ArrayList<>();
        reg7.add("Not yet. Subscribers will be notified when online renewal is available.");

        List<String>reg8=new ArrayList<>();
        reg8.add("Most surgeries are covered under the NHIS. Where you are not sure, please contact your district office or the NHIS Call Centre. 054 444 6447, short code 6447 (MTN and Vodafone).");

        List<String>reg9=new ArrayList<>();
        reg9.add("VIP ward accommodation is in the Exclusions List.");

        List<String>reg10=new ArrayList<>();
        reg10.add("Children under 18 years belong to the exempt category. Therefore, you only pay processing fee.");

        List<String>reg11=new ArrayList<>();
        reg11.add("Elderly people 70 years and above belong to the exempt category. Therefore, you only pay processing fee.");


        listHash.put(listDataAdapter.get(0),reg);
        listHash.put(listDataAdapter.get(1),reg1);
        listHash.put(listDataAdapter.get(2),reg2);
        listHash.put(listDataAdapter.get(3),reg3);
        listHash.put(listDataAdapter.get(4),reg4);
        listHash.put(listDataAdapter.get(5),reg5);
        listHash.put(listDataAdapter.get(6),reg6);
        listHash.put(listDataAdapter.get(7),reg7);
        listHash.put(listDataAdapter.get(8),reg8);
        listHash.put(listDataAdapter.get(9),reg9);
        listHash.put(listDataAdapter.get(10),reg10);
        listHash.put(listDataAdapter.get(11),reg11);
     //   listHash.put(listDataAdapter.get(12),reg12);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
                //startActivity(new Intent(FAQs.this,Welcome.class));
                break;
        }
    }
}
