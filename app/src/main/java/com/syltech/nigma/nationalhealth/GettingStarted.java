package com.syltech.nigma.nationalhealth;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;



public class GettingStarted extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener{

    private ImageButton btnBack;
    private List<RowItem> nhis=new ArrayList<RowItem>();
    private TextView txt;
    Typeface tfc1;

//    String[] players={"Registering as a member","Accessing healthcare with NHIS card","Rights and responsibilities","Renewals","Replacements","Offences"};
//    int[] images={R.color.colorRed,R.color.colorYellow,R.color.colorBrown,R.color.colorPurple,R.color.colorLightBlue,R.color.colorDeepBlue};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getting_started);


        btnBack= (ImageButton) findViewById(R.id.imageButtonGetBack);
        btnBack.setOnClickListener(this);

        nhis.add(new RowItem("Registering as a member", R.color.colorRed));
        nhis.add(new RowItem("Accessing healthcare with NHIS card", R.color.colorYellow));
        nhis.add(new RowItem("Rights and responsibilities", R.color.colorBrown));
        nhis.add(new RowItem("Renewals", R.color.colorPurple));
        nhis.add(new RowItem("Replacements", R.color.colorLightBlue));
        nhis.add(new RowItem("Offenses", R.color.colorDeepBlue));

        txt=(TextView) findViewById(R.id.textView3);
        tfc1= Typeface.createFromAsset(getAssets(),"fonts/ARLRDBD.ttf");
        txt.setTypeface(tfc1);

        ArrayAdapter<RowItem> adapter = new customAdapter();

        ListView myList=(ListView) findViewById(R.id.list);
        myList.setAdapter(adapter);

        myList.setOnItemClickListener(this);
        btnBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
                //startActivity(new Intent(this,Welcome.class));
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                startActivity(new Intent(GettingStarted.this, RegisteringInfo.class));
                break;
            case 1:
                startActivity(new Intent(GettingStarted.this, Accessing.class));
                break;
            case 2:
                startActivity(new Intent(GettingStarted.this, Rights.class));
                break;
            case 3:
                startActivity(new Intent(GettingStarted.this, Renewal.class));
                break;
            case 4:
                startActivity(new Intent(GettingStarted.this, Replacement.class));
                break;
            case 5:
                startActivity(new Intent(GettingStarted.this, Offence.class));
                break;

            //Toast.makeText(GettingStarted.this, "My listview "+position, Toast.LENGTH_SHORT).show();
        }

    }

    private class customAdapter extends ArrayAdapter<RowItem>{

        public customAdapter(){
            super(GettingStarted.this,R.layout.list_item,nhis);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if(convertView==null){
                convertView= getLayoutInflater().inflate(R.layout.list_item,parent,false);
            }

            RowItem item = nhis.get(position);

            ImageView myImg= (ImageView) convertView.findViewById(R.id.profile_pic);
            TextView myHeading= (TextView) convertView.findViewById(R.id.member_name);

            myImg.setImageResource(item.getProfile_pic_id());
            myHeading.setText(item.getMember_name());

            return convertView;
        }
    }

}

