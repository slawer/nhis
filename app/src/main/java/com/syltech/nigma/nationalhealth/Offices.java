package com.syltech.nigma.nationalhealth;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

public class Offices extends AppCompatActivity implements ExpandableListView.OnChildClickListener, View.OnClickListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener {
    private ExpandableListView list;
    private ExpandableListAdapter2 listAdapter;
    private ArrayList<Region> pillsList=new ArrayList<Region>();
    private TextView district,loca,address,number;
    private ImageButton btnBack;
    private SearchView search;
    private SearchManager searchManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offices);

        searchManager=(SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search=(SearchView) findViewById(R.id.search1);
        search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setOnCloseListener(this);

        initData();

        list=(ExpandableListView) findViewById(R.id.liste);
        listAdapter=new ExpandableListAdapter2(Offices.this,pillsList);
        list.setAdapter(listAdapter);
        list.setOnChildClickListener(this);

        btnBack=(ImageButton) findViewById(R.id.imageButtonGetBack);

        btnBack.setOnClickListener(this);


    }

    private void initData() {
        ArrayList<Location> childList= new ArrayList<Location>();
        Location tablets=new Location("Ashanti Regional Office");
        childList.add(tablets);
        tablets=new Location("Adansi North");
        childList.add(tablets);
        tablets=new Location("Adansi South");
        childList.add(tablets);
        tablets=new Location("Afigya Sekyere");
        childList.add(tablets);
        tablets=new Location("Ahafo Ano North");
        childList.add(tablets);
        tablets=new Location("Ahafo Ano South");
        childList.add(tablets);
        tablets=new Location("Amansie Central");
        childList.add(tablets);
        tablets=new Location("Amansie East");
        childList.add(tablets);
        tablets=new Location("Amansie West");
        childList.add(tablets);
        tablets=new Location("Asante Akim North");
        childList.add(tablets);
        tablets=new Location("Asante Akim South");
        childList.add(tablets);
        tablets=new Location("Asokwa Sub-Metro");
        childList.add(tablets);
        tablets=new Location("Atwima Mponua");
        childList.add(tablets);
        tablets=new Location("B.A.K.");
        childList.add(tablets);
        tablets=new Location("Bantama");
        childList.add(tablets);
        tablets=new Location("Ejisu");
        childList.add(tablets);
        tablets=new Location("Ejura");
        childList.add(tablets);
        tablets=new Location("Kwabre");
        childList.add(tablets);
        tablets=new Location("Manhyia");
        childList.add(tablets);
        tablets=new Location("Obuasi");
        childList.add(tablets);
        tablets=new Location("Offinsoman");
        childList.add(tablets);
        tablets=new Location("Sekyere East");
        childList.add(tablets);
        tablets=new Location("Sekyere West");
        childList.add(tablets);
        tablets=new Location("Subin");
        childList.add(tablets);

        Region groupList=new Region("Ashanti Region",childList);
        pillsList.add(groupList);

        //Group B
        childList=new ArrayList<Location>();
        tablets=new Location("Brong Ahafo Regional Office");
        childList.add(tablets);
        tablets=new Location("Sene");
        childList.add(tablets);
        tablets=new Location("Wenchi");
        childList.add(tablets);
        tablets=new Location("Berekum");
        childList.add(tablets);
        tablets=new Location("Sunyani");
        childList.add(tablets);
        tablets=new Location("Atebubu");
        childList.add(tablets);
        tablets=new Location("Kintampo South");
        childList.add(tablets);
        tablets=new Location("Asunafo North");
        childList.add(tablets);
        tablets=new Location("Tano North");
        childList.add(tablets);
        tablets=new Location("Kintampo");
        childList.add(tablets);
        tablets=new Location("Jaman South");
        childList.add(tablets);
        tablets=new Location("Nkoranza");
        childList.add(tablets);
        tablets=new Location("Techiman");
        childList.add(tablets);
        tablets=new Location("Asunafo South");
        childList.add(tablets);
        tablets=new Location("Tain");
        childList.add(tablets);
        tablets=new Location("Dormaa Ahenkro");
        childList.add(tablets);
        tablets=new Location("Jaman North");
        childList.add(tablets);
        tablets=new Location("PRU");
        childList.add(tablets);
        tablets=new Location("Asutifi");
        childList.add(tablets);

        groupList=new Region("Brong Ahafo Region",childList);
        pillsList.add(groupList);


        //Group C
        childList=new ArrayList<Location>();
        tablets=new Location("Central Regional Office");
        childList.add(tablets);
        tablets=new Location("Abura-Asebu-Kwamankese");
        childList.add(tablets);
        tablets=new Location("Ajumako-Enyan-Essiam");
        childList.add(tablets);
        tablets=new Location("Awutu-Efutu-Senya");
        childList.add(tablets);
        tablets=new Location("Agona");
        childList.add(tablets);
        tablets=new Location("Assin North");
        childList.add(tablets);
        tablets=new Location("Assin South");
        childList.add(tablets);
        tablets=new Location("Asikuma-Odoben-Brakwa");
        childList.add(tablets);
        tablets=new Location("Cape Coast Muni");
        childList.add(tablets);
        tablets=new Location("Gomoa");
        childList.add(tablets);
        tablets=new Location("Komenda-Eguafo-Edina-Abirem");
        childList.add(tablets);
        tablets=new Location("Mfantseman");
        childList.add(tablets);
        tablets=new Location("Lower Denkyira");
        childList.add(tablets);
        tablets=new Location("Upper Denkyira");
        childList.add(tablets);

        groupList=new Region("Central Region",childList);
        pillsList.add(groupList);


        //Group D
        childList=new ArrayList<Location>();
        tablets=new Location("Eastern Regional Office");
        childList.add(tablets);
        tablets=new Location("Afam Plains");
        childList.add(tablets);
        tablets=new Location("Akuapem North");
        childList.add(tablets);
        tablets=new Location("Akuapem South");
        childList.add(tablets);
        tablets=new Location("Asuogyaman");
        childList.add(tablets);
        tablets=new Location("Atiwa");
        childList.add(tablets);
        tablets=new Location("Birim North");
        childList.add(tablets);
        tablets=new Location("Birim South");
        childList.add(tablets);
        tablets=new Location("East Akim");
        childList.add(tablets);
        tablets=new Location("Fanteakwa");
        childList.add(tablets);
        tablets=new Location("Kwaebibirim");
        childList.add(tablets);
        tablets=new Location("Kwahu South");
        childList.add(tablets);
        tablets=new Location("Kwahu West");
        childList.add(tablets);
        tablets=new Location("Manya Krobo");
        childList.add(tablets);
        tablets=new Location("New Juaben");
        childList.add(tablets);
        tablets=new Location("Suhum");
        childList.add(tablets);
        tablets=new Location("West Akim");
        childList.add(tablets);
        tablets=new Location("Yilo Krobo");
        childList.add(tablets);

        groupList=new Region("Eastern Region",childList);
        pillsList.add(groupList);


        //Group E
        childList=new ArrayList<Location>();
        tablets=new Location("Greater Accra Regional Office");
        childList.add(tablets);
        tablets=new Location("Ablekuma");
        childList.add(tablets);
        tablets=new Location("Ashiedu Keteke");
        childList.add(tablets);
        tablets=new Location("Ayawaso");
        childList.add(tablets);
        tablets=new Location("Dangme East");
        childList.add(tablets);
        tablets=new Location("Dangme West");
        childList.add(tablets);
        tablets=new Location("GA District");
        childList.add(tablets);
        tablets=new Location("Kpeshie");
        childList.add(tablets);
        tablets=new Location("Okaikoi");
        childList.add(tablets);
        tablets=new Location("Osu Klottey");
        childList.add(tablets);
        tablets=new Location("Tema");
        childList.add(tablets);

        groupList=new Region("Greater Accra",childList);
        pillsList.add(groupList);


        //Group F
        childList=new ArrayList<Location>();
        tablets=new Location("Northern Regional Office");
        childList.add(tablets);
        tablets=new Location("Tamale");
        childList.add(tablets);
        tablets=new Location("Tolon/Kumbungu");
        childList.add(tablets);
        tablets=new Location("Savelugu");
        childList.add(tablets);
        tablets=new Location("Nanumba North");
        childList.add(tablets);
        tablets=new Location("Zabzugu/Tatale");
        childList.add(tablets);
        tablets=new Location("Saboba/Chereponi");
        childList.add(tablets);
        tablets=new Location("Karaga");
        childList.add(tablets);
        tablets=new Location("East Mamprusi");
        childList.add(tablets);
        tablets=new Location("Bunkpurugu/Yunyoo");
        childList.add(tablets);
        tablets=new Location("Nanumba South");
        childList.add(tablets);
        tablets=new Location("Gushegu");
        childList.add(tablets);
        tablets=new Location("Yendi");
        childList.add(tablets);
        tablets=new Location("West Gonja");
        childList.add(tablets);
        tablets=new Location("West Mamprusi");
        childList.add(tablets);
        tablets=new Location("Sawla/Tuna/Kalba");
        childList.add(tablets);
        tablets=new Location("East Gonja");
        childList.add(tablets);
        tablets=new Location("Central Gonja");
        childList.add(tablets);

        groupList=new Region("Northern Region",childList);
        pillsList.add(groupList);


        //Group G
        childList=new ArrayList<Location>();
        tablets=new Location("Upper East Regional Office");
        childList.add(tablets);
        tablets=new Location("Bolgatanga");
        childList.add(tablets);
        tablets=new Location("Bawku");
        childList.add(tablets);
        tablets=new Location("Bawku West");
        childList.add(tablets);
        tablets=new Location("Bongo");
        childList.add(tablets);
        tablets=new Location("Builsa");
        childList.add(tablets);
        tablets=new Location("Kassena/Nankana");
        childList.add(tablets);

        groupList=new Region("Upper East Region",childList);
        pillsList.add(groupList);


        //Group H
        childList=new ArrayList<Location>();
        tablets=new Location("Upper West Regional Office");
        childList.add(tablets);
        tablets=new Location("Jirapa / Lambussie");
        childList.add(tablets);
        tablets=new Location("Lawra");
        childList.add(tablets);
        tablets=new Location("Nadowli");
        childList.add(tablets);
        tablets=new Location("Sissala East");
        childList.add(tablets);
        tablets=new Location("Sissala West");
        childList.add(tablets);
        tablets=new Location("Wa East");
        childList.add(tablets);
        tablets=new Location("Wa Municipal");
        childList.add(tablets);
        tablets=new Location("Wa West");
        childList.add(tablets);

        groupList=new Region("Upper West Region",childList);
        pillsList.add(groupList);


        //Group I
        childList=new ArrayList<Location>();
        tablets=new Location("Volta Regional Office");
        childList.add(tablets);
        tablets=new Location("Kpando");
        childList.add(tablets);
        tablets=new Location("Jasikan");
        childList.add(tablets);
        tablets=new Location("Keta");
        childList.add(tablets);
        tablets=new Location("Adaklu Anyigbe");
        childList.add(tablets);
        tablets=new Location("South Dayi");
        childList.add(tablets);
        tablets=new Location("South Tongu");
        childList.add(tablets);
        tablets=new Location("Ho");
        childList.add(tablets);
        tablets=new Location("Hohoe");
        childList.add(tablets);
        tablets=new Location("Nkwanta");
        childList.add(tablets);
        tablets=new Location("Ketu");
        childList.add(tablets);
        tablets=new Location("Krachi East");
        childList.add(tablets);
        tablets=new Location("Krachi West");
        childList.add(tablets);
        tablets=new Location("Akatsi");
        childList.add(tablets);
        tablets=new Location("North Tongu");
        childList.add(tablets);
        tablets=new Location("Kadjebi");
        childList.add(tablets);

        groupList=new Region("Volta Region",childList);
        pillsList.add(groupList);


        //Group K
        childList=new ArrayList<Location>();
        tablets=new Location("Western Regional Office");
        childList.add(tablets);
        tablets=new Location("Sekondi");
        childList.add(tablets);
        tablets=new Location("Takoradi");
        childList.add(tablets);
        tablets=new Location("Ahantaman");
        childList.add(tablets);
        tablets=new Location("Wassa West");
        childList.add(tablets);
        tablets=new Location("Mpohor Wassa East");
        childList.add(tablets);
        tablets=new Location("Nzema East");
        childList.add(tablets);
        tablets=new Location("Amenfi East");
        childList.add(tablets);
        tablets=new Location("Amenfiman");
        childList.add(tablets);
        tablets=new Location("Jomoro");
        childList.add(tablets);
        tablets=new Location("Aowin Suaman");
        childList.add(tablets);
        tablets=new Location("Bia");
        childList.add(tablets);
        tablets=new Location("Sefwi-Wiawso");
        childList.add(tablets);
        tablets=new Location("Juabeso");
        childList.add(tablets);
        tablets=new Location("Bibiani-Anhwiaso");
        childList.add(tablets);

        groupList=new Region("Western Region",childList);
        pillsList.add(groupList);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        AlertDialog.Builder mBuilder=new AlertDialog.Builder(Offices.this);
        View mView=getLayoutInflater().inflate(R.layout.popper,null);
        mBuilder.setView(mView);
        mBuilder.setCancelable(true);
        mBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = mBuilder.create();


        district=(TextView) mView.findViewById(R.id.txtDistrict);
        loca=(TextView) mView.findViewById(R.id.txtLocation);
        address=(TextView) mView.findViewById(R.id.txtAddress);
        number=(TextView) mView.findViewById(R.id.txtNumber);


        String selected = listAdapter.getChild(groupPosition,childPosition);

        switch (selected) {
            case "Ashanti Regional Office":
                district.setText("Ashanti Regional Office");
                loca.setText("Opposite OPD, Komfo Anokye Teaching Hospital, Share same building with Nimo Pharmacy, Bantama");
                address.setText("P. O. Box KS 15075, Kumasi");
                number.setText("O3220 27585");
                dialog.show();
                break;

            case "Adansi North":
                district.setText("Adansi North");
                loca.setText("Adjacent To District Fire Service Office");
                address.setText("P. O. Box 214, Fomena-Adansi");
                number.setText("0322091299");
                dialog.show();
                break;

            case "Adansi South":
                district.setText("Adansi South");
                loca.setText("Within the District Assembly Premises");
                address.setText("P. O. Box 1, New Edubiase");
                number.setText("0208030192");
                dialog.show();
                break;

            case "Afigya Sekyere":
                district.setText("Afigya Sekyere");
                loca.setText("Old Assembly Block, Adjacent: District Police Headquarters");
                address.setText("P. O. Box 92, Agona-Ash");
                number.setText("0322094075, 0289527384");
                dialog.show();
                break;

            case "Ahafo Ano North":
                district.setText("Ahafo Ano North");
                loca.setText("Behind Ghana National Fire Service");
                address.setText("P. O. Box 39, Tepa");
                number.setText("0322047141, 0322047011");
                dialog.show();
                break;

            case "Ahafo Ano South":
                district.setText("Ahafo Ano South");
                loca.setText("Adjacent to District Assembly Hall");
                address.setText("P. O. Box 9, Mankranso");
                number.setText("0289116032");
                dialog.show();
                break;

            case "Amansie Central":
                district.setText("Amansie Central");
                loca.setText("Opposite The Church of Pentecost");
                address.setText("P. O. Box 7, Jacobu");
                number.setText("#");
                dialog.show();
                break;

            case "Amansie East":
                district.setText("Amansie East");
                loca.setText("Behind the Bekwai Municipal Assembly Block");
                address.setText("P. O. Box 350, Bekwai-Ashanti");
                number.setText("0322420191, 0289116033");
                dialog.show();
                break;
            case "Amansie West":
                district.setText("Amansie West");
                loca.setText("Manso Nkantia");
                address.setText("P. O. Box 1, Manso Nkantia");
                number.setText("0322096668, 0322096669");
                dialog.show();
                break;

            case "Asante Akim North":
                district.setText("Asante Akim North");
                loca.setText("Ahenbronum near Konongo Old Palace, in the same building with Merchant Bank Ghana Ltd");
                address.setText("P. O. Box 214, Konongo");
                number.setText("0322124006");
                dialog.show();
                break;

            case "Asante Akim South":
                district.setText("Asante Akim South");
                loca.setText("Within the District Assembly Premises");
                address.setText("P. O. Box 12, Juaso");
                number.setText("0322095158");
                dialog.show();
                break;

            case "Asokwa Sub-Metro":
                district.setText("Asokwa Sub-Metro");
                loca.setText("Top Martins Complex, Plot Number 4A, Block C Asokwa, Near DSTV’s office");
                address.setText("P. O. Box KS 14907, Kumasi");
                number.setText("0322080961, 0289116036");
                dialog.show();
                break;

            case "Atwima Mponua":
                district.setText("Atwima Mponua");
                loca.setText("In Atwima Mponua District Assembly Block");
                address.setText("P. O. Box N88, Nyinahin – Ashanti");
                number.setText("0322092346");
                dialog.show();
                break;

            case "Atwima Nwabiagya":
                district.setText("Atwima Nwabiagya");
                loca.setText("Adjacent the District Police Station (Nkawie)");
                address.setText("PLB 68, Nkawie - Toase");
                number.setText("0332091540, 0332094693");
                dialog.show();
                break;

            case "B.A.K.":
                district.setText("B.A.K.");
                loca.setText("Toamfom off Bekwai Road");
                address.setText("P. O. Box 24, Kuntenase");
                number.setText("0289112511");
                dialog.show();
                break;

            case "Bantama":
                district.setText("Bantama");
                loca.setText("Bantama High Street near the old G.N.T.C");
                address.setText("P. O. Box Kj 508, Kejetia-Kumasi");
                number.setText("0322080789, 0289116038");
                dialog.show();
                break;

            case "Ejisu":
                district.setText("Ejisu");
                loca.setText("Behind the Police Station");
                address.setText("P.O. Box 144, Ejisu");
                number.setText("0289116043");
                dialog.show();
                break;

            case "Ejura":
                district.setText("Ejura");
                loca.setText("Within the Ejura-Sekyedumase District Assembly block");
                address.setText("P.O. Box 9, Ejura");
                number.setText("0322099504, 0208030194");
                dialog.show();
                break;

            case "Kwabre":
                district.setText("Kwabre");
                loca.setText("Opposite Kwabre East District Assembly");
                address.setText("P.O. Box 8, Mamponteng");
                number.setText("0322091165");
                dialog.show();
                break;

            case "Manhyia":
                district.setText("Manhyia");
                loca.setText("Dichemso");
                address.setText("P.M.B. Kumasi");
                number.setText("0322081786");
                dialog.show();
                break;

            case "Obuasi":
                district.setText("Obuasi");
                loca.setText("Last Floor, Horsey Park Stores");
                address.setText("P.O. Box 861, Obuasi");
                number.setText("0322541379, 0322092215");
                dialog.show();
                break;

            case "Offinsoman":
                district.setText("Offinsoman");
                loca.setText("Opposite Offinso Rural Bank Ltd, Offinso Newtown");
                address.setText("P.O. Box 431, Offinso");
                number.setText("0332091540, 0332094693");
                dialog.show();
                break;

            case "Sekyere East":
                district.setText("Sekyere East");
                loca.setText("Opposite the Effiduase Police Station");
                address.setText("P. O. Box 302, Effiduase-Ashanti");
                number.setText("0322095162, 0322095163");
                dialog.show();
                break;

            case "Sekyere West":
                district.setText("Sekyere West");
                loca.setText("Premises of Mampong Municipal Assembly");
                address.setText("Municipal Assembly P. O. Box 460, Mampong - Ashanti");
                number.setText("0322222448, 0322222449");
                dialog.show();
                break;

            case "Subin":
                district.setText("Subin");
                loca.setText("Adum, Adjacent Aseda House");
                address.setText("P.O. Box Kj 509, Kejetia-Kumasi");
                number.setText("032208161/ 30289110000");
                dialog.show();
                break;
        }

        switch (selected){
            case "Brong Ahafo Regional Office":
                district.setText("Brong Ahafo Regional Office");
                loca.setText("Pentecost Premises, Adjacent Stanbic Bank");
                address.setText("P. O. Box 2116, Sunyani");
                number.setText("0352025293/ 0352025220/ 0203803648");
                dialog.show();
                break;

            case "Sene":
                district.setText("Sene");
                loca.setText("World Vision Road, Near Sene District Assembly");
                address.setText("P. O. Box 11, Kwame Danso");
                number.setText("035209 6444, 035 209 6445");
                dialog.show();
                break;

            case "Wenchi":
                district.setText("Wenchi");
                loca.setText("Near Wenchi Urban Council");
                address.setText("P. O. Box 21, Wenchi");
                number.setText("0246846307, 0244108006");
                dialog.show();
                break;

            case "Berekum":
                district.setText("Berekum");
                loca.setText("Near the Municipal Police Office");
                address.setText("C/O BOX 21, Berekum");
                number.setText("0352028709/0352024176");
                dialog.show();
                break;

            case "Sunyani":
                district.setText("Sunyani");
                loca.setText("Behind Pentecost International Worship Centre. SSNIT New Road, Nkwabenge North");
                address.setText("P. O. Box 2640, Sunyani");
                number.setText("0352028709/0352024176");
                dialog.show();
                break;

            case "Atebubu":
                district.setText("Atebubu");
                loca.setText("Opposite the District Assembly");
                address.setText("P. O. Box 125, Atebubu");
                number.setText("0322099563");
                dialog.show();
                break;

            case "Kintampo South":
                district.setText("Kintampo South");
                loca.setText("Opposite the District Hospital");
                address.setText("P. O. Box 50, Jema");
                number.setText("0208542980/0261784161");
                dialog.show();
                break;

            case "Asunafo North":
                district.setText("Asunafo North");
                loca.setText("Council Yard");
                address.setText("P. O. Box 237, Goaso");
                number.setText("0352091878/0243750865");
                dialog.show();
                break;

            case "Asunafo South":
                district.setText("Asunafo South");
                loca.setText("The Chief's Palace Kukuom");
                address.setText("P. O. Box 27, Kukuom");
                number.setText("0272772134");
                dialog.show();
                break;

            case "Tano North":
                district.setText("Tano North");
                loca.setText("Sawmill Road Duayaw Nkwanta");
                address.setText("P. O. Box DN 96");
                number.setText("0209128514/0244836520");
                dialog.show();
                break;

            case "Kintampo":
                district.setText("Kintampo");
                loca.setText("P&T Road opposite Municipal Hospital.");
                address.setText("P. O. Box 130, Kintampo");
                number.setText("0352038867");
                dialog.show();
                break;

            case "Jaman South":
                district.setText("Jaman South");
                loca.setText("Behind ST. Marys Hospital");
                address.setText("P. O. Box 56, Drobo");
                number.setText("0352094326");
                dialog.show();
                break;

            case "Nkoranza":
                district.setText("Nkoranza");
                loca.setText("Behind Rosana Hotel");
                address.setText("");
                number.setText("0352092088/0208030163");
                dialog.show();
                break;

            case "Techiman":
                district.setText("Techiman");
                loca.setText("Behind Electoral Commission");
                address.setText("P. O. Box TM522, Techiman");
                number.setText("0352091314");
                dialog.show();
                break;

            case "Tain":
                district.setText("Tain");
                loca.setText("Behind The Chief's Palace");
                address.setText("P. O. Box 44, Nsawkaw");
                number.setText("0352094731/0244519641");
                dialog.show();
                break;

            case "Dormaa Ahenkro":
                district.setText("Dormaa Ahenkro");
                loca.setText("Mortuary Junction");
                address.setText("P. O. Box 594, Dormaa");
                number.setText("");
                dialog.show();
                break;

            case "Jaman North":
                district.setText("Jaman North");
                loca.setText("Sampa District Hospital Premises");
                address.setText("P. O. Box 62, Sampa");
                number.setText("0542641496/0244024724");
                dialog.show();
                break;

            case "PRU":
                district.setText("PRU");
                loca.setText("Near ST Mathias Hospital");
                address.setText("P. O. Box 115, PRU");
                number.setText("026191589/0208252900");
                dialog.show();
                break;

            case "Asutifi":
                district.setText("Asutifi");
                loca.setText("Opposite ST. Elizabeth Hospital");
                address.setText("P. O. Box 23, Hwediem");
                number.setText("0352095455");
                dialog.show();
                break;
        }
        switch(selected){
            case "Central Regional Office":
                district.setText("Central Regional Office");
                loca.setText("Jubilee School, GPRTU Block");
                address.setText("P. O. Box AD 1021, Cape Coast");
                number.setText("O4234552");
                dialog.show();
                break;

            case "Abura-Asebu-Kwamankese":
                district.setText("Abura-Asebu-Kwamankese");
                loca.setText("Opposite Methodist School");
                address.setText("Abura Dunkwa");
                number.setText("042-94102");
                dialog.show();
                break;

            case "Ajumako-Enyan-Essiam":
                district.setText("Ajumako-Enyan-Essiam");
                loca.setText("Behind the Bank");
                address.setText("Enyan Denkyira");
                number.setText("020-8030143");
                dialog.show();
                break;

            case "Awutu-Efutu-Senya":
                district.setText("Awutu-Efutu-Senya");
                loca.setText("South Campus");
                address.setText("P. O. Box WB 504, Winneba");
                number.setText("043-221045/ 043222013");
                dialog.show();
                break;

            case "Agona":
                district.setText("Agona");
                loca.setText("Near ECG Office");
                address.setText("P. O. Box SW 595, Agona Swedru");
                number.setText("043-221045/ 043222013");
                dialog.show();
                break;

            case "Assin North":
                district.setText("Assin North");
                loca.setText("Behind the Fire Service station");
                address.setText("P. O. Box 102, Assin Fosu");
                number.setText("0246-598561");
                dialog.show();
                break;

            case "Assin South":
                district.setText("Assin South");
                loca.setText("Emancipation Centre");
                address.setText("Assin Manso");
                number.setText("");
                dialog.show();
                break;

            case "Asikuma-Odoben-Brakwa":
                district.setText("Asikuma-Odoben-Brakwa");
                loca.setText("Near the Ekumfi Market, Breman Asikuma");
                address.setText("P. O. Box WB 504, Breman Asikuma");
                number.setText("0249-787389");
                dialog.show();
                break;


            case "Cape Coast Muni":
                district.setText("Cape Coast Muni");
                loca.setText("Red Cross Premises");
                address.setText("Cape Coast");
                number.setText("042-32191");
                dialog.show();
                break;

            case "Gomoa":
                district.setText("Gomoa");
                loca.setText("Kaneshie Market");
                address.setText("P. O. Box AP 162, Apam");
                number.setText("");
                dialog.show();
                break;

            case "Komenda-Eguafo-Edina-Abirem":
                district.setText("Komenda-Eguafo-Edina-Abirem");
                loca.setText("Teterkesim");
                address.setText("Elimina");
                number.setText("042 94283 / 0289-116022");
                dialog.show();
                break;

            case "Mfantseman":
                district.setText("Mfantseman");
                loca.setText("Near SRI");
                address.setText("P. O. Box 28, Saltpond");
                number.setText("0289-116023");
                dialog.show();
                break;


            case "Lower Denkyiraa":
                district.setText("Lower Denkyira");
                loca.setText("R.30 Dist Assembly building");
                address.setText("Twifo Praso");
                number.setText("0208030146");
                dialog.show();
                break;

            case "Upper Denkyira":
                district.setText("Upper Denkyira");
                loca.setText("In the same building with Dunkwa-on-Offin Post Office");
                address.setText("P. O. Box DW 89,Dunkwa-on-Offin");
                number.setText("0372-28960");
                dialog.show();
                break;

        }
        switch (selected){
            case "Western Regional Office":
                district.setText("Western Regional Office");
                loca.setText("2nd Floor, SIC Building, Harbour Road, Takoradi");
                address.setText("N/A");
                number.setText("O312027193");
                dialog.show();
                break;

            case "Ahantaman":
                district.setText("Ahantaman");
                loca.setText("Behind Ahantawest District Assembly, Agona Nkwanta");
                address.setText("P.O.Box 22, Agona Nkwanta");
                number.setText("024-4181314");
                dialog.show();
                break;

            case "Wassa West":
                district.setText("Wassa West");
                loca.setText("Tarkwa Community Centre Near the Municipal Government Hospital-Tarkwa");
                address.setText("P.O.Box 1 Tarkwa");
                number.setText("024-3308610/ 0362-20272");
                dialog.show();
                break;

            case "Mpohor Wassa East":
                district.setText("Mpohor Wassa East");
                loca.setText("In District Directorate Of Health, Behind District Assembly Office Daboase");
                address.setText("P.O.Box 1008, Takoradi");
                number.setText("027-7873947/ 024-1071162");
                dialog.show();
                break;

            case "Nzema East":
                district.setText("Nzema East");
                loca.setText("Near Municipal Assembly-Axim");
                address.setText("P.O.Box 25 Axim");
                number.setText("024-3407697");
                dialog.show();
                break;

            case "Amenfi East":
                district.setText("Amenfi East");
                loca.setText("Opposite District Directorate Of Health-Wassa Akropong");
                address.setText("P.O.Box 25, Wassa Akropong");
                number.setText("020-8905049/ 024-3172212");
                dialog.show();
                break;

            case "Amenfiman":
                district.setText("Amenfiman");
                loca.setText("Community Library, Asankrakwa");
                address.setText("P.M.B Asankragwa");
                number.setText("024-3480449");
                dialog.show();
                break;

            case "Jomoro":
                district.setText("Jomoro");
                loca.setText("In District Assembly Block-Half Assin");
                address.setText("P.O.Box 176 Half Assin");
                number.setText("020-8401735");
                dialog.show();
                break;

            case "Aowin Suaman":
                district.setText("Aowin Suaman");
                loca.setText("Near District Assembly Office-Enchi");
                address.setText("C/O Box 32 Enchi");
                number.setText("024-3447465");
                dialog.show();
                break;


            case "Bia":
                district.setText("Bia");
                loca.setText("Near District Directorate Of Health Service-Essiam");
                address.setText("C/O Bia Dist.Assembly PMB, Essam Debiso");
                number.setText("024-5179971/ 024-2704962");
                dialog.show();
                break;

            case "Sefwi-Wiawso":
                district.setText("Sefwi-Wiawso");
                loca.setText("Near Cocoa board,Just By The Roadside-Sefwi Wiawso");
                address.setText("P.O.Box 183, Sefwi Wiawso");
                number.setText("024-4518243/ 027-4263530");
                dialog.show();
                break;

            case "Sekondi":
                district.setText("Sekondi");
                loca.setText("Opposite Library Board Near STMA Office, Sekondi");
                address.setText("P.O.Box AX 43 Takoradi");
                number.setText("024-4808515/ 031-2048892");
                dialog.show();
                break;

            case "Takoradi":
                district.setText("Takoradi");
                loca.setText("Near Super Star Hotel-Market Circle-Takoradi");
                address.setText("P.O.Box AX 43 Takoradi");
                number.setText("026-4417583/ 031-2029308");
                dialog.show();
                break;

            case "Shama":
                district.setText("Shama");
                loca.setText("Near The District Police Station-Shama");
                address.setText("P.O.Box 5 Shama");
                number.setText("024-9847282/ 031-2093379");
                dialog.show();
                break;

            case "Juabeso":
                district.setText("Juabeso");
                loca.setText("Near District Hospital Opposite Cocoa Board-Juabeso");
                address.setText("P.O.Box 1 Juabeso");
                number.setText("024-3689575");
                dialog.show();
                break;

            case "Bibiani-Anhwiaso":
                district.setText("Bibiani-Anhwiaso");
                loca.setText("Near Gov’t District Hospital-Opposite Lorry Park-Bibiani");
                address.setText("P.O.Box 49 Bibiani");
                number.setText("024-3240139");
                dialog.show();
                break;
        }
        switch (selected){
            case "Eastern Regional Office":
                district.setText(" Eastern-Regional Office");
                loca.setText("Biney Plaza-Adweso, Koforidua");
                address.setText("P. O.Box KF 2786, Koforidua");
                number.setText("081-26919/ 081 26920");
                dialog.show();
                break;

            case "Afam Plains":
                district.setText("Afam Plains");
                loca.setText("Near Lorry Station, Donkorkrom");
                address.setText("P. O. Box 43, Donkorkrom");
                number.setText("0848-22134");
                dialog.show();
                break;

            case "Akuapem North":
                district.setText("Akuapem North");
                loca.setText("Inside the Abiriw Market, Akropong.");
                address.setText("P. O. Box 154, Akropong");
                number.setText("0289116012");
                dialog.show();
                break;

            case "Akuapem South":
                district.setText("Akuapem South");
                loca.setText("Near District Magistrate Court, Nsawam.");
                address.setText("P. O. Box NW 602, Nsawkaw");
                number.setText("0832-22047");
                dialog.show();
                break;

            case "Asuogyaman":
                district.setText("Asuogyaman");
                loca.setText("New Adom Dwamena School, New Akrade.");
                address.setText("P. O. Box AB 456, Akosombo");
                number.setText("0251-20262");
                dialog.show();
                break;

            case "Atiwa":
                district.setText("Atiwa");
                loca.setText("Near GCB, Anyinam");
                address.setText("P. O. Box AY 71, Anyinam");
                number.setText("0289112228");
                dialog.show();
                break;

            case "Birim North":
                district.setText("Birim North");
                loca.setText("Near GCB, Anyinam");
                address.setText("P. O. Box 1, New Abirem");
                number.setText("081-26919/ 081 26920");
                dialog.show();
                break;

            case "Birim South":
                district.setText("Birim South");
                loca.setText("Near GCB, Anyinam");
                address.setText("P. O. Box 1, New Abirem");
                number.setText("081-26919/ 081 26920");
                dialog.show();
                break;

            case "East Akim":
                district.setText("East Akim");
                loca.setText("Near Municipal Assembly Office,Kibi");
                address.setText("P. O. Box 174, Kibi");
                number.setText("081-30772");
                dialog.show();
                break;

            case "Fanteakwa":
                district.setText("Fanteakwa");
                loca.setText("New Lorry Park,Begoro");
                address.setText("P. O. Box 113, Begoro");
                number.setText("081-95294");
                dialog.show();
                break;

            case "Kwaebibirim":
                district.setText("Kwaebibirim");
                loca.setText("Near Latter Day Saint Church,Kade.");
                address.setText("P. O. Box 114, Kade");
                number.setText("0208030155");
                dialog.show();
                break;


            case "Kwahu South":
                district.setText("Kwahu South");
                loca.setText("Near ECG Office, Mpraeso.");
                address.setText("P. O. Box 27, Mpraeso");
                number.setText("0846-22264");
                dialog.show();
                break;

            case "Kwahu West":
                district.setText("Kwahu West");
                loca.setText("Opposite Holy Family Hospital");
                address.setText("P. O. Box 770, Nkawkaw");
                number.setText("0842-22344");
                dialog.show();
                break;

            case "Manya Krobo":
                district.setText("Manya Krobo");
                loca.setText("Behind Agormenya Methodist J.S.S");
                address.setText("P. O. Box OK 266, Odumase-Krobo");
                number.setText("0289116014");
                dialog.show();
                break;

            case "New Juaben":
                district.setText("New Juaben");
                loca.setText("Opposite Total 1 Filling Station, Koforidua");
                address.setText("P. O. Box KF 518, Koforidua");
                number.setText("081-26266");
                dialog.show();
                break;

            case "Suhum":
                district.setText("Suhum");
                loca.setText("Suhum Community Centre");
                address.setText("P. O. Box U 260, Suhum");
                number.setText("0858-22302");
                dialog.show();
                break;

            case "West Akim":
                district.setText("West Akim");
                loca.setText("At the Main Market, Asamankese");
                address.setText("P. O. Box AS 392, Asamankese");
                number.setText("08126720");
                dialog.show();
                break;

            case "Yilo Krobo":
                district.setText("Yilo Krobo");
                loca.setText("Behind District Magistrate Court, Somanya");
                address.setText("P. O. Box 102, Somanya");
                number.setText("0289116017");
                dialog.show();
                break;
        }
                    switch (selected) {

                    case "Greater Accra Regional Office":
                    district.setText("Regional Office-Greater Accra");
                    loca.setText("Oxford street, near Osu Shell");
                    address.setText("");
                    number.setText("0302 762087");
                    dialog.show();
                    break;

                    case "Ablekuma":
                    district.setText("Ablekuma");
                    loca.setText("Mataheko, Opposite Regent University College");
                    address.setText("P.O.Box MP 1771, Mamprobi, Accra");
                    number.setText("0302-301721, 301739");
                    dialog.show();
                    break;

                    case "Ashiedu Keteke":
                    district.setText("Ashiedu Keteke");
                    loca.setText("Near PML Hosp. Behind Arena Lorry Station");
                    address.setText("P. O. Box GP 2152, Accra");
                    number.setText("0302-668643, 0272277092");
                    dialog.show();
                    break;

                    case "Ayawaso":
                    district.setText("Ayawaso");
                    loca.setText("Dzorwulu, Near Last Chance Last stop");
                    address.setText("P. O. Box PMB 44, Kanda, Accra");
                    number.setText("0302-760183, 0245010688");
                    dialog.show();
                    break;

                    case "Dangme East":
                    district.setText("Dangme East");
                    loca.setText("Ada Foah, Opposite Methodist Mission House");
                    address.setText("");
                    number.setText("03035-22321, 0243446855");
                    dialog.show();
                    break;

                    case "Dangme West":
                    district.setText("Dangme West");
                    loca.setText("Dodowa, Premises of the District Assembly");
                    address.setText("");
                    number.setText("027-7423003");
                    dialog.show();
                    break;

                    case "GA District":
                    district.setText("GA District");
                    loca.setText("Amasaman, Premises of the Municipal Assembly");
                    address.setText("P. O. Box AM 1, Amasaman");
                    number.setText("0302-917161, 0243870481");
                    dialog.show();
                    break;

                    case "Kpeshie":
                    district.setText("Kpeshie");
                    loca.setText("Nungua, Opp the Kpeshie Divisional Police Headquarters");
                    address.setText("P. O. Box OS 1979, Osu");
                    number.setText("028-9116006");
                    dialog.show();
                    break;

                    case "Okaikoi":
                    district.setText("Okaikoi");
                    loca.setText("Abeka, Near Abeka Post Office.");
                    address.setText("P. O. Box PMB 9, Kaneshie");
                    number.setText("0302-238325, 0271270311");
                    dialog.show();
                    break;

                    case "Osu Klottey":
                    district.setText("Osu Klottey");
                    loca.setText("Asylum Down, Near FIDA Ghana and Happy FM");
                    address.setText("P. O. Box AD 133, Adabraka");
                    number.setText("0302-250997, 0243138814");
                    dialog.show();
                    break;

                    case "Tema":
                    district.setText("Tema");
                    loca.setText("Community 8, Near Ernest Chemists");
                    address.setText("");
                    number.setText("027-7003147");
                    dialog.show();
                    break;
                    }
                    switch (selected) {

                    case "Northern Regional Office":
                    district.setText("Northern Regional Office");
                    loca.setText("Alhassan Gbanzaba Junction, Education Ridge, Tamale");
                    address.setText("P. O. Box 491 Tamale");
                    number.setText("0242861022/ 208408223");
                    dialog.show();
                    break;

                    case "Tamale":
                    district.setText("Tamale");
                    loca.setText("Near Tamale Metro Assembly");
                    address.setText("P. O. Box Tl2455,Tamale");
                    number.setText("O244868349");
                    dialog.show();
                    break;

                    case "Tolon/Kumbungu":
                    district.setText("Tolon/Kumbungu");
                    loca.setText("Opposite World Vision Office");
                    address.setText("P.O. Box 1, Tolon");
                    number.setText("O243507671");
                    dialog.show();
                    break;

                    case "Savelugu":
                    district.setText("Savelugu");
                    loca.setText("Near The Chief’s Palace");
                    address.setText("P.O. Box 45, Savelugu");
                    number.setText("O243389473");
                    dialog.show();
                    break;

                    case "Nanumba North":
                    district.setText("Nanumba North");
                    loca.setText("Bimbilla Hospital Premises");
                    address.setText("P.O. Box 1, Bimbilla");
                    number.setText("071-23476 / 242861022");
                    dialog.show();
                    break;

                    case "Zabzugu/Tatale":
                    district.setText("Zabzugu/Tatale");
                    loca.setText("District Assembly Premises");
                    address.setText("P.O. Box Zb 1, Zabzugu");
                    number.setText("O242209226");
                    dialog.show();
                    break;

                    case "Saboba/Chereponi":
                    district.setText("Saboba/Chereponi");
                    loca.setText("Saboba Medical Centre Premises");
                    address.setText("P.O. Box Sb42, Saboba");
                    number.setText("0243655312");
                    dialog.show();
                    break;

                    case "Karaga":
                    district.setText("Karaga");
                    loca.setText("District Assembly Premises");
                    address.setText("P.O. Box. 1 , Karaga");
                    number.setText("0244605877/ 278077765");
                    dialog.show();
                    break;

                    case "East Mamprusi":
                    district.setText("East Mamprusi");
                    loca.setText("Opposite District Education Office");
                    address.setText("P.O. Box 41, Gambaga");
                    number.setText("0243711822/ 201435455");
                    dialog.show();
                    break;

                    case "Bunkpurugu/Yunyoo":
                    district.setText("Bunkpurugu/Yunyoo");
                    loca.setText("Nakpanduri Area Council Building");
                    address.setText("P.O. Box 11, Nakpanduri");
                    number.setText("O245946085");
                    dialog.show();
                    break;

                    case "Nanumba South":
                    district.setText("Nanumba South");
                    loca.setText("Wulensi Health Centre Premises");
                    address.setText("P.O. Box 1, Wulensi");
                    number.setText("0245201716");
                    dialog.show();
                    break;

                    case "Gushegu":
                    district.setText("Gushegu");
                    loca.setText("Gushegu District Hospital Premises");
                    address.setText("P.O. Box 1, Gushegu");
                    number.setText("0205894261");
                    dialog.show();
                    break;

                    case "Yendi":
                    district.setText("Yendi");
                    loca.setText("Near Yendi Municipal Assembly");
                    address.setText("P.O. Box 133, Yendi");
                    number.setText("0243258952");
                    dialog.show();
                    break;

                    case "West Gonja":
                    district.setText("West Gonja");
                    loca.setText("Near Damongo District Assembly");
                    address.setText("P.O. Box Dm97, Damongo");
                    number.setText("0244772692");
                    dialog.show();
                    break;

                    case "West Mamprusi":
                    district.setText("West Mamprusi");
                    loca.setText("Walewale Hospital Premises");
                    address.setText("P.O. Box 6, Walewale");
                    number.setText("0208267889");
                    dialog.show();
                    break;

                    case "Bole":
                    district.setText("Bole");
                    loca.setText("Bole District Hospital Premises");
                    address.setText("P.O. Box 72, Bole");
                    number.setText("0244088291");
                    dialog.show();
                    break;

                    case "Sawla/Tuna/Kalba":
                    district.setText("Sawla/Tuna/Kalba");
                    loca.setText("Inside Vip Market, Sawla");
                    address.setText("P.O. Box 3, Sawla");
                    number.setText("0242861022");
                    dialog.show();
                    break;

                    case "East Gonja":
                    district.setText("East Gonja");
                    loca.setText("Near Salaga Hospital");
                    address.setText("P.O. Box 9 Salaga");
                    number.setText("0208314424");
                    dialog.show();
                    break;

                    case "Central Gonja":
                    district.setText("Central Gonja");
                    loca.setText("Near Buipe Health Centre");
                    address.setText("P.O. Box Tl2455 Tamale");
                    number.setText("0242377156");
                    dialog.show();
                    break;

                    }
                    switch (selected) {

                    case "Upper East Regional Office":
                    district.setText("UPPER EAST Regional Office");
                    loca.setText("Ground floor of the Municipal Assembly");
                    address.setText("P. O. Box 38, Bolgatanga.");
                    number.setText("O7222960");
                    dialog.show();
                    break;

                    case "Bolgatanga":
                    district.setText("Bolgatanga");
                    loca.setText("Near Bolga Mun. Health Administration.");
                    address.setText("P. O. Box 38, Bolga.");
                    number.setText("020 8740628");
                    dialog.show();
                    break;

                    case "Bawku":
                    district.setText("Bawku");
                    loca.setText("Near Bawku Mun. Assembly,");
                    address.setText("P. O. Box 1, Bawku.");
                    number.setText("024 4980866");
                    dialog.show();
                    break;

                    case "Bawku West":
                    district.setText("Bawku West");
                    loca.setText("Behind The District Police Station");
                    address.setText("P. O. Box 1, Zebilla.");
                    number.setText("024 4505075");
                    dialog.show();
                    break;

                    case "Bongo":
                    district.setText("Bongo");
                    loca.setText("Near Bongo District Assembly");
                    address.setText("P. O. Box 1, Bongo");
                    number.setText("024 4243833");
                    dialog.show();
                    break;

                    case "Builsa":
                    district.setText("Builsa");
                    loca.setText("Opposite Sandema Community Centre");
                    address.setText("P. O. Box 3, Sandema");
                    number.setText("024 3708345");
                    dialog.show();
                    break;

                    case "Kassena/Nankana":
                    district.setText("Kassena/Nankana");
                    loca.setText("Behind The District Court");
                    address.setText("P. O. Box 94, Navrongo.");
                    number.setText("024 4068116");
                    dialog.show();
                    break;
                    }
                    switch (selected) {

                    case "Upper West Regional Office":
                    district.setText("UPPER WEST Regional Office");
                    loca.setText("Sokpayiri Okala Building, Opposite Bundanbile Store, Wa");
                    address.setText("P. O. Box 587, Wa");
                    number.setText("0756 21002/ 21043");
                    dialog.show();
                    break;

                    case "Jirapa / Lambussie":
                    district.setText("Jirapa / Lambussie");
                    loca.setText("Behind the District Asembly building");
                    address.setText("P. O. Box 1, Jirapa");
                    number.setText("024 4985706");
                    dialog.show();
                    break;

                    case "Lawra":
                    district.setText("Lawra");
                    loca.setText("Within the Lawra District Hospital Yard");
                    address.setText("P. O. Box 89, Lawra");
                    number.setText("020 8379653/ 024 6105374");
                    dialog.show();
                    break;

                    case "Nadowli":
                    district.setText("Nadowli");
                    loca.setText("Opp. St Thomas Aquinas Primary sch. Nadowli New Town");
                    address.setText("P. O. Box 40, Nadowli");
                    number.setText("024 2608733");
                    dialog.show();
                    break;

                    case "Sissala East":
                    district.setText("Sissala East");
                    loca.setText("Old District Assembly Building");
                    address.setText("P. O. Box 107,Tumu");
                    number.setText("024 9230021/ 020 8333450");
                    dialog.show();
                    break;


                    case "Sissala West":
                    district.setText("Sissala West");
                    loca.setText("G.H.S Bungalows-Gwollu");
                    address.setText("P. O. Box 90, Gwollu");
                    number.setText("027 4506144/ '024 8666147");
                    dialog.show();
                    break;


                    case "Wa East":
                    district.setText("Wa East");
                    loca.setText("Near the Funsi Catholic Church");
                    address.setText("P. O. Box 212, Funsi");
                    number.setText("020 9240378/ 024 6541886");
                    dialog.show();
                    break;

                    case "Wa Municipal":
                    district.setText("Wa Municipal");
                    loca.setText("Opp. MFU Yard, Kabanye");
                    address.setText("P. O. Box 587, WA");
                    number.setText("024 3750174/ 020 9365250");
                    dialog.show();
                    break;


                    case "Wa West":
                    district.setText("Wa West");
                    loca.setText("Within the District Assembly Building");
                    address.setText("P. O. Box 151, Wechiau");
                    number.setText("024 4144047/ 020 7440854");
                    dialog.show();
                    break;

                    }
                    switch (selected) {


                    case "Volta Regional Office":
                    district.setText("Volta Regional Office");
                    loca.setText("N/A");
                    address.setText("NHIA, PMB, HO");
                    number.setText("03620 25038/ 25036");
                    dialog.show();
                    break;

                    case "Kpando":
                    district.setText("Kpando");
                    loca.setText("Opp. Gayibor Academy Junction, Near MTN Office");
                    address.setText("Post Office Box Kd 59, Kpando");
                    number.setText("0936 50778/024-4994879");
                    dialog.show();
                    break;

                    case "Jasikan":
                    district.setText("Jasikan");
                    loca.setText("Within Jasikan District Assembly Building");
                    address.setText("P. O. Box 20 Jasikan");
                    number.setText("024 3504074");
                    dialog.show();
                    break;

                    case "Keta":
                    district.setText("Keta");
                    loca.setText("Dzelukofe. Sharing Compound With Radio Jubilee F.M.");
                    address.setText("Post Office Box Kw. 231, Keta");
                    number.setText("0362 642111/ 643044");
                    dialog.show();
                    break;

                    case "Adaklu Anyigbe":
                    district.setText("Adaklu Anyigbe");
                    loca.setText("Adjacent the District Police Headquaters,");
                    address.setText("Dmhis P.O. Box Ap47 Kpetoe");
                    number.setText("0362 094915");
                    dialog.show();
                    break;

                    case "South Dayi":
                    district.setText("South Dayi");
                    loca.setText("Former Premises Of The District Assembly");
                    address.setText("P. O. Box 8, Kpeve");
                    number.setText("O243728440");
                    dialog.show();
                    break;

                    case "South Tongu":
                    district.setText("South Tongu");
                    loca.setText("Opposite New Dist. Assembly Complex,");
                    address.setText("Box Sk 146,Sogakope");
                    number.setText("0244437724/0289116054");
                    dialog.show();
                    break;

                    case "Ho":
                    district.setText("Ho");
                    loca.setText("Off Coca Cola To Volta Regional Hospital Road");
                    address.setText("P.O. Box Hp 1137, HO");
                    number.setText("0362025246");
                    dialog.show();
                    break;

                    case "Hohoe":
                    district.setText("Hohoe");
                    loca.setText("Alabato Road, Behind the main market");
                    address.setText("Box 13 Hohoe");
                    number.setText("036220523");
                    dialog.show();
                    break;

                    case "Nkwanta":
                    district.setText("Nkwanta");
                    loca.setText("District Assembly Block V/R Nkwanta");
                    address.setText("C/O P. O. Box 1, Old Nkwanta");
                    number.setText("N/A");
                    dialog.show();
                    break;


                    case "Ketu":
                    district.setText("Ketu");
                    loca.setText("Old Assembly Hall, Atikume Denu");
                    address.setText("Atikume Denu Box De 189, Denu");
                    number.setText("0362091234/0362-091235");
                    dialog.show();
                    break;

                    case "Krachi East":
                    district.setText("Krachi East");
                    loca.setText("Opp. Church Of Pentecost (Central Chapel Dambai Junction)");
                    address.setText("Post Office Box 1, Dambai");
                    number.setText("N/A");
                    dialog.show();
                    break;

                    case "Akatsi":
                    district.setText("Akatsi");
                    loca.setText("Akatsi Old Market (Main Road Accra To Aflao)");
                    address.setText("Post Office Box Ak 105 Akatsi,");
                    number.setText("N/A");
                    dialog.show();
                    break;

                    case "Krachi West":
                    district.setText("Krachi West");
                    loca.setText("Opposite Ghana Post");
                    address.setText("Mail Box 42, Kete-Krachi");
                    number.setText("095322204/ 0242915374");
                    dialog.show();
                    break;

                    case "North Tongu":
                    district.setText("North Tongu");
                    loca.setText("Near The Main Station Opposite the Market");
                    address.setText("P.O.Box 19 Adidome V/R");
                    number.setText("0362095233");
                    dialog.show();
                    break;

                    case "Kadjebi":
                    district.setText("Kadjebi");
                    loca.setText("Near Ghana Commercial Bank, Kadjebi");
                    address.setText("P. O. Box 50, Kadjebi");
                    number.setText("O244209780");
                    dialog.show();
                    break;

                    }

                    return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        listAdapter.fitlerData(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.fitlerData(newText);
        return false;
    }

    @Override
    public boolean onClose() {
        listAdapter.fitlerData("");
        return false;
    }
}
