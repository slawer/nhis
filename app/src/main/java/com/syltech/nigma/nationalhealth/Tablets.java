package com.syltech.nigma.nationalhealth;

import java.util.ArrayList;

/**
 * Created by nigma on 28/06/2017.
 */

public class Tablets extends ArrayList<Tablets> {
    private String lblListItem;

    public Tablets(String lblListItem)
    {
        this.lblListItem = lblListItem;
    }

    public String getLblListItem()
    {
        return lblListItem;
    }

    public void setLblListItem(String lblListItem) {

        this.lblListItem = lblListItem;
    }
}
