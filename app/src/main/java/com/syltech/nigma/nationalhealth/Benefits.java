package com.syltech.nigma.nationalhealth;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Benefits extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener{

    private ImageButton btnBack;
    private List<RowItem> nhis=new ArrayList<RowItem>();
    private TextView txt;
    Typeface tfc1;

    String[] players={"Registering as a member","Accessing healthcare with NHIS card","Rights and responsibilities","Renewals","Replacements","Offences"};
    int[] images={R.color.colorRed,R.color.colorYellow,R.color.colorBrown,R.color.colorPurple,R.color.colorLightBlue,R.color.colorDeepBlue};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_benefits);


        btnBack= (ImageButton) findViewById(R.id.imageButtonGetBack);
        btnBack.setOnClickListener(this);

        nhis.add(new RowItem("Out-Patient Services(OPD)", R.color.colorRed));
        nhis.add(new RowItem("In-Patient Services(IPD)", R.color.colorYellow));
        nhis.add(new RowItem("Oral Health Services", R.color.colorBrown));
        nhis.add(new RowItem("Eye Care Services", R.color.colorPurple));
        nhis.add(new RowItem("Maternity Care", R.color.colorLightBlue));
        nhis.add(new RowItem("Emergencies", R.color.colorDeepBlue));

        txt=(TextView) findViewById(R.id.textView3);
        tfc1= Typeface.createFromAsset(getAssets(),"fonts/ARLRDBD.ttf");
        txt.setTypeface(tfc1);

        ArrayAdapter<RowItem> adapter = new Benefits.customAdapter();

        ListView myList=(ListView) findViewById(R.id.list);
        myList.setAdapter(adapter);

        myList.setOnItemClickListener(this);
        btnBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
                //startActivity(new Intent(this,Welcome.class));
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                startActivity(new Intent(Benefits.this, OutPatient.class));
                break;
            case 1:
                startActivity(new Intent(Benefits.this, InPatient.class));
                break;
            case 2:
                startActivity(new Intent(Benefits.this, OralHealth.class));
                break;
            case 3:
                startActivity(new Intent(Benefits.this, EyeCare.class));
                break;
            case 4:
                startActivity(new Intent(Benefits.this, Maternity.class));
                break;
            case 5:
                startActivity(new Intent(Benefits.this, Emergencies.class));
                break;

            //Toast.makeText(GettingStarted.this, "My listview "+position, Toast.LENGTH_SHORT).show();
        }

    }

    private class customAdapter extends ArrayAdapter<RowItem>{

        public customAdapter(){
            super(Benefits.this,R.layout.list_item,nhis);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if(convertView==null){
                convertView= getLayoutInflater().inflate(R.layout.list_item,parent,false);
            }

            RowItem item = nhis.get(position);

            ImageView myImg= (ImageView) convertView.findViewById(R.id.profile_pic);
            TextView myHeading= (TextView) convertView.findViewById(R.id.member_name);

            myImg.setImageResource(item.getProfile_pic_id());
            myHeading.setText(item.getMember_name());

            return convertView;
        }
    }
        /*ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.list_item);
        ListView myList= (ListView) findViewById(R.id.list);

        //Setup adapter
      //  CustomAdapter adapter=new CustomAdapter(this,players,images);
        myList.setAdapter(adapter);
*/



}

