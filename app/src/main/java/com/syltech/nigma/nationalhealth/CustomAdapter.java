package com.syltech.nigma.nationalhealth;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nigma on 16/06/2017.
 */

public class CustomAdapter extends ArrayAdapter<RowItem> {
    Context context;
    String[] players;
    int[] images;
    LayoutInflater inflater;
    //List<RowItem> rowItems;


    public CustomAdapter(Context context, String[] players, int[] images) {
        super(context,R.layout.list_item);

        this.context = context;
        this.players = players;
        this.images = images;
       // this.inflater = inflater;
    }


    //private view holder class
    private class ViewHolder {
        ImageView img;
        TextView play;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Check if view is null if so then create it
        if(convertView==null)
        {
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_item,null);

        }

        //othrtwise
        ViewHolder holder=new ViewHolder();

        //initialise our views
        holder.play= (TextView) convertView.findViewById(R.id.member_name);
        holder.img= (ImageView) convertView.findViewById(R.id.profile_pic);

        //assign data
        holder.play.setText(players[position]);
        holder.img.setImageResource(images[position]);

        return convertView;
    }
}
