package com.syltech.nigma.nationalhealth;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nigma on 28/06/2017.
 */

public class MyTabListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Pills> tabletList;
    private ArrayList<Pills> originalList;

    public MyTabListAdapter(Context context,ArrayList<Pills> tabletList){
        this.context=context;
        this.tabletList=new ArrayList<Pills>();
        this.tabletList.addAll(tabletList);

        this.originalList=new ArrayList<Pills>();
        this.originalList.addAll(tabletList);
    }
    @Override
    public int getGroupCount() {
        return tabletList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Tablets> tabList=tabletList.get(groupPosition).getTabs();
        return tabList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return tabletList.get(groupPosition);//.toString();
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {

        //ArrayList<Tablets> pillstList=tabletList.get(groupPosition).getName().get(childPosition);
        return tabletList.get(groupPosition).getTabs().get(childPosition).getLblListItem(); //.get(childPosition).getLblListItem();//.toString();//.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Typeface tfcFaqs;

        //String headerTitle=(String)getGroup(groupPosition);
        Pills headerTitle=(Pills) getGroup(groupPosition);
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_group3,null);
        }


        TextView lblListHeader=(TextView)convertView.findViewById(R.id.lblHeader);

        tfcFaqs= Typeface.createFromAsset(context.getAssets(),"fonts/ARLRDBD.ttf");

        lblListHeader.setTypeface(tfcFaqs);
        lblListHeader.setText(headerTitle.getName().trim());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText=(String)getChild(groupPosition,childPosition);

        //Tablets childText = (Tablets) getChild(groupPosition,childPosition);
        Typeface tfcFaqs;
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_item_3,null);
        }
        TextView txtListChild=(TextView)convertView.findViewById(R.id.lblListItem);
        //lblListHeader.setTypeface(null, Typeface.BOLD);
        tfcFaqs= Typeface.createFromAsset(context.getAssets(),"fonts/HelveticaNeue-Thin.ttf");

        txtListChild.setTypeface(tfcFaqs);
        txtListChild.setText(childText);//.getLblListItem().trim());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void fitlerData(String query){
        query=query.toLowerCase();
        Log.v("MyTabListAdapter",String.valueOf(tabletList.size()));
        tabletList.clear();

        if(query.isEmpty()){
            tabletList.addAll(originalList);
        }
        else{
            for (Pills pills:originalList){
                ArrayList<Tablets> pillsList=pills.getTabs();
                ArrayList<Tablets> newList=new ArrayList<Tablets>();

                for(Tablets tablets: pillsList){
                    if (tablets.getLblListItem().toLowerCase().contains(query)){
                        newList.add(tablets);
                    }
                }
                if(newList.size()>0)
                {
                    Pills mPills=new Pills(pills.getNumber(),pills.getName(),newList);;
                    tabletList.add(mPills);
                }
            }
        }
        Log.v("MyTabListAdapter",String.valueOf(tabletList.size()));
        notifyDataSetChanged();
    }
}
