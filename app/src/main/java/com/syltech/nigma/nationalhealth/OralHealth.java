package com.syltech.nigma.nationalhealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class OralHealth extends AppCompatActivity implements View.OnClickListener{

    private ImageButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oral_health);

        btnBack=(ImageButton) findViewById(R.id.imageButtonGetBack);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
                //startActivity(new Intent(OralHealth.this,Benefits.class));
                break;
        }
    }
}
