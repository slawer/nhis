package com.syltech.nigma.nationalhealth;

import java.util.ArrayList;

/**
 * Created by nigma on 14/07/2017.
 */

public class Region {


    private String name;
    private ArrayList<Location> loc=new ArrayList<Location>();

    public Region( String name, ArrayList<Location> loc) {
        this.name = name;
        this.loc = loc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Location> getLoc() {
        return loc;
    }

    public void setLoc(ArrayList<Location> loc) {
        this.loc = loc;
    }
}
