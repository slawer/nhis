package com.syltech.nigma.nationalhealth;

import java.util.ArrayList;

/**
 * Created by nigma on 14/07/2017.
 */

public class Location extends ArrayList<Region> {
    private String lblListItem;

    public Location(String lblListItem) {
        this.lblListItem = lblListItem;
    }

    public String getLblListItem() {
        return lblListItem;
    }

    public void setLblListItem(String lblListItem) {
        this.lblListItem = lblListItem;
    }
}
