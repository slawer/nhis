package com.syltech.nigma.nationalhealth;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class RegisteringInfo extends AppCompatActivity implements View.OnClickListener{

    private TextView txt;
    private ImageButton btnBack;

    Typeface tfc1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registering_info);


        btnBack=(ImageButton) findViewById(R.id.imageButtonGetBack);

        btnBack.setOnClickListener(this);

        txt=(TextView) findViewById(R.id.textView3);
        tfc1= Typeface.createFromAsset(getAssets(),"fonts/ARLRDBD.ttf");
        txt.setTypeface(tfc1);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.imageButtonGetBack:
                this.finish();
                //startActivity(new Intent(this,GettingStarted.class));
                break;
        }
    }
}
