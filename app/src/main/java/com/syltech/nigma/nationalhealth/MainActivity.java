package com.syltech.nigma.nationalhealth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.graphics.Typeface.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private FloatingActionButton fabFirst;
    private TextView txt;
    Typeface tfc1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fabFirst=(FloatingActionButton) findViewById(R.id.fabFirst);
        txt=(TextView) findViewById(R.id.textView);
        tfc1=Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue-Thin.ttf");
        txt.setTypeface(tfc1);

        fabFirst.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.fabFirst:
                startActivity(new Intent(this,Welcome.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Are you sure you want to exit ?");
        builder.setCancelable(true);
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
}
